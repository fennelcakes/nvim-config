 -- Neovim init.lua
-- TODO: pcall() all require('plugin') funcs
-- TODO: code_runner and run-code are redundant, code_runnner works better rn
-- TODO: Move plugin remaps to their plugin's .lua file



_G.__luacache_config = {
  chunks = {
    enable = true,
    path = vim.fn.stdpath("cache") .. "/luacache_chunks",
  },
  modpaths = {
    enable = true,
    path = vim.fn.stdpath("cache") .. "/luacache_modpaths"
  },
}

require("impatient") -- for faster loading

-- Config calls
require("lsp")
require("options")
require("remaps")
require("functions")
require("highlights")
require("global_vars")
require("plugins")
-- Print a random vimtip on NvimTree toggle
require("vimtips")
require("vmlens")


