--i=1,1 Global key remap
-- Plugin specific remaps are in their own .lua file in the plugins folder.

-- Modes
-- '' (an empty string)     mapmode-nvo 	Normal, Visual, Select, Operator-pending 	:map
-- 'n' 	mapmode-n 	    Normal 	:nmap
-- 'v' 	mapmode-v 	    Visual and Select 	:vmap
-- 's' 	mapmode-s 	    Select 	:smap
-- 'x' 	mapmode-x 	    Visual 	:xmap
-- 'o' 	mapmode-o 	    Operator-pending 	:omap
-- '!' 	mapmode-ic 	    Insert and Command-line 	:map!
-- 'i' 	mapmode-i 	    Insert 	:imap
-- 'l' 	mapmode-l 	    Insert, Command-line, Lang-Arg 	:lmap
-- 'c' 	mapmode-c 	    Command-line 	:cmap
-- 't' 	mapmode-t 	   Terminal 	:tmap


local map = vim.api.nvim_set_keymap
local silent = { silent = true, noremap = true }
map("", "<Space>", "<Nop>", silent)
vim.g.mapleader = " "
vim.g.maplocalleader = " "
-- Remap for dealing with word wrap
-- vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
-- vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Command Center as remapper
map("n", "<leader>p", "<cmd>Telescope command_center<cr>", { noremap = true })
map("v", "<leader>p", "<cmd>Telescope command_center<cr>", { noremap = true })
local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  -- Write/Quit/Source
  {
    desc = "Quit",
    cmd = "<cmd>q<CR>",
    keys = { "n", "<leader>;q", noremap }
  },
  {
    desc = "Write",
    cmd = "<cmd>w<CR>",
    keys = { "n", "<leader>w", noremap }
  },
  {
    desc = "Quit All",
    cmd = "<cmd>qa<CR>",
    keys = { "n", "<leader>;x", noremap }
  },
  {
    desc = ":source init.lua file",
    cmd = "<cmd>source ~/.config/nvim/init.lua<CR>",
    keys = { "n", "<leader>;si", noremap }
  },
  {
    desc = "Write + Source current file",
    cmd = "<cmd>w<CR><CMD>so %<CR>",
    keys = { "n", "<leader>;so", noremap }
  },
  {
    desc = "Open config folder",
    cmd = "<cmd>e ~/.config/nvim/<CR>",
    keys = { "n", "<leader>;ve", noremap }
  },


  -- Operator Pending remaps
  {
    desc = "word to inner word",
    cmd = "iw",
    keys = { "o", "w", noremap }
  },
  {
    desc = "'W'ord to outer 'W'ord",
    cmd = "aw",
    keys = { "o", "W", noremap }
  },
  -- Overridden by othes plugins
  -- {
  --   desc = "sentence to inner sentence",
  --   cmd = "is",
  --   keys = { "o", "s", noremap }
  -- },
  -- {
  --   desc = "'S'ord to outer 'S'entence",
  --   cmd = "as",
  --   keys = { "o", "S", noremap }
  -- },


  -- Movement
  {
    desc = "<M-left> to move to beginn. of word (Insert)",
    cmd = "<Esc>bi",
    keys = { "i", "<M-Left>", noremap },
  },
  {
    desc = "<M-right> to move to end of word (Insert)",
    cmd = "<Esc>ea",
    keys = { "i", "<M-right>", noremap },
  },
  {
    desc = "<M-f> to delete word (Insert)",
    cmd = "<C-o>de",
    keys = { "i", "<M-Del>", noremap },
  },
  {
    desc = "<M-w> (delete) to diw (Normal)",
    cmd = "db",
    keys = { "n", "<M-BS>", noremap },
  },
  {
    desc = "<M-w> to <C-w> (Insert)",
    cmd = "<C-w>",
    keys = { "i", "<M-BS>", noremap },
  },
  {
    desc = "<Esc> to same cursor position (Insert)",
    cmd = "<Esc>l",
    -- keys = { "i", "ō", noremap }
    keys = { "i", "<Esc>", noremap }
  },
  {
    desc = "New line below (Insert)",
    cmd = "<Esc>o",
    -- keys = { "i", "ō", noremap }
    keys = { "i", "<M-o>", noremap }
  },
  {
    desc = "New line above (Insert)",
    cmd = "<Esc>O",
    -- keys = { "i", "ō", noremap }
    keys = { "i", "<S-M-o>", noremap }
  },
  {
    desc = "New line below (no insert mode)",
    cmd = "o<esc>",
    keys = { "n", "o", noremap }
  },
  {
    desc = "New line above (Normal)",
    cmd = "O<esc>",
    keys = { "n", "O", noremap }
  },
  {
    desc = "New line below and enter i-mode (Normal)",
    cmd = "o",
    -- keys = { "i", "ō", noremap }
    keys = { "n", "<M-o>", noremap }
  },
  {
    desc = "New line above and enter i-mode (Normal)",
    cmd = "<Esc>O",
    -- keys = { "i", "ō", noremap }
    keys = { "n", "<S-M-o>", noremap }
  },
  {
    desc = "Move to end of line (Insert)",
    cmd = "<esc>A",
    -- keys = { "i", "ā", noremap }
    keys = { "i", "<M-a>", noremap }
  },
  {
    desc = "Move to start of line in (Insert)",
    cmd = "<esc>I",
    -- keys = { "i", "ī", noremap }
    keys = { "i", "<M-i>", noremap }
  },
  {
    desc = "Move to end of line (Normal)",
    cmd = "<esc>A",
    -- keys = { "i", "ā", noremap }
    keys = { "n", "<M-a>", noremap }
  },
  {
    desc = "Move to start of line (Normal)",
    cmd = "<esc>I",
    -- keys = { "i", "ī", noremap }
    keys = { "n", "<M-i>", noremap }
  },
  {
    desc = "E: E == ge (move to end of previous word)",
    cmd = "ge",
    keys = { "n", "E", noremap }
  },

  -- Copying/Pasting
  {
    desc = "Paste from clipboard (normal)",
    cmd = "\"+p",
    keys = { "n", "<M-p>", noremap }
  },
  {
    desc = "Paste from clipboard (normal)",
    cmd = "<esc>\"+pA",
    keys = { "i", "<M-p>", noremap }
  },
  {
    desc = "Yank (Y) from cursor to end of line",
    cmd = "y$",
    keys = { "n", "Y", noremap }
  },
  {
    desc = "Paste last yank below",
    cmd = '"0p',
    keys = { "n", "gp", noremap }
  },
  {
    desc = "Paste last yank above",
    cmd = '"0P',
    keys = { "n", "gP", noremap }
  },
  {
    desc = "Copy contents of current file to clipboard",
    cmd = "<cmd>%y+<CR>",
    keys = { "n", "<leader>cp", noremap }
  },
  {
    desc = "Yank visual selection to clipboard",
    cmd = ":\'<,\'>y+<CR><esc>",
    keys = { "v", "<leader>y", noremap }
  },
  {
    desc = "Yank word under cursor to clipboard",
    cmd = "\"+yiw",
    keys = { "n", "yw", noremap }
  },
  {
    desc = "Yank filename to clipboard",
    cmd = "<cmd>let @+ = expand(\"%:t\")<CR>",
    keys = { "n", "<leader>cf", noremap }
  },
  {
    desc = "Visual Reselect Last Yank",
    cmd = "`[v`]",
    keys = { "n", "gy", noremap }
  },
  {
    desc = "Next Method Start",
    cmd = "]m",
    keys = { "n", "]]", noremap }
  },
  {
    desc = "Previous Method Start",
    cmd = "[m",
    keys = { "n", "[[", noremap }
  },
  {
    desc = "Substitute word under cursor w/ \" register",
    cmd = 'diw"0P',
    keys = { "n", "\\s", noremap }
  },
  {
    desc = "Substitute visual selection",
    cmd = "\"hy:%s/<C-r>h//gc<left><left><left>",
    keys = { "x", "<C-r>", noremap }
  },

  -- Visual Mode
  {
    desc = "Select last change",
    cmd = "`^v`.",
    keys = { "n", "gV", noremap }
  },
  {
    desc = "Enter Visual Mode", -- Swapped with standard vim command
    cmd = "v",
    keys = { "n", "V", noremap }
  },
  {
    desc = "Enter Visual Line Mode",
    cmd = "V",
    keys = { "n", "v", noremap }
  },
  {
    desc = "Visual select word under cursor",
    cmd = "viw",
    keys = { "n", "<M-v>", noremap }
  },
  {
    desc = "Select entire file",
    cmd = "ggvG",
    keys = { "n", "<leader>va", noremap }
  },
  {
    desc = "Find/Replace pattern within selection (Visual)",
    cmd = ":s/\\%V\\v",
    keys = { "v", "<leader>vs", noremap }
  },
  {
    desc = "Delete pattern within selection",
    cmd = ":s/\\v//<Left><Left>",
    keys = { "v", "<leader>vd", noremap }
  },
  {
    desc = "Sort visual selection alphabetically",
    cmd = "<cmd>sort<CR>",
  },
  {
    desc = "Insert text at the beginning of each selected line (visual)",
    cmd = ":norm i",
    keys = { "v", "<leader>i", noremap }
  },


  -- Window Management
  {
    desc = "Split window (Horizontal)",
    cmd = "<c-w>s",
    keys = { "n", "<leader>ws", noremap }
  },
  {
    desc = "Split window (Vertical)",
    cmd = "<c-w>v",
    keys = { "n", "<leader>wv", noremap }
  },
  {
    desc = "Move to window below",
    cmd = "<c-w>j",
    keys = { "n", "<M-down>", noremap }
    -- keys = { "n", "<leader>wj", noremap }
  },
  {
    desc = "Move to window above",
    cmd = "<c-w>k",
    keys = { "n", "<M-up>", noremap }
    -- keys = { "n", "<leader>wk", noremap }
  },
  {
    desc = "Move to window left",
    cmd = "<c-w>h",
    keys = { "n", "<M-left>", noremap }
    -- keys = { "n", "<leader>wh", noremap }
  },
  {
    desc = "Move to window right",
    cmd = "<c-w>l",
    keys = { "n", "<M-right>", noremap }
    -- keys = { "n", "<leader>wl", noremap }
  },
  {
    desc = "Move to previous window (Normal)",
    cmd = "<c-w>p",
    keys = { "n", "<M-S-p>", noremap }
  },
  {
    desc = "Move to previous window (Terminal)",
    cmd = "<c-w>p",
    keys = { "n", "<M-S-p>", noremap }
  },
  {
    desc = "Move to next window (cycle)",
    cmd = "<c-w>w",
    keys = { "n", "<leader>ww", noremap }
  },
  {
    desc = "close current window:",
    cmd = "<c-w>c",
    keys = { "n", "<leader>wc", noremap }
  },
  {
    desc = "close all windows but current:",
    cmd = "<c-w>o",
    keys = { "n", "<leader>wo", noremap }
  },
  {
    desc = "equal window size:",
    cmd = "<c-w>=",
    keys = { "n", "<leader>w=", noremap }
  },

  -- Tab/Buffer Management
  {
    desc = "Go to next tab page",
    cmd = "gt",
    -- keys = { "n", "<leader>tn", noremap }
  },
  {
    desc = "Go to previous tab page",
    cmd = "gT",
    -- keys = { "n", "<leader>tn", noremap }
  },
  {
    desc = "Create new tab",
    cmd = "<cmd>tabnew<CR>",
    keys = { "n", "<leader>tn", noremap }
  },
  {
    desc = "Close current tab",
    cmd = "<cmd>tabclose<CR>",
    keys = { "n", "<leader>tc", noremap }
  },
  {
    desc = "Close all tabs but current",
    cmd = "<cmd>tabonly<CR>",
    keys = { "n", "<leader>to", noremap }
  },
  -- ':tabmove N', Move current tab to after tab page N.
  -- User zero to make the current page the first one.
  -- Without the N, the ta page is made the last one.
  {
    desc = "Move current tab to page n+1",
    cmd = ":tabmove ",
    keys = { "n", "<leader>tm", noremap }
  },

  -- These are handled by bufferline/cybu plugin commands
  -- {
  --     desc = "Move to previous buffer",
  --     cmd = "<cmd>bp<CR>",
  --     keys = {"n", "<S-h>", noremap}
  -- },
  -- {
  --     desc = "Move to next buffer",
  --     cmd = "<cmd>bn<CR>",
  --     keys = {"n", "<S-l>", noremap}
  -- },

  -- Text Handling & Search
  {
    desc = "Force text to wrap at the textwidth limit",
    cmd = "gqG"
  },
  {
    desc = "<C-L> to dd (Normal)",
    cmd = "dd",
    keys = { "n", "<C-l>", noremap },
  },
  {
    desc = "<C-L> to dd (Insert)",
    cmd = "<Esc>ddi",
    keys = { "i", "<C-l>", noremap },
  },
  {
    desc = "<C-w> - delete to previous word",
    cmd = "<Esc>ddi",
    keys = { "i", "<C-l>", noremap },
  },
  {
    desc = "Delete trailing whitespace from visual selection",
    cmd = "<cmd>s/\\s*$//<CR>/Removed trailing whitespace!<CR>",
    keys = { "v", "<leader>rw", noremap }
  },
  {
    desc = "Move visual selection tab-left",
    cmd = "<gv",
    keys = { "v", "<", noremap }
  },
  {
    desc = "Move visual selection tab-right",
    cmd = ">gv",
    keys = { "v", ">", noremap }
  },
  -- {
  --   desc = "Tab backwards in insert mode",
  --   cmd = "<c-d>",
  --   keys = { "i", "<S-Tab>", noremap }
  -- },
  {
    desc = "Global substitution in current file",
    cmd = ":%s///g<Left><Left><Left>",
    keys = { "n", "<leader>vs", noremap }
  },
  {
    desc = "Insert a space char in Normal Mode",
    cmd = "i<Space><ESC>",
    keys = { "n", "<C-Space>", noremap }
  },
  {
    desc = "Repeat recorded command {char}",
    cmd = "@",
    keys = { "n", "<leader>qr", noremap }
  },
  {
    desc = "Repeat recorded `q` command",
    cmd = "@w",
    keys = { "n", "Q", noremap }
  },
  {
    desc = "vimgrep current file",
    cmd = ":vimgrep // % | copen<left><left><left><left><left><left><left><left><left><left><left>",
    keys = { "n", "<leader>fv", noremap }
  },
  {
    desc = "Open quickfix list",
    cmd = "<cmd>copen<cr>",
    keys = { "n", "<leader>qo", noremap }
  },
  {
    desc = "Increment numbers in a visual selection",
    cmd = "<cmd>norm <CR>gv",
  },
  {
    desc = "Decrease numbers in a visual selection",
    cmd = "<cmd>norm <CR>gv",
  },


  -- Terminal Management
  {
    desc = "Open a terminal in a vertical split",
    cmd = "<c-w>v:terminal<CR>",
    -- keys = { "n", ";t", noremap } -- F12 being a karabiner remap
    keys = { "n", "<M-\\>", noremap }
  },
  {
    desc = "Open a terminal in a new tab",
    cmd = "<CMD>tabnew<CR><CMD>terminal<CR>i",
    keys = { "n", "<C-t>", noremap }
  },
  {
    desc = "Switch Tabs in Terminal Mode",
    cmd = "<C-\\><C-n>gt",
    -- keys = { "n", ";t", noremap }
    keys = { "t", "gt", noremap }
  },
  {
    desc = "Enter normal mode in a terminal",
    cmd = "<C-\\><C-n>",
    -- keys = { "n", ";t", noremap }
    keys = { "t", "<M-\\>", noremap }
  },
  {
    desc = "In Terminal: move to window left",
    cmd = "<C-\\><C-n><C-w>h",
    keys = { "t", "<leader>wh", noremap }
  },
  {
    desc = "In Terminal: move to window below",
    cmd = "<C-\\><C-n><C-w>j",
    keys = { "t", "<leader>wj", noremap }
  },
  {
    desc = "In Terminal: move to window above",
    cmd = "<C-\\><C-n><C-w>k",
    keys = { "t", "<leader>wk", noremap }
  },
  {
    desc = "In Terminal: move to window right",
    cmd = "<C-\\><C-n><C-w>l",
    keys = { "t", "<leader>wl", noremap }
  },

  -- Misc (Toggle options)
  {
    desc = "Toggle rnu and nu",
    cmd = "<cmd>set rnu!<CR>",
    keys = { "n", "<leader>rnu", noremap }
  },
  {
    desc = "Toggle rnu and nu",
    cmd = "<cmd>set nu!<CR>",
    keys = { "n", "<leader>nu", noremap }
  },
  {
    desc = "Toggle spellcheck",
    cmd = "<cmd>set spell!<CR>",
  },
  {
    desc = "Toggle Virtual Edit",
    cmd = "<cmd>let &ve=(&ve == 'none' ? 'all' : 'none')<CR><CMD>set ve<CR>",
    keys = { "n", "<leader>;vt", noremap }
  },


  -- Misc useful shortcuts
  {
    desc = "Record macro on register q",
    cmd = "qq",
    keys = { "n", "<M-q>", noremap }
  },
  {
    desc = "Back to previous file",
    cmd = "<CMD>:edit #<cr><CR>",
    keys = { "n", "<bs>", noremap }
  },
  {
    desc = "Format buffer",
    cmd = "<cmd>lua vim.lsp.buf.format()<CR>",
    keys = { "n", ",ff", noremap }
  },
  {
    desc = "Create an ordered list", -- Need to input range and format print with a string if needed
    cmd = ":for i in range(,) | put =''.i.'' | endfor<C-f>", -- <C-f> for command-line window. `.` concats in put
    keys = { "n", "<leader><leader>ol" }
  },
  {
    desc = "Command-Line Window",
    cmd = "q:<up>$", -- Automatically enter insert mode
    keys = { "n", "<M-;>", noremap }
  },
  {
    desc = "Forward Search Command-Line Window",
    cmd = "q/i", -- Automatically enter insert mode
    keys = { "n", "q/", noremap }
  },
  {
    desc = "Backwards Search Command-Line Window",
    cmd = "q?i", -- Automatically enter insert mode
    keys = { "n", "q?", noremap }
  },
  {
    desc = "Ctrl-C (doubled_)",
    cmd = "<c-c><c-c>",
    keys = { "n", "<c-c>", noremap }
  },
  {
    desc = "Command-Line Window (from command line)",
    cmd = "<C-f>i",
    keys = { "n", "<C-f>", noremap }
  },
  {
    desc = "Datetime print",
    cmd = "<cmd>put =strftime('%Y-%m-%d %T')<CR>",
  },
  -- Too obtrusive but I like the idea
  -- {
  --   desc = "Command-line Window",
  --   cmd = "q:",
  --   keys ={ "n", "::", noremap}
  -- },



  -- Tests
  {
    desc = "Test of PageUp",
    cmd = "<cmd>echo 'bob'<CR>",
    keys = { "n", "<PageUp>", noremap }
  },
  {
    desc = "Test of Ctrl+Meta",
    cmd = "<cmd>echo 'bob'<CR>",
    keys = { "n", "<C-M-y>", noremap }
  },
  {
    desc = "Test of Meta+Number",
    cmd = "<cmd>echo 'bob'<CR>",
    keys = { "n", "<M-0>", noremap }
  },

  -- Highlights
  -- TODO: Make_this into a function that can toggle this
  {
    desc = "Highlight current line (persistent)",
    cmd = "<cmd>call matchadd('LineHighlight', '\\%'.line('.').'l')<CR>",
    keys = { "n", "<leader><leader>l", noremap }
  },
  {
    desc = "Clear persistent line highlight",
    cmd = "<cmd>call clearmatches()<CR>",
    keys = { "n", "<leader><leader>c", noremap }
  },
  {
    desc = "Clear highlights",
    cmd = "<cmd>set hlsearch!<CR>",
    keys = { "n", "<leader>hh", noremap }
  },


  -- NOTE: Plugin Remaps ^8390

  -- -- EasyAlign
  {
    desc = "EasyAlign inner paragraph (visual)",
    cmd = "<Plug>(EasyAlign)",
    keys = { "x", "ga", noremap }
  },
  {
    desc = "EasyAlign inner paragraph (normal)",
    cmd = "<Plug>(EasyAlign)",
    keys = { "n", "ga", noremap }
  },

  -- -- Indent-Blankline
  -- {
  --   desc = "Toggle Indent-Blankline",
  --   cmd = "<cmd>IndentBlanklineToggle",
  --   keys = { "n", "<leader><leader>i", noremap }
  -- },

  -- Sort Folds
  {
    desc = "Sort Folds",
    cmd = "<Plug>SortFolds",
    -- keys = { "v", "<Leader>sf", noremap },
  },

  -- Plenary
  { desc = "Plenary: run test on file",
    cmd = "<Plug>PlenaryTestFile"
  },
})



local api = vim.api
-- local augroup = api.nvim_create_augroup
local autocmd = api.nvim_create_autocmd
-- local cmd = api.nvim_command

vim.api.nvim_create_user_command(
  "FollowMarkdownLink",
  function()
    vim.cmd('norm "zyi[')
    local reg = vim.fn.getreg('z')
    require("telescope.builtin").find_files({
      cwd = "~/Library/Mobile Documents/iCloud~md~obsidian/Documents/",
      search_file = reg
    })
  end,
  {}
)
--
autocmd({ "FileType" }, {
  pattern = "markdown",
  callback = function()
    command_center.add({
      {
        desc = "Follow Markdown Link",
        cmd = "<CMD>FollowMarkdownLink<CR>",
        keys = { "n", "<C-CR>", noremap }
      },
    })

  end,
})
