local api = vim.api
-- local augroup = api.nvim_create_augroup
local autocmd = api.nvim_create_autocmd
-- local cmd = api.nvim_command

-- Global options

local options = {
  pumheight = 20, -- limit popup menu height (e.g. for completion suggestions)
  -- splitkeep = "screen",
  splitright     = true,
  splitbelow     = true,
  backup         = false, -- creates a backup file
  -- clipboard = "unnamedplus", -- allows neovim to access the system clipboard
  cmdheight = 0,
  confirm        = true, -- confirm before closing unsaved file

  colorcolumn    = "80", -- highlight given column
  cursorcolumn   = true,
  cursorline     = true,

  fileencoding   = "utf-8",

  -- fold options
  foldcolumn     = '0', -- fold column
  fillchars      = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]],
  foldmethod     = 'expr',
  foldexpr       = 'nvim_treesitter#foldexpr()',
  list           = true,
  hidden         = true,
  -- laststatus = 2,
  mouse          = "a",
  nu             = true,
  relativenumber = true,
  virtualedit    = "none", -- Position cursor everywhere
  -- There's a mapping to toggle this in 'functions.lua', i.e. `:V`
  timeoutlen     = 250,

  tw             = 79, -- width of document

  wrap           = true,
  linebreak      = true,

  -- Tab Handling:
  expandtab      = true,
  shiftwidth     = 2,
  shiftround     = true,
  showmode       = false,
  softtabstop    = 2,
  tabstop        = 8,

  termguicolors  = true, -- default is false

  -- Undofile
  undodir        = '../../backup',
  undofile       = true,
  undoreload     = 10000,
  updatetime     = 300,
  inccommand     = "nosplit", -- split shows substitutions in a preview window
  -- search
  ignorecase     = true,
  smartcase      = true, -- only works with ingnorecase = true

  -- Change directory to the current file's directory
  autochdir      = true,

  -- GUI Settings
  guifont        = "SauceCodePro Nerd Font:h18",
  -- Command Line history
  history = 1000,
}

for k, v in pairs(options) do -- for key, value pair, let...
  vim.opt[k] = v
end

--[[
-- " if has('gui_running')
-- "
-- "     " Mouse Settings:
-- "     set mouse=a     " on OSX press ALT and click
-- "     highlight Cursor guifg=white guibg=DarkCyan
-- "     " highlight iCursor guifg=white guibg=steelblue gui=bold
-- "     set guicursor=n-v-c:block-Cursor
-- "     " set guicursor+=i:ver100-iCursor
-- "     set guicursor+=n-v-c:blinkon0
-- "     set guicursor+=i:blinkwait10
-- "     set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
-- "     \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
-- "     \,sm:block-blinkwait175-blinkoff150-blinkon175c
-- "     " Font:
-- "     " Syntax: set guifont=<font_name>\ <font_weight>\ <size>
-- "     " set guifont=UbuntuMono\ Nerd\ Font\ Code:h16
-- "     " set guifont=MesloLGM\ NF\ Code:h16
-- "     set guifont=Source\ Code\ Pro\ Code:h16
-- "     " set guifont=Hack\ Nerd\ Font\ Code:h16
-- "
-- "     " Set Background Tone:
-- "     set background=dark
-- "
-- " endif
--
]]

-- Tab Settings for specific filetypes
autocmd({ "Filetype" }, {
  pattern = "html", -- add other filetypes here
  callback = function()
    local tab_opts = {
      -- Tab Handling:
      shiftwidth = 4,
      softtabstop = 4,
    }
    for k, v in pairs(tab_opts) do
      vim.o[k] = v
    end
  end,
})


-- Append/Remove options
-- How to append individually:
-- vim.opt.shortmess:append "c"
-- vim.opt.shortmess:append "a"
-- vim.opt.guicursor:append "n-v-i-c:blinkon1" -- Cursor blink on

autocmd("FileType", {
  callback = function()
    vim.opt.formatoptions = vim.opt.formatoptions
        - "a" -- Dont format pasted code
        - "t" -- Delegate to linter prgs/LSP
        - "o" -- O and o don't continue comments
        - "r" -- Return does not continue comments
        + "c" -- comments respect textwidth
        + "q" -- Allow formatting comments w/ gq
        + "n" -- Recognize numbered lists
        + "j" -- Auto-remove comments if possible.
        + "2" -- Indent according to 2nd line

    vim.opt.shortmess = vim.opt.shortmess
        + "a"
        + "c"

    vim.opt.guicursor = vim.opt.guicursor
        + "n-v-i-c:blinkon1" -- Cursor blink on

    -- vim.opt_local.spell = true

  end,
  desc = "Custom formatoptions",
})

-- Set opts for entering and moving into new windows (e.g. Saga Outline)
local opt = vim.o
autocmd({ "CursorMoved" }, {
  pattern = "*",
  callback = function()
    opt.relativenumber = true
  end,
})


-- -- This makes code_runner act weird.
-- autocmd({ "TermEnter", "TermOpen" }, {
--   pattern = "*",
--   callback = function()
--     opt.relativenumber = false
--     opt.relativenumber = false
--     opt.startinsert = true
--   end,
-- })

-- Global variables
-- vim.g.jk_jumps_minimum_lines = 5 -- cf plugin
vim.g.python3_host_prog = "/usr/local/bin/python3"

-- Neovide Settings
vim.cmd([[
if exists("g:neovide")
  let g:neovide_floating_blur_amount_x = 2.0
  let g:neovide_floating_blur_amount_y = 2.0
  let g:neovide_input_macos_alt_is_meta=v:true
  let g:neovide_remember_window_size = v:true
endif
]])


