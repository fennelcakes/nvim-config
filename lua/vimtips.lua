-- local api = vim.api
-- local augroup = api.nvim_create_augroup
-- local autocmd = api.nvim_create_autocmd

-- NOTE: Pay attention to escape sequences if you're reading tips from its file
local tips = {

  -- Non-Vim tips
  "FZF IN TERMINAL: tap ESC and then C quickly.",

  -- Plugin Related tips
  "<M-s> to fast wrap a surrounding pair in insert mode (autopairs)",
  "cx{motion} and cxx (for line) to exchange {motion}",
  "<leader>si/su/sn for ISwap (swap treesitter items)",
  "NvimTree - Go to file <leader>;e",
  "TS-Subjects: . (smart) ; (outer) (i;) inner",
  "TS-units: au (outer) (iu) inner (objects)",
  "<M-a> and <M-p> in normal mode to jump around matching words (vim-illuminate)",
  "g?p for debugline (and capitalized variations, g?P, g?v, g?V, g?o)",
  ":DeleteDebugPrints deletes all debug prints (g?p, etc.)",
  -- Nvim-Surround Tips
  [[
      Surround:  keymaps = {
        insert = "<C-g>s",
        insert_line = "<C-g>S",
        normal = "ys",
        normal_cur = "yss",
        normal_line = "yS",
        normal_cur_line = "ySS",
        visual = "S",
        visual_line = "gS",
        delete = "ds",
        change = "cs", }
        ]],
  "Surround: <C-g>S in Insert mode to add pair on new lines",
  "Surround: <C-g>s in Insert mode to add pair around curor in line",
  "Surround: S{pairchar} in Visual to add pair char to selection on new lines",
  "Surround: yS to surrund object w/ pairs on new lines",
  "Surround: ySS to surround line w/ pairs on new lines",
  "Surround: yss to surround line w/ pairs",
  "Surround: t/T as 'tag' text objects", -- e.g. cstdiv changes <img id="foo"> to <div id="foo">, csTdiv changes it to <div>
  "Surround: f as function object OR 'pair', e.g. dsf OR ysiwf{funcname}",
  "Surround: ys{obj}i ('insert') to surround w/ custom left/right delimiter, e.g. yssi/<CR>\\",
  -- Treesitter-Unit
  "In O-PENDING, iu and au capture a treesitter node and all its children",


  -- Personal Remaps/Functions
   "\\s Substitute word under cursor w/ \" register -> 'diw\"0P'",
  -- "<M-o> in n- or i-mode to create new line and enter insert mode",
  "<M-v> to visual select word under the cursor (viw)",
  -- "<M-p> to paste from system register, Normal and Insert mode",
  "cq to record a macro, q to stop, <CR> to repeat it on a match of the word under the cursor",
  -- "<leader>` to toggle boolean values (true/false, 0/1)",
  "\\s to substitue word under cursor w/ 0 register",
  "Persistent Line Highlights: <leader><leader>l | <leader><leader>c to clear.",
  "<Tab>s in Normal mode to insert into the right indentation",
  "<leader>i in visual mode to insert text at the beginning of each selected line",
  "<C-w> in Insert to delete from the cursor position to the beginning of the word",
  "<C-u> in Insert to delete from the cursor position to the beginning of the word",
  "<C-m> to invoke Line Management Hydra",
  "<leader>;hd to invoke Dap Hydra",
  "toggle_fstring() function in command center to toggle a Python string to format-string",

  -- General Vim commands
  ":<line number>t. – copies the line number specified and pastes it to the line below",
  "g<C-a> in visual to increment numbers in selection",
  -- 1
  -- 1
  -- 1
  "== to align line/selection",
  "={object} to align object",
  "o in Visual Mode to jump to start/end of selection",
  "q: to open editable command line",
  "q/ or q? to open editable command-line search",
  "<C-u> i-mode: delete all entered chars before the cursor in the current line",
  "gt and gT to cycle between tabs",
  "<C-t> and <C-d> in/dedentt the whole line, unlike <Tab> which will indent everything after the cursor",
  ":g/pat/normal {commands} to execute a normal mode command across all matches",
  "Use . or any single byte char that's not alphabetic, \\, or |, instead of / in your multi-line commands",
  "]s and [s to navigate through misspelled words",
  -- "gv to reselect last visual selection",
  "gv to visual reselect last yank",
  ":norm in Visual mode to execute a normal operation",
  "R to enter Replace mode",
  "<C-a> and <C-x> increment/decrement the next number on the line, or, w/ :norm, in a visual selection",
  "<C-g> and <C-t> to cycle through search results w/o leaving search 'mode' (the command line)",
  "g< can be used to see the last page of previous command output.",
  ":messages to view previously given messages.",

  -- VimTips Daily https://github.com/forkmantis/vimtipsdaily/blob/master/bot.js
  "<C-o> to use a single Normal mode operation in Insert mode.",
  "'o' to toggle to other end of highlighted text.",
  "gf jumps to the filename under the cursor",
  "q: to access command-line window, and <C-c> to close it.",
  "<C-f> to switch from Command-Line mode to the command-line window (works for search too)",
  "the argdo to execute an ex command across all files in the :args list. i.e., a substitution command",
  "'{range}: normal {stuff}' to edit multiple lines in a selection",
  ":ls command to list buffers in memory",
  -- Tips from Practical VIm Part II. Files */
  "':write !{cmd}' to pipe buffer as input into the specified command",
  "%:h to get the path part of the current file. i.e., cd %:h to set the working dir to current",
  -- Tips from Practical VIm Part III. Getting Around Faster */
  "a /search as the motion for some other command, like delete (d/search<C-R>)",
  -- "`` to jump to snap back to where you were before your last jump (or '' to jump to the last line)",
  ") and ( to jump to the beginning and end of sentences.",
  ":changes to see the change list. Use g; and g, to jump to the locations of the changes.",
  -- "m{capital letter} to make a global mark. Close and reopen VIm, and press '{capital letter} to open the file w/ the global mark.",
  -- "The 0 register will contain your most recent yank, even if you have deleted after yanking. Yank a word, delete a different word, then type \"0p to put the yanked word.",
  -- Tips from Practical VIm Part IV. Registers */
  -- "Delete using the \"_d{motion} command to delete without overwriting your default register.",
  -- "the \"+ register to interact with the system clipboard. I.e., \"+p to put system clipboard into your current buffer.",
  "Put from the \"% register to put the name of the current file.",
  "Put from the \"# register to put the name of the alternate file (the most recent other file edited in the current buffer).",
  "Put from the \": register to put the most recently executed EX command.",
  -- "Put from the \"/ register to put the last search pattern used.",
  "In insert mode, put the contents of the default register with <c-r>\".",
  "gP and gp to put before or after the current line, and place cursor below the pasted line.",
  -- ":reg to view the contents of all registers, or :reg{register} to view the contents of one.",
  "10@a to play the macro saved in register a 10 times.",
  ":normal @a to play the macro saved in register a on every line in the selection.",
  "qA to append keystrokes to the macro in register a",
  ":edit! to revert all changes to the current buffer",
  -- "'var += 1' in a macro to make a numbered list.  let i = 1<CR> qa I<C-r>=i<CR>) <Esc>:let i += 1<CR> j q", -- Tip 71
  "<leader><leader>ol to begin an ordered list",
  "Edit a macro by putting it's register into your buffer, editing, then yanking it back to its register. \"{reg}p, [edit], \"{reg}dd",

  -- Tips from Practical VIm Part V. Patterns */
  -- "\"\\_s\" to search for whitespace or a linebreak. Compare to searching for \\s to see the difference.",
  "\\zs and \\ze to delimit the visible or selected part of a match.",
  -- "the escape({string}, {chars}) function to escape characters - tip 79",*/
  "In character-wise visual selection mode, use gn to extend your selection to the next occurrence of the most recent search pattern.",
  "ea to append to the end of the current word.",
  "Append '/e' to the end of a search to place the cursor at the end of the next match. i.e. /search phrase/e",
  "use gUgn to capitalize the next search match.",
  -- "To wrap text in, say, parens, use c{motion}(<C-r>\"). Change copies to default buffer, and in Insert mode, <C-r>\" pastes from the default buffer.",
  ":%s--/gn to count the matches of your most recent search.",
  ":vimgrep /{search phrase}/g % to populate the Quickfix list with each match in the current buffer, which can be navigated with :cnext or :cprev.",
  -- My own tips */
  -- ":marks to list marks.",
  -- "In insert mode, type the first few characters of a word, then press <C-n> or <C-p> to autocomplete with the next or previous matching word",
  -- Random stuff I ran across and added */
  "yHP to yank from the current line to the top of the screen and PUT above the top of the screen.",
  ":2,4co. to copy lines 2 through 4 and PUT it below the current line. Use :help :co for more info",
  "'&' to repeat your last substitution on the current line. Use 'g&' to repeat the last substitution on all lines.",
  -- "Open VIm directly to the first occurrence of a search term with `vim +/searchterm filename`",
  "Repeat your most recent search/replace on the current line by pressing & or g& across all lines",
  -- "-3,-1s#a#X#g to replace all instances of 'a' with 'X' from from 3 lines to 1 line above the cursor.",
  ":g to execute a command on a series of lines matching a pattern, i.e. ':g/aaa/y A' to yank all matches of 'aaa' into the register.",
  ":v to execute a command on a series of lines not matching a pattern, i.e. ':v/aaa/d' to delete all non-matches of 'aaa'.",
  "Select a column of numbers, and use g<C-a> to increment them as a set",
  "Delete without overwriting your last yank using the black hole register (_). i.e., Delete a word w/ \"_daw, then put the last value in your default register",
  "Delete a sentence using 'das' with the cursor anywhere in the sentence.  Change a sentence using 'cas'",
  "z+ (or zt), zz,  z- (or zb) to redraw the screen with the cursor at the top, middle, or bottom of the screen respectively",
  -- "use :help help to learn how to use VIm's robust help system",
  "<C-f> during a `/` search to open a search history window",
  -- ":bd to close a buffer without closing VIm.  Use :bd! to close without saving changes.",
  "gU{motion} to make everything in the {motion} uppercase. i.e. gUaw to make the word under the cursor uppercase.",
  "Navigate to the beginning or end of your most recently yanked or changed text using `[ or `] respectively",
  "Navigate to the beginning or end of your most recent visual selection text using `< or `> respectively",
  "Put from the \"+ register to put from the system clipboard.",
  "/ to search, then <C-r>/ as your search pattern to put your most recent yank/delete in insert mode",
  -- "Open the last edited file on the machine with cursor on the latest location by pressing <C-o><C-o>",
  "Limit a search and replace operation between lines matching 2 regex patterns using /pattern1/,/pattern2/s/search/replace/",
  -- "Insert/Append text at the beginning/end of each line of the block using I/A{text}<Esc>",
  "the ex command 'j' to join some lines on a range, i.e. :1,5j",
  "Visually select some lines, and save them to a new file with the command :w path/to/new/file",
}

local Event = require('nvim-tree.api').events.Event

require('nvim-tree.api').events.subscribe(Event.TreeClose, function()
    local randomIndex = math.random(#tips)
    local randomTip = tips[randomIndex]
    vim.notify(randomTip)
end)


local noremap = { noremap = true }
local command_center = require("command_center")
command_center.add({
  {
    desc = "Show a random VimTip",
    cmd = function()
      local randomIndex = math.random(#tips)
      local randomTip = tips[randomIndex]
      vim.notify(randomTip)
    end,
    keys = { "n", "<leader>\\", noremap }
  }
})




