--------- EXAMPLE FOR SINGLE HL COMMAND ----------

-- -- Treesitter autogroup
-- augroup("treesitter", {
--   clear = true,
-- })
-- -- Tree sitter rainbow colors
-- autocmd({ "Filetype" }, {
--   pattern = "*",
--   callback = function()
--     hl(0, "rainbowcol1", {
--       bold = false,
--       fg = "#FF7B72",
--     })
--   end,
--   group = "treesitter",
-- })
--------------------------------------------------


-- local cmd = vim.cmd
-- -- Overridden by iterm
-- cmd([[ autocmd FileType * highlight Cursor guifg=White guibg=DarkCyan ]])
-- cmd([[ autocmd FileType * highlight iCursor guifg=White guibg=SteelBlue gui=bold]])


local api = vim.api
local augroup = api.nvim_create_augroup
local autocmd = api.nvim_create_autocmd
-- local cmd = api.nvim_command
local hl = api.nvim_set_hl
local opt_local = vim.opt_local
-- local opt_global = vim.opt_global

-- Highlights for lspsaga winbar

local colors = {
  fg = '#bbc2cf',
  bg1 = '#232B38',
  highlight = '#556178',
  bg2 = '#1E242E',
  separator = '#d16d9e',
  red = '#e95678',
  orange = '#FF8700',
  yellow = '#f7bb3b',
  green = '#afd700',
  cyan = '#36d0e0',
  blue = '#61afef',
  violet = '#C972E4',
  teal = '#1abc9c',
}

-- local kind = {
--   [1] = { 'File', ' ', colors.fg },
--   [2] = { 'Module', ' ', colors.blue },
--   [3] = { 'Namespace', ' ', colors.orange },
--   [4] = { 'Package', ' ', colors.violet },
--   [5] = { 'Class', ' ', colors.violet },
--   [6] = { 'Method', ' ', colors.violet },
--   [7] = { 'Property', ' ', colors.cyan },
--   [8] = { 'Field', ' ', colors.teal },
--   [9] = { 'Constructor', ' ', colors.blue },
--   [10] = { 'Enum', '了', colors.green },
--   [11] = { 'Interface', '練', colors.orange },
--   [12] = { 'Function', ' ', colors.violet },
--   [13] = { 'Variable', ' ', colors.blue },
--   [14] = { 'Constant', ' ', colors.cyan },
--   [15] = { 'String', ' ', colors.green },
--   [16] = { 'Number', ' ', colors.green },
--   [17] = { 'Boolean', '◩ ', colors.orange },
--   [18] = { 'Array', ' ', colors.blue },
--   [19] = { 'Object', ' ', colors.orange },
--   [20] = { 'Key', ' ', colors.red },
--   [21] = { 'Null', 'ﳠ ', colors.red },
--   [22] = { 'EnumMember', ' ', colors.green },
--   [23] = { 'Struct', ' ', colors.violet },
--   [24] = { 'Event', ' ', colors.violet },
--   [25] = { 'Operator', ' ', colors.green },
--   [26] = { 'TypeParameter', ' ', colors.green },
-- }

local g_highlights = {
  foldcolumn = {
    bold = false,
    bg = colors.bg1,
  },
  LineNr = {
    bold = false,
    fg = "#AB9766",
  },

  LineHighlight = {
    bg = colors.highlight
  },
  MatchParen = {
    fg = "#AB9766",
    bg = "#4B4D53",
  },

  rainbowcol1 = {
    bold = false,
    fg = "#FF7B72",
  },
  CurSearch = {
    --   fg = "#657085",
    --   bg = "#26333C",
    fg = "#AB9766",
    bg = "#4B4D53",
  },
  Search = {
    fg = "#AB9766",
    bg = "#4B4D53",
  },
  SpellBad = {
    underdouble = false,
    italic = true,
    reverse = false,
    undercurl = true,
    bold = false,
    -- fg = "#e95678",
    -- bg = "#e95678",
  },

  -- TSOperator = { -- NOTE: deprecated?
  --   -- bold = true,
  --   -- italic = true,
  --   fg = "#FF7B72",
  -- },
  -- TSVariableBuiltin = {
  --   -- bold = false,
  --   italic = true,
  --   -- fg = "#FF7B72",
  -- },
  -- TSFunction = {
  --   -- bold = true,
  --   -- italic = true,
  --   -- reverse = true,
  --   -- inverse = true,
  --   -- standout = true,
  --   -- fg = "#F5A97F",
  -- },

}

for k, v in pairs(g_highlights) do
  autocmd({ "Filetype" }, {
    pattern = "*",
    callback = function()
      hl(0, k, v)
    end,
  })
end

-- Highlight on yank
local highlight_group = augroup('YankHighlight', { clear = true })
autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

augroup("WinChange", { -- add/remove color/cursor columns when entering/leaving window
  clear = true,
})
autocmd({ "WinEnter" }, {
  pattern = "*",
  callback = function()
    opt_local.colorcolumn = "80" -- value of colorcolumn must be a string
    opt_local.cursorcolumn = true
  end,
  group = "WinChange",
})
autocmd({ "WinLeave" }, {
  pattern = "*",
  callback = function()
    opt_local.colorcolumn = "0"
    opt_local.cursorcolumn = false
  end,
  group = "WinChange",
})


-- lspsaga winbar autogroup
-- augroup("lspsaga_winbar", {
--   clear = true,
-- })

-- Highlights for lspsaga symbols in winbar
-- autocmd({ "Filetype" }, {
--   pattern = "*",
--   callback = function()
--
--     for _, v in pairs(kind) do
--       hl(0, 'LspSagaWinbar' .. v[1], { fg = v[3], bg = "#1E242E" })
--     end
--
--     hl(0, "LspSagaWinbarSep", {
--       fg = colors.separator,
--       bg = colors.bg2,
--     })
--     -- hl(0, "File", {
--     --   fg = colors.fg,
--     --   bg = colors.bg2,
--     -- })
--
--     hl(0, "Winbar", { -- winbar of current window
--       bg = colors.bg2,
--     })
--
--     -- hl(0, "WinbarNC", { -- winbar of not-current window
--     --   bg = colors.bg1,
--     -- })
--
--   end,
--   group = "lspsaga_winbar",
-- })



--[[

" define line highlight color
highlight LineHighlight ctermbg=darkgray guibg=darkgray
" highlight the current line
nnoremap <silent> <Leader>l :call matchadd('LineHighlight', '\%'.line('.').'l')<CR>
" clear all the highlighted lines
nnoremap <silent> <Leader>c :call clearmatches()<CR>

]] --



-- local map = vim.api.nvim_set_keymap
-- map("n", "<leader><leader>l", ":call matchadd('LineHighlight', '\\%'.line('.').'l')<CR>", { noremap = true })
