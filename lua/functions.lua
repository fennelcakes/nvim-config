-- Functions
local api = vim.api
-- local augroup = api.nvim_create_augroup
-- local autocmd = api.nvim_create_autocmd
local user_cmd = api.nvim_create_user_command
local map = vim.keymap.set
local noremap = { noremap = true }
-- local cmd = api.nvim_command
-- local hl = api.nvim_set_hl
local opt = vim.opt
-- local opt_local = vim.opt_local
-- local opt_global = vim.opt_global


local user_cmd = vim.api.nvim_create_user_command
user_cmd('BufCurOnly', 'execute "%bdelete|edit#|bdelete#"', { nargs = 0 }) -- Deletes all buffers but current


-- http://www.kevinli.co/posts/2017-01-19-multiple-cursors-in-500-bytes-of-vimscript/
--[[
-- function! SetupCR()
--   nnoremap <Enter> :nnoremap <lt>Enter> n@z<CR>q:<C-u>let @z=strpart(@z,0,strlen(@z)-1)<CR>n@z
-- endfunction
--
-- nnoremap cq :call SetupCR()<CR>*``qz
-- ]]



-- Pair Movement
-- Move around pairs backwards
vim.cmd([[
  function! MoveToPrevPairs()
  "   Get line number and column
      " let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
      " Comment out above line and uncomment line below to include < >
      let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>\|;'
      let [lnum, col] = searchpos(backsearch, 'bn')
  "    Move cursor
      call setpos('.', [0, lnum, col, 0])
  endfunction
  ]])

-- test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---


-- Move around pairs forward
vim.cmd([[
  function! MoveToNextPairs()
  "   Get line number and column
       " let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
      " Comment out above line and uncomment line below to include < >
      let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>\|;'
      let [lnum, col] = searchpos(forwardsearch, 'n')
  "    Move cursor
      call setpos('.', [0, lnum, col, 0])
  endfunction
  ]])

-- test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---



-- Toggle /Echo virtualedit
-- The ve toggle can be handled by Hydra's 'options' hydra (<leader>vo)
-- -- Trying to make a toggle for a non-boolean option
user_cmd(
  'VE',
  function(opts)
    local arg = string.lower
    if arg(opts.args) == "all" then
      opt.virtualedit = "none"
    elseif arg(opts.args) == "none" then
      opt.virtualedit = "all"
    else
      vim.cmd('echo "invalid value"')
    end
    vim.cmd("echo 'virtualedit ='&virtualedit")
  end,
  { nargs = 1 }
)
user_cmd(
  'V', 'execute \'VE \' . &virtualedit', { nargs = 0 }
)

user_cmd(
  'VT', 'let &ve=(&ve == "none" ? "all" : "none") | set ve', { nargs = 0 }
)
-- or, a singline line vimscript for this:
-- map('n', '<leader>;vt', ':let &ve=(&ve == "none" ? "all" : "none")<CR>:set ve<CR>', { noremap = true })

-- user command to alias ':PackerCompile' to ':PC'
user_cmd(
  'PC', 'PackerCompile', { nargs = 0 }
)



local command_center = require("command_center")

command_center.add({
  {
    desc = "Move to next pair char forward (insert mode)",
    cmd = "<ESC><CMD>call MoveToNextPairs()<CR>a",
    keys = { "i", "<M-n>", noremap }
  },
  {
    desc = "Move to next pair char backward (insert mode)",
    cmd = "<ESC><CMD>call MoveToPrevPairs()<CR>a",
    keys = { "i", "<M-e>", noremap }
  },
  {
    desc = "Move to next pair char forward (normal mode)",
    cmd = "<ESC><CMD>call MoveToNextPairs()<CR>",
    keys = { "n", "<M-n>", noremap }
  },
  {
    desc = "Move to next pair char backward (normal mode)",
    cmd = "<ESC><CMD>call MoveToPrevPairs()<CR>",
    keys = { "n", "<M-e>", noremap }
  },
  {
    desc = "Delete all buffers but current",
    cmd = "<CMD>BufCurOnly<CR>",
    keys = { "n", "<leader>bo", noremap }
  },
})

-- Make macros repeatable with cq then <CR> to repeat (instead of .) on
-- "search" terms.
vim.cmd([[function! SetupCR()
  nnoremap <Enter> :nnoremap <lt>Enter> n@z<CR>q:<C-u>let @z=strpart(@z,0,strlen(@z)-1)<CR>n@z
endfunction

nnoremap cq :call SetupCR()<CR>*``qz]])

--[[
a test of something more
a test of something more
a test of something more

]]
