-- Change info icons
local signs = { Error = " ", Warn = " ", Hint = " ", Info = "ﴞ " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end


local config = {
  virtual_text = true,
  signs = {
    active = signs,
  },
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = false,
    style = "minimal",
    border = "rounded",
    source = "always",
    header = "",
    prefix = "",
  },
}

vim.diagnostic.config(config)

-- TODO: Add to any lsp that supports textDocument/documentColor
-- local on_attach = function(client)
--   if client.server_capabilities.colorProvider then
--     -- Attach document colour support
--     require("document-color").buf_attach(bufnr, { mode = "background" })
--   end
-- end


-- simrat39/inlay-hints.nvim
local ih = require("inlay-hints")
local navic = require("nvim-navic")
local lsp_flags = {
  debounce_text_changes = 150,
}



local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  navic.attach(client, bufnr)
  ih.on_attach(client, bufnr)
end

local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())


require("mason").setup {}
require("mason-lspconfig").setup()
require("mason-lspconfig").setup_handlers({
  -- lua
  function(lua_language_server)
    require("lspconfig")[lua_language_server].setup {
      on_attach = on_attach,
      flags = lsp_flags,
      capabilities = capabilities,
      settings = {
        Lua = {
          hint = {
            enable = true,
          },
          diagnostics = {
            -- Get the language server to recognize the `vim` global and others
            globals = { 'vim', 'P', 'REQUIRE', 'R', 'it', 'describe', 'before_each' },
          },
        },
      }
    }
  end,

  -- Python
  -- function(python_language_server)
  --   require("lspconfig")[python_language_server].setup {
  --     on_attach = on_attach,
  --     flags = lsp_flags,
  --     capabilities = capabilities
  --   }
  -- end,

  -- JS
  function(typescript_language_server)
    require("lspconfig")[typescript_language_server].setup {
      on_attach = on_attach,
      flags = lsp_flags,
      capabilities = capabilities
    }
  end,

})

-- NOTE: ?? Don't know why this doesn't work in the mason-lspconfig.setup_handlers func
require('lspconfig').jsonls.setup {}

-- local null_ls = require("null-ls")
-- null_ls.setup({
--   sources = {
--     null_ls.builtins.diagnostics.eslint_d,
--     null_ls.builtins.code_actions.eslint_d,
--     null_ls.builtins.formatting.prettier
--   },
--   on_attach = on_attach
-- })
--



-- --
-- -- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
-- local capabilities = vim.lsp.protocol.make_client_capabilities()
-- capabilities.textDocument.completion.completionItem.snippetSupport = true
-- capabilities.textDocument.foldingRange = {
--   dynamicRegistration = false,
--   lineFoldingOnly = true
-- }
--
-- -- capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)
-- local capabilitiesSansFomatting = vim.lsp.protocol.make_client_capabilities()
-- capabilitiesSansFomatting.textDocument.documentSymbol = false
--
-- local on_attach = function()
--   -- print("LSP Attached")
-- end
--
--
-- local lspconfig = require('lspconfig')
--
-- lspconfig.bashls.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
--
-- lspconfig.clangd.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
--
-- lspconfig.cssls.setup {
--   capabilities = capabilities,
--
-- }
--
-- -- lspconfig.denols.setup {
-- --   capabilities = capabilities,
-- --   on_attach = on_attach
-- -- }
--
-- -- lspconfig.emmet_ls.setup {
-- --   capabilities = capabilities,
-- --   on_attach = on_attach
-- -- }
--
-- lspconfig.eslint.setup {
--   capabilities = capabilities,
--   on_attach = on_attach,
--   settings = {
--     codeAction = {
--       disableRuleComment = {
--         enable = true,
--         location = "separateLine"
--       },
--       showDocumentation = {
--         enable = true
--       }
--     },
--     codeActionOnSave = {
--       enable = false,
--       mode = "all"
--     },
--     format = true,
--     nodePath = "",
--     onIgnoredFiles = "off",
--     packageManager = "npm",
--     quiet = false,
--     rulesCustomizations = {},
--     run = "onType",
--     useESLintClass = false,
--     validate = "on",
--     workingDirectory = {
--       mode = "location"
--     }
--   }
-- }
--
--
-- lspconfig.gopls.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
--
-- lspconfig.html.setup {
--   capabilities = capabilities,
--   init_options = {
--     configurationSection = { "html", "css", "javascript" },
--     embeddedLanguages = {
--       css = true,
--       javascript = true
--     },
--     provideFormatter = true
--   },
--
-- }
--
-- lspconfig.jsonls.setup {
--   capabilities = capabilitiesSansFomatting,
--   on_attach = on_attach
-- }
-- lspconfig.marksman.setup {
--   capabilities = capabilities,
--
-- }
--
-- lspconfig.pyright.setup {
--   capabilities = capabilities,
--
-- }
-- lspconfig.sqls.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
--
-- }
-- lspconfig.sumneko_lua.setup {
--   capabilities = capabilities,
--   on_attach = on_attach,
--   settings = {
--     Lua = {
--       diagnostics = {
--         -- Get the language server to recognize the `vim` global and others
--         globals = { 'vim', 'P', 'REQUIRE', 'R', 'it', 'describe', 'before_each' },
--       },
--     },
--   },
-- }
--
-- lspconfig.tailwindcss.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
--
-- lspconfig.tsserver.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
--
-- lspconfig.vimls.setup {
--   capabilities = capabilities,
--   on_attach = on_attach
-- }
