-- {{ Automatically install packer
local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
  print("Installing packer close and reopen Neovim...")
  vim.cmd { cmd = "packadd packer.nvim" }
end

-- Update or sync Packer on write
local api = vim.api
local augroup = api.nvim_create_augroup
local autocmd = api.nvim_create_autocmd
local on_packer_write = function()
  vim.cmd { cmd = "source <afile>" }
  vim.cmd { cmd = "PackerSync" }
end

local packer_user_config = augroup("packer_user_config", { clear = true })
autocmd("BufwritePost", {
  pattern = 'plugins.lua',
  callback = on_packer_write,
  group = packer_user_config
})


local noremap = { noremap = true }
local command_center = require("command_center")
command_center.add({
  {
    desc = "Packer: Sync",
    cmd = ":PackerSync<CR>",
    keys = { "n", "<leader>;ps", noremap }
  },
  {
    desc = "Packer: Update (w/ Preview)",
    cmd = ":PackerUpdate --preview<CR>",
    keys = { "n", "<leader>;pp", noremap }
  }
})

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- }}

-- {{ Have packer use a popup window
-- local util = require("packer.util")
packer.init({
  -- snapshot = util.join_paths(vim.fn.stdpath("config"), "packer-lock.json"),
  -- snapshot_path = vim.fn.stdpath("config"),
  max_jobs = 10,
  display = {
    open_fn = function()
      return require("packer.util").float({ border = "rounded" })
    end,
  },
  log = { level = 'trace' }
})

-- }}


return require("packer").startup {
  function(use)

    -- copilot
    -- use { 'github/copilot.vim', }
    use { 'zbirenbaum/copilot.lua', }

    -- Loading
    use {
      'wbthomason/packer.nvim',
      'lewis6991/impatient.nvim',
      'olimorris/persisted.nvim',
    }
    -- use { "milkias17/reloader.nvim", requires = { { "nvim-lua/plenary.nvim" } } }
    -- use {
    --   'rmagatti/auto-session',
    --   commit = "9848758572a2d0fd2484c54edf63a8651a8a3d13",
    -- }
    -- use {
    --   'rmagatti/session-lens',
    -- }

    -- Colorschemes
    use {
      'navarasu/onedark.nvim',
      'EdenEast/nightfox.nvim',
      'sainnhe/everforest',
      'rmehri01/onenord.nvim',
      'folke/tokyonight.nvim',
      'rhysd/vim-color-spring-night',
      'rebelot/kanagawa.nvim',
      'ellisonleao/gruvbox.nvim',
      'Rigellute/rigel',
      'projekt0n/github-nvim-theme',
      'rose-pine/neovim',
    }
    use { "catppuccin/nvim", as = "catppuccin" }


    -- LSP etc.
    use {
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
      'neovim/nvim-lspconfig',
    }
    use {
      'glepnir/lspsaga.nvim',
      commit = "04e8167740c66193686ea3d14b511c7b160ea755", -- stable? at 8.0
      -- release
      -- commit = "bd95871d67b8942a7869f8284c151b85f653027b", -- stable pre 8.0
    }
    use {
      -- 'stevearc/aerial.nvim',
      'averms/black-nvim',
      -- 'p00f/clangd_extensions.nvim',
      -- 'folke/lsp-colors.nvim', -- Unsure if this is relevant, re-install if diagnostic hl groups don't look right
      -- 'j-hui/fidget.nvim',
      'onsails/lspkind.nvim',
      'simrat39/inlay-hints.nvim',
      -- 'lvimuser/lsp-inlayhints.nvim',
      'alexaandru/nvim-lspupdate',
      -- 'ray-x/lsp_signature.nvim',
      'jose-elias-alvarez/null-ls.nvim',
      'MunifTanjim/prettier.nvim',
      'ii14/emmylua-nvim', -- nvim lua completion library
      -- 'ziontee113/neo-minimap',
      'SmiteshP/nvim-navic',
      'narutoxy/dim.lua',
      'adoyle-h/lsp-toggle.nvim',
    }
    -- use { 'simrat39/symbols-outline.nvim' }


    -- Cmp
    use {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'hrsh7th/nvim-cmp',
      'hrsh7th/cmp-calc',
      'hrsh7th/cmp-nvim-lua',
      'hrsh7th/cmp-nvim-lsp-signature-help',
      -- 'amarakon/nvim-cmp-buffer-lines',
    }

    -- snippets
    use {
      'saadparwaiz1/cmp_luasnip',
      'rafamadriz/friendly-snippets',
    }
    use {
      'L3MON4D3/LuaSnip',
      -- commit = "a96cf3083934c794620d1e1ae2a7578f61b7dc1b"
    }



    -- Movement and text manipulation
    use {
      'gbprod/substitute.nvim',
      -- 'cshuaimin/ssr.nvim', -- structural search and replace
      -- 'tommcdo/vim-exchange', -- use substitute
      'windwp/nvim-autopairs',
      -- 'rhysd/clever-f.vim', -- leap takes care of this now
      'ggandor/flit.nvim',
      'terrortylor/nvim-comment',
      -- 'teranex/jk-jumps.vim',
      'booperlv/nvim-gomove',
      'junegunn/vim-easy-align',
      'jinh0/eyeliner.nvim',
      -- 'phaazon/hop.nvim',
      'ggandor/leap.nvim',
      'ggandor/leap-ast.nvim',

      'anuvyklack/hydra.nvim',
      'airblade/vim-matchquote',
      -- 'andymass/vim-matchup',
      'kylechui/nvim-surround',
      'obreitwi/vim-sort-folds',
      'mg979/vim-visual-multi',
      -- 'otavioschwanck/cool-substitute.nvim',
    }
    use {
      'ray-x/sad.nvim',
      requires = { 'ray-x/guihua.lua' }
    }
    use { '~/plugins/leap-spooky.nvim' }
    -- use { 'ggandor/leap-spooky.nvim',
    --   -- commit = "d436429619676b90aaccde0144cb2f64d6125680" -- yrr works
    --   -- commit = "1e58d4ef4b9408c4f6d2aa68ee397619ae579ab3" -- This one 'breaks' yrr when "V" and "v" maps are reversed
    -- }


    -- GUI-like enhancements
    use 'nvim-zh/colorful-winsep.nvim'
    -- Packer
    use({
      "folke/noice.nvim",
      -- commit = 'c14b0649c2356a4e86aeea63449859ee0b39ef6d',
      requires = {
        -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
        "MunifTanjim/nui.nvim",
        -- OPTIONAL:
        --   `nvim-notify` is only needed, if you want to use the notification view.
        --   If not available, we use `mini` as the fallback
        "rcarriga/nvim-notify",
      }
    })
    use {
      '~/plugins/command_center.nvim', -- to allow for "o" mode remaps
      -- 'FeiyouG/command_center.nvim',
      -- commit = "1a4cc439cb142664bda2a30307386056d02aa991",
    }
    use {
      'ntpeters/vim-better-whitespace',
      'Akianonymus/nvim-colorizer.lua',
      'mrshmllow/document-color.nvim',
      -- 'yamatsum/nvim-cursorline',
      'RRethy/vim-illuminate',
      'folke/zen-mode.nvim',
      'folke/twilight.nvim',
      'smjonas/inc-rename.nvim',
      -- 'lukas-reineke/indent-blankline.nvim',
      'ahmedkhalf/project.nvim',
      'petertriho/nvim-scrollbar',
      'kyazdani42/nvim-web-devicons',
      'doums/suit.nvim',
      'haringsrob/nvim_context_vt',
      -- 'toppair/peek.nvim',
    }
    use { 'kevinhwang91/nvim-ufo', requires = 'kevinhwang91/promise-async' }
    use {
      'MunifTanjim/nui.nvim',
      -- commit = "e9889bbd9919544697d497537acacd9c67d0de99",
    }


    -- Highlights etc
    use {
      'mvllow/modes.nvim',
      commit = '84260f8624abf09307e4c4206e03b96c68694a02',
      -- tag = "v0.2.0",
      -- Any commit past 84260 doesn't respect bg_d highlight 30Aug22. Maybe
      -- put this plugin or colorschemes in after/plugin
      -- 'mechatroner/rainbow_csv',
    }
    use {
      'jghauser/shade.nvim', -- fork
      -- 'sunjon/Shade.nvim',
      'levouh/tint.nvim', -- this has better options than Shade but messes
      -- with highlight groups too much
    }


    -- File Navigation
    use {
      'kyazdani42/nvim-tree.lua',
      'ThePrimeagen/harpoon',
    }

    -- Status/Buffer line/Winbar
    use { 'nvim-lualine/lualine.nvim',
      commit = "d2a727da9fe391f7839a81d39a6361d90146da1e",
    }
    use { 'akinsho/bufferline.nvim',
      -- commit = "c4dd9b4de03b891f648b098c25e4dc1bc48a13e5"
      -- disable = true,
    }
    use { 'tiagovla/scope.nvim' } -- allows buffers to be scoped to tabs w/ bufferline
    use { 'ghillb/cybu.nvim' } -- cycle buffers like Cmd+Tab on MacOS
    use { 'toppair/reach.nvim' } -- cycle buffers like Cmd+Tab on MacOS

    -- Window management
    use {
      'sindrets/winshift.nvim',
      's1n7ax/nvim-window-picker',
    }
    use { "anuvyklack/windows.nvim",
      requires = {
        "anuvyklack/middleclass",
        "anuvyklack/animation.nvim"
      },
    }

    -- Search tools
    use {
      -- Telescope
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
      'nvim-telescope/telescope-fzy-native.nvim',
      'nvim-telescope/telescope-ui-select.nvim',
      'nvim-telescope/telescope-packer.nvim',
      'princejoogie/dir-telescope.nvim',
      'LukasPietzschmann/telescope-tabs',
      'LinArcX/telescope-changes.nvim',
      'keyvchan/telescope-find-pickers.nvim',
      'Zane-/cder.nvim',
      'LinArcX/telescope-env.nvim',
      'jvgrootveld/telescope-zoxide',
      'AckslD/nvim-neoclip.lua',
      -- Other
      'edluffy/specs.nvim',
      'kkharji/sqlite.lua',
      'junegunn/vim-slash',
      'kevinhwang91/nvim-hlslens',
      'axkirillov/easypick.nvim',
    }
    use({
      'mrjones2014/dash.nvim',
      run = 'make install',
    })



    -- Quality of life improvements
    use {
      'wellle/targets.vim', -- NOTE: this might not play well with nvim-surround
      'Pocco81/auto-save.nvim',
      'famiu/bufdelete.nvim', -- Close buffers handles everything but delete hidden buffers (is_loaded but not visible or accessible with :bnext etc)
      'kazhala/close-buffers.nvim',
      'ethanholz/nvim-lastplace',
      'folke/todo-comments.nvim',
      'tpope/vim-repeat',
      'kevinhwang91/promise-async',
      'mbbill/undotree',
      'chentoast/marks.nvim',
      'glepnir/mcc.nvim',
      'folke/which-key.nvim',
      'nat-418/boole.nvim',
      'samjwill/nvim-unception',
      -- 'chaoren/vim-wordmotion',
      -- 'anuvyklack/vim-smartword',
      -- 'ja-ford/delaytrain.nvim'
    }
    use {
      '~/plugins/delaytrain.nvim',
      branch = 'func-mapping'
    }

    use { 'echasnovski/mini.nvim' }

    -- Diagnostics, debugging, etc.
    use {
      'mfussenegger/nvim-dap',
      "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap" },
      'mfussenegger/nvim-dap-python',
      'theHamsta/nvim-dap-virtual-text',
      'andrewferrier/debugprint.nvim',
      'WhoIsSethDaniel/toggle-lsp-diagnostics.nvim',
    }
    use {

      'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
      disable = false
    }
    use {
      'folke/trouble.nvim',
      -- 'arjunmahishi/run-code.nvim',
      -- See also: https://github.com/michaelb/sniprun, which could replace
      -- code_runner and lab.nvim
      'kevinhwang91/nvim-bqf',
    }
    -- Code runners
    use { 'CRAG666/code_runner.nvim', }
    use { '0x100101/lab.nvim', run = { 'cd js && npm ci' } }

    -- Treesitter
    use {
      'nvim-treesitter/nvim-treesitter',
      run = ":TSUpdate",
    }
    use {
      'nvim-treesitter/nvim-treesitter-context',
      -- 'nvim-treesitter/nvim-treesitter-textobjects',
      'RRethy/nvim-treesitter-textsubjects',
      'nvim-treesitter/playground',
      'windwp/nvim-ts-autotag',
      -- 'drybalka/tree-climber.nvim',
      'p00f/nvim-ts-rainbow',
      'David-Kunz/treesitter-unit',
      'mizlan/iswap.nvim',
      -- 'nvim-treesitter/nvim-treesitter-refactor',
    }
    use({
      'Wansmer/sibling-swap.nvim',
      requires = { 'nvim-treesitter' },
    })

    use({
      'Dkendal/nvim-treeclimber',
      requires = { 'rktjmp/lush.nvim' },
    })


    -- Git
    use {
      "lewis6991/gitsigns.nvim",
      config = function()
        require('gitsigns').setup()
        require("scrollbar.handlers.gitsigns").setup()
      end
    }
    use 'lewis6991/satellite.nvim'
    use {
      -- 'tpope/vim-fugitive',
      'dinhhuy258/git.nvim',
      'pwntester/octo.nvim',
    }
    use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' }
    use { 'sindrets/diffview.nvim',
      requires = 'nvim-lua/plenary.nvim' }
    -- commit = "a2945c82a58f23fba15c1b5319642fd6b2666df7" }



    -- Misc
    use {
      -- 'rcarriga/nvim-notify',
      -- 'doums/monark.nvim', -- Show icon next to cursor on mode switch
      -- 'subnut/nvim-ghost.nvim', -- type in browser via nvim
      'nullchilly/fsread.nvim',
      'numToStr/FTerm.nvim',
      'smjonas/live-command.nvim',
      'rktjmp/paperplanes.nvim', -- Publish code to paste bins
      'm-demare/attempt.nvim',
      'LintaoAmons/scratch.nvim',
      'Krafi2/jeskape.nvim',
      -- 'tamton-aquib/keys.nvim', -- Show keystroke like keycastr

      -- 'justinhj/battery.nvim',
      -- 'notomo/cmdbuf.nvim', -- too buggy but cool concept
    }
    -- use {
    --   'jghauser/follow-md-links.nvim'
    -- }
    -- use {
    --   "nvim-neorg/neorg",
    --   requires = { "nvim-lua/plenary.nvim",
    --     "nvim-neorg/neorg-telescope", }
    -- }
    -- use { 'ranjithshegde/orgWiki.nvim' }
    --
    use {
      'fennelcakes/opptogg.nvim' -- Use boole.nvim instead
      --   -- '~/plugins/opptogg.nvim'
    }

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if PACKER_BOOTSTRAP then
      require("packer").sync()
    end


  end
}
