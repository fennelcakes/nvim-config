-- cf "The Ultimate LuaSnip Tutorial for Beginners | Neovim Lua Snippet Engine"

local ls = require("luasnip") --{{{
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {} --}}}

local group = vim.api.nvim_create_augroup("Lua Snippets", { clear = true })
-- Set filetype here
local file_pattern = "*.tutorial"



-- Start LuaSnip tutorial

-- Using text node
-- s for snippet, t for text node, i for insert node
local myFirstSnippet = s("myFirstSnippet", {
  -- Text node can take a table as an argument
  t({ "--Hi! this is my snip", "--" }),
  -- Insert node takes two args: position and placeholder text
  i(1, "!!PLACEHOLDER!!"),
  t({ "", "--It has multiple lines" }),
})
table.insert(snippets, myFirstSnippet)


-- Format function (a useful snippet for LuaSnip snippets is used as an example)
-- Curly braces {} indicate cursor position (escape w/ double, e.g. {{}} = {})
local lsSnip = s("lsSnip", fmt([[
local {} = function({})
  {}
end
]], {
  -- These insert nodes correspond to the brackets {} in the function string
  -- Cursor is placed there by corresponding insert nodes.
  -- Cycle through insert nodes with keymap (alt-j, alt-k)
  -- above
  i(1, "!var!"),
  i(2, "!args!"),
  i(3, "--TODO:"),
}))
table.insert(snippets, lsSnip)


-- Choice nodes
-- A choice node takes a position and a table as its arguments.
local choiceNode = s(
  "choiceNode",
  fmt(
    [[
    local {} = {}
    ]],
    {
      i(1, "varName"),
      -- Use alt-h, alt-l to cycle through choices on this node
      -- Use a text node (t()) to have a pre-set choice, or an i() node to
      -- insert your own text (position 1 b/c order for children resets)
      c(2, { t("choice1"), i(1, "choice2") }),
    }
  )
)
table.insert(snippets, choiceNode)


-- Auto-snippets
local autoSnippet = s(";auto trigger;", { t("This was auto triggered") })
table.insert(autosnippets, autoSnippet)

-- Regex autotrigger
-- When regTrig=true, whatever is passed into the trigger is interpreted as
-- regex. (must use lua regex patterns, e.g. %w, not \w)
local regexAuto = s({ trig = "regdigit%d%d", regTrig = true }, { t("This was auto triggered") })
table.insert(autosnippets, regexAuto)
-- e.g. `regdigit99`


-- Function node (run a lua function that returns a string - must be a string)
local lsFunctD = s({ trig = "lsFunct[(%d)(%w)(%p)]", regTrig = true }, {
  f(function()
    local x = 2
    local y = 2
    local z = x + y
    return tostring(z)
  end)
})
table.insert(autosnippets, lsFunctD)
-- e.g. `lsFunct6`

-- function node with capture group
local lsFunctCap = s({ trig = "lsCap(%d+)(%a+)(%s)", regTrig = true },
  {
    t({ "--" }),
    f(
      function(_, snip) -- `snip` has methods
        return snip.captures[1] .. " + " -- returns contents of capture group 1
      end),
    f (
      function(_, snip)
        return snip.captures[2]
      end),
  })
table.insert(autosnippets, lsFunctCap)

--Manipulating insert nodes
local lsFArg = s({trig = "lsFArg"}, {
i(1, "varName"),
t(" = "),
f(function(arg)
  -- pass i-node 1[1] (its first element) as an argument
  return arg[1][1]:upper()
end, 1)
})
table.insert(autosnippets, lsFArg)
-- End LuaSnip tutorial

-- Repeat function (replicate a node)
local lsRep = s({trig = "lsRep"}, {
  i(1, "varName"),
  t(" = "),
  rep(1),
})
table.insert(autosnippets, lsRep)

-- Dynamic node


return snippets, autosnippets


-- Test Area

