local ls = require("luasnip") --{{{
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {} --}}}

local group = vim.api.nvim_create_augroup("Lua Snippets", { clear = true })
-- Set filetype here
local file_pattern = "*.lua"



-- Start Snippets


-- html comment <!--{}-->
local html_comm = s({trig = ";hcom", hidden = false}, {
  -- Text node can take a table as an argument
  t({ "<!--"}),
  -- Insert node takes two args: position and placeholder text
  i(1, ""),
  t("-->"),
})
table.insert(autosnippets, html_comm)

-- Jinja/Flask block
local jinj = s({trig = ";jinj", hidden = false}, {
  -- Text node can take a table as an argument
  t({ "{%"}),
  -- Insert node takes two args: position and placeholder text
  i(1, ""),
  t("%}"),
})
table.insert(autosnippets, jinj)

-- html tag codes
local html_tag = s({trig = ";htag", hidden = false}, {
  -- Text node can take a table as an argument
  t({ "&lt;"}),
  -- Insert node takes two args: position and placeholder text
  i(1, ""),
  t("&gt;"),
})

-- End Snippets

return snippets, autosnippets


-- Test Area


