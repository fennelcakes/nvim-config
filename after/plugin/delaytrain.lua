-- local map = vim.keymap.set
-- local map = vim.api.nvim_set_keymap
-- local noremap = {noremap = true}
-- map("n", "d", "h", noremap)
-- map("n", "t", "j", noremap)
-- map("n", "r", "k", noremap)
-- map("n", "n", "l", noremap)
--
-- map("n", "e", "l", noremap)
-- map("v", "e", "l", noremap)
--
-- map("v", "d", "j", noremap)
-- map("v", "t", "h", noremap)
-- map("v", "r", "l", noremap)
-- map("v", "n", "k", noremap)
--
-- vim.keymap.set("n", "<M-j>", function () print("Hello from <M-j>") end, noremap)
-- map("i", "<M-j>", "<Down>", noremap)



-- vim.keymap.set("n", "s", function ()
--   vim.notify("TEST")
-- end, { noremap = true })

require('delaytrain').setup {
  delay_ms = 1500, -- How long repeated usag of a key should be prevented
  grace_period = 5, -- How many repeated keypresse are allowed
  keys = { -- Which keys (in which modes) should be delayed
   ['nv'] = { 'h', 'j', 'k', 'l' },
    -- ['nv'] = { 'd', 't', 'r', 'n' },
    -- ['nv'] = { 'h', 'j', 'k', 'l','d', 't', 'r', 'e', 'n' },
    -- ['nv'] = {'s'},
    ['nvi'] = { '<Left>', '<Down>', '<Up>', '<Right>' },
    -- ['i'] = {'<M-j>'}
  },
}

