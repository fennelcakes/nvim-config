local api = vim.api
local hl = api.nvim_set_hl

-- Operators (+ - * / = > <)
hl(0, "@operator", { fg = "#FF7B72" })
-- Builtin variables (e.g. python self)
hl(0, "@variable.builtin", { italic = true })

-- HTML Tags
-- hl(0, "@tag", {fg  = "#EBCB8B" })
-- hl(0, "@attribute", {fg  = "#ED5E6A" })


