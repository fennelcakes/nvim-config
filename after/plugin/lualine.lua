local navic = require("nvim-navic")

local lualine = require('lualine')
local colors = {
  bg       = '#202328',
  fg       = '#bbc2cf',
  yellow   = '#ECBE7B',
  cyan     = '#008080',
  darkblue = '#081633',
  green    = '#98be65',
  orange   = '#FF8800',
  violet   = '#a9a1e1',
  magenta  = '#c678dd',
  blue     = '#51afef',
  red      = '#ec5f67',
}

-- local mode_map = {
--   ["n"] = "N",
--   ["no"] = "O·P",
--   ["nov"] = "O·P",
--   ["noV"] = "O·P",
--   ["no\22"] = "O·P",
--   ["niI"] = "N·I",
--   ["niR"] = "N·R",
--   ["niV"] = "N",
--   ["nt"] = "N·T",
--   ["v"] = "V",
--   ["vs"] = "V",
--   ["V"] = "V·L",
--   ["Vs"] = "V·L",
--   ["\22"] = "V·B",
--   ["\22s"] = "V·B",
--   ["s"] = "S",
--   ["S"] = "S·L",
--   ["\19"] = "S·B",
--   ["i"] = "I",
--   ["ic"] = "I·C",
--   ["ix"] = "I·X",
--   ["R"] = "R",
--   ["Rc"] = "R·C",
--   ["Rx"] = "R·X",
--   ["Rv"] = "V·R",
--   ["Rvc"] = "RVC",
--   ["Rvx"] = "RVX",
--   ["c"] = "C",
--   ["cv"] = "EX",
--   ["ce"] = "EX",
--   ["r"] = "R",
--   ["rm"] = "M",
--   ["r?"] = "C",
--   ["!"] = "SH",
--   ["t"] = "T",
-- }

-- local function modes()
--   return mode_map[vim.api.nvim_get_mode().mode] or "__"
-- end

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand('%:t')) ~= 1
  end,
  hide_in_width = function()
    return vim.fn.winwidth(0) > 80
  end,
  check_git_workspace = function()
    local filepath = vim.fn.expand('%:p:h')
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,
}


local ns = vim.api.nvim_create_namespace('showcmd_msg')
local showcmd_msg
vim.ui_attach(ns, { ext_messages = true, ext_cmdline = false }, function(event, ...)
  if event == 'msg_showcmd' then
    local content = ...
    showcmd_msg = #content > 0 and content[1][2] or ''
  end
end)


local config = {
  options = {
    icons_enabled = true,
    theme = 'auto',
    -- component_separators = { left = '', right = '' },
    component_separators = { nil },
    section_separators = { left = '', right = '' },
    disabled_filetypes = {},
    always_divide_middle = true,
    globalstatus = true, -- enable global statusline (have a single statusline
    -- at bottom of neovim instead of one for  every window).
  },
  sections = {
    -- lualine_a = { modes },
    lualine_a = { 'mode' },
    lualine_b = {
      { 'branch',
        color = { fg = colors.blue, gui = 'bold' },
      },
      { 'diff',
        -- Is it me or the symbol for modified us really weird
        symbols = { added = ' ', modified = '柳 ', removed = ' ' },
        diff_color = {
          added = { fg = colors.green },
          modified = { fg = colors.orange },
          removed = { fg = colors.red },
        },
        cond = conditions.hide_in_width,

      },
    },
    -- Insert lualin_c with parameters for filetype, with icon only option, and filename
    lualine_c = {
      -- Display number of loaded buffers
      { function()
        local is_loaded = vim.api.nvim_buf_is_loaded
        local tbl = vim.api.nvim_list_bufs()
        local loaded_bufs = 0
        for i = 1, #tbl do
          if is_loaded(tbl[i]) then
            loaded_bufs = loaded_bufs + 1
          end
        end
        return loaded_bufs
      end,
        icon = '﬘',
        color = { fg = 'DarkCyan', gui = 'bold' },
      },
      {
        require("noice").api.statusline.message.get,
        cond = require("noice").api.statusline.message.has,
      },
    },
    lualine_x = {
      {
        require("noice").api.statusline.command.get,
        cond = require("noice").api.statusline.command.has,
        color = { fg = "#ff9e64" },
      },
      {
        require("noice").api.statusline.mode.get,
        cond = require("noice").api.statusline.mode.has,
        color = { fg = "#ff9e64" },
      },
      {
        require("noice").api.statusline.search.get,
        cond = require("noice").api.statusline.search.has,
        color = { fg = "#ff9e64" },
      },
      { function() return showcmd_msg end },
      { 'diagnostics' },
      {
        function() -- displays whether LSP diagnostics are on or off
          local is_lsp_diag_loaded = require("toggle_lsp_diagnostics").settings.all
          if is_lsp_diag_loaded then
            return ""
          else
            return ""
          end
        end,
        color = {
          fg = '#C972E4',
          -- fg = '#008B8B',
          -- bg = '#1E242E',
        },
        on_click = function()
          vim.api.nvim_command("Trouble document_diagnostics")
        end
      },
    },
    lualine_y = { 'encoding', 'fileformat' },

    lualine_z = {
      {
        'location', icons_enabled = true,
        icon = '',
        max_length = vim.o.columns * 2 / 3
      },

      { 'os.date("%H:%M")' },
    }

  },

  -- inactive_sections = {
  --   lualine_a = {},
  --   lualine_b = {},
  --   lualine_c = {},
  --   lualine_x = {},
  --   lualine_y = {},
  --   lualine_z = {}
  -- },
  tabline = {},
  extensions = { 'nvim-tree', 'symbols-outline' },
  winbar = {
    lualine_a = {
      { 'filetype',
        icon_only = true,
        color = {
          fg = '#bbc2cf',
          bg = '#1E242E',
        },
      },
      { 'filename',
        file_status = true,
        color = {
          fg = '#bbc2cf',
          bg = '#1E242E',
        },
      },
    },
    lualine_b = {
      { navic.get_location, cond = navic.is_available,
        color = {
          fg = '#bbc2cf',
          bg = '#1E242E',
        },
      },
    },
    -- lualine_c = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_x = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_y = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_z = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- }
  },
  --
  inactive_winbar = {
    lualine_a = {
      { 'filetype',
        icon_only = true,
        color = {
          fg = '#bbc2cf',
          bg = '#1E242E',
        },
      },
      { 'filename',
        file_status = true,
        color = {
          fg = '#bbc2cf',
          bg = '#1E242E',
        },
      },
    },
    lualine_b = {
      { navic.get_location, cond = navic.is_available },
    },
    -- lualine_c = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_x = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_y = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- },
    -- lualine_z = {
    --   color = {
    --     fg = '#bbc2cf',
    --     bg = '#1E242E',
    --   },
    -- }
  }
}


-- Inserts a component in lualine_c at left section
local function ins_left(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
  -- Lsp server name .
  function()
    local msg = 'No Active Lsp'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients()
    if next(clients) == nil then
      return msg
    end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = '',
  color = { fg = 'DarkCyan', gui = 'bold' },
}


-- Now don't forget to initialize lualine
lualine.setup(config)
