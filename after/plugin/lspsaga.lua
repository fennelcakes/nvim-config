local saga = require 'lspsaga'

saga.init_lsp_saga({
  max_preview_lines = 1000,
  -- rename_in_select = false,
  symbol_in_winbar = {
    in_custom = true,
    show_file = true,
    separator = '  ',
  },

  show_outline = {
    win_position = 'right',
    win_width = 60,
    auto_enter = true,
    auto_preview = true,
    virt_text = '┃',
    jump_key = 'o',
    -- auto refresh when change buffer
    auto_refresh = true,
  },
  -- "single" | "double" | "rounded" | "bold" | "plus"
  border_style = "rounded",

})




local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  -- Lspsaga
  {
    desc = "Saga Toggle Outline",
    cmd = "<cmd>LSoutlineToggle<CR>",
    keys = { "n", "<Leader>a", noremap }
  },
  {
    desc = "Saga Hover Doc",
    cmd = "<cmd>Lspsaga hover_doc<CR>",
    keys = { "n", "K", noremap }
  },
  -- use lsp_finder command (gh)
  -- {
  --   desc = "Saga Implementation",
  --   cmd = "<cmd>Lspsaga implement<CR>",
  --   keys = { "n", "<leader>gi", noremap }
  -- },
  {
    desc = "Saga Finder",
    cmd = "<cmd>Lspsaga lsp_finder<CR>",
    -- cmd = function()
    --   require('lspsaga.finder').lsp_finder()
    -- end,
    keys = { "n", "gh", noremap }
  },
  {
    desc = "Saga Code Action (normal)",
    cmd = "<cmd>Lspsaga code_action<CR>",
    -- cmd = function()
    --   require('lspsaga.codeaction').code_action()
    -- end,
    keys = { "n", "<leader>ca", noremap }
  },
  -- {
  --   desc = "Saga Code Action (visual)",
  --   cmd = function()
  --     vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<C-U>", true, false, true))
  --     require("lspsaga.codeaction").range_code_action()
  --   end,
  --   keys = { "v", "<leader>ca", noremap }
  -- },
  {
    desc = "Saga Rename",
    cmd = "<cmd>Lspsaga rename<CR>",
    keys = { "n", "<leader>rn", noremap } -- Use IncRename instead (gr)
  },
  {
    desc = "Saga Definition Peek/Preview",
    cmd = "<cmd>Lspsaga peek_definition<CR>",
    keys = { "n", "gd", noremap }
  },
  {
    desc = "Saga Line Diagnostics",
    cmd = function()
      require('lspsaga.diagnostic').show_line_diagnostics()
    end,
    keys = { "n", "gl", noremap }
  },
  {
    desc = "Saga Jump Diagnostic Previous",
    cmd = function()
      require("lspsaga.diagnostic").goto_prev()
    end,
    -- cmd = "<cmd>Lspsaga diagonstic_jump_prev<CR>",
    keys = { "n", "[e", noremap }
  },
  {
    desc = "Saga Jump Diagnostic Next",
    cmd = function()
      require("lspsaga.diagnostic").goto_next()
    end,
    -- cmd = "<cmd>Lspsaga diagonstic_jump_next<CR>",
    keys = { "n", "]e", noremap }
  },
  {
    desc = "Saga Jump Diagnostic (Severe) Previous",
    cmd = function()
      require("lspsaga.diagnostic").goto_prev({ severity = vim.diagnostic.severity.ERROR })
    end,
    keys = { "n", "[E", noremap }
  },
  {
    desc = "Saga Jump Diagnostic (Severe) Next",
    cmd = function()
      require("lspsaga.diagnostic").goto_next({ severity = vim.diagnostic.severity.ERROR })
    end,
    keys = { "n", "]E", noremap }
  },
  {
    desc = "Saga Open Float Terminal",
    cmd = "<cmd>Lspsaga open_floaterm<CR>",
  },
  {
    desc = "Saga Close Float Terminal",
    cmd = [[<C-\><C-n><cmd>Lspsaga close_floaterm<CR>]],
  },
})



-- Winbar Symbols Support
-- -- Example:
-- local function get_file_name(include_path)
--   local file_name = require('lspsaga.symbolwinbar').get_file_name()
--   if vim.fn.bufname '%' == '' then return '' end
--   if include_path == false then return file_name end
--   -- Else if include path: ./lsp/saga.lua -> lsp > saga.lua
--   local sep = vim.loop.os_uname().sysname == 'Windows' and '\\' or '/'
--   local path_list = vim.split(string.gsub(vim.fn.expand '%:~:.:h', '%%', ''), sep)
--   local file_path = ''
--   for _, cur in ipairs(path_list) do
--     file_path = (cur == '.' or cur == '~') and '' or
--         file_path .. cur .. ' ' .. '%#LspSagaWinbarSep#>%*' .. ' %*'
--   end
--   return file_path .. file_name
-- end
--
-- local function config_winbar()
--   local exclude = {
--     ['teminal'] = true,
--     ['toggleterm'] = true,
--     ['prompt'] = true,
--     ['NvimTree'] = true,
--     ['help'] = true,
--   } -- Ignore float windows and exclude filetype
--   if vim.api.nvim_win_get_config(0).zindex or exclude[vim.bo.filetype]
--   then
--     vim.wo.winbar = ''
--   else
--     local ok, lspsaga = pcall(require, 'lspsaga.symbolwinbar')
--     local sym
--     if ok then sym = lspsaga.get_symbol_node() end
--     local win_val = ''
--     win_val = get_file_name(false) -- set to true to include path
--     if sym ~= nil then win_val = win_val .. sym end
--     vim.wo.winbar = win_val
--   end
-- end
--
-- local events = { 'BufEnter', 'BufWinEnter', 'CursorMoved' }
--
-- vim.api.nvim_create_autocmd(events, {
--   pattern = { '*' },
--   callback = function() config_winbar() end,
-- })
--
-- vim.api.nvim_create_autocmd('User', {
--   pattern = 'LspsagaUpdateSymbol',
--   callback = function() config_winbar() end,
-- })
