local status_ok, tabline = pcall(require, "tabline")
if not status_ok then
  return
end

tabline.setup {
  -- Defaults configuration options
  enable = true,
  options = {
    -- If lualine is installed tabline will use separators configured in lualine by default.
    -- These options can be used to override those settings.
    section_separators = { '▎', '▎' },
    component_separators = { '|', '|' },
    max_bufferline_percent = 66, -- set to nil by default, and it uses vim.o.columns * 2/3
    show_tabs_always = true, -- this shows tabs only when there are more than one tab or if the first tab is named
    show_devicons = true, -- this shows devicons in buffer section
    show_bufnr = false, -- this appends [bufnr] to buffer section,
    show_filename_only = true, -- shows base filename only instead of relative path in filename
    modified_icon = "●", -- change the default modified icon
    modified_italic = true, -- set to true by default; this determines whether the filename turns italic if modified
    show_tabs_only = false, -- this shows only tabs instead of tabs + buffers
  }
}

-- vim.cmd [[
--       set guioptions-=e " Use showtabline in gui vim
--       set sessionoptions+=tabpages,globals " store tabpages and globals in session
--     ]]

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  {
    desc = "Move to next buffer in the tabline.",
    cmd = "<CMD>TablineBufferNext<CR>",
    keys = { "n", "<S-l>", noremap }
  },
  {
    desc = "Move to previous buffer in the tabline.",
    cmd = "<CMD>TablineBufferPrevious<CR>",
    keys = { "n", "<S-h>", noremap }
  },
  {
    desc = "Bind the current tab's buffers to these files.",
    cmd = "<CMD>TablineBuffersBind %<CR>",
    keys = { "n", "<leader>tb", noremap }
  },
  {
    desc = "Clear the binding of current tab's buffers.",
    cmd = "<CMD>TablineBuffersClearBind<CR>",
    keys = { "n", "<leader>tC", noremap }
  },
  {
    desc = "Open a new tab with these files.",
    cmd = "<CMD>TablineTabNew ",
    keys = { "n", "<leader>tN", noremap }
  },
  {
    desc = "Rename current tab's name.",
    cmd = "<CMD>TablineTabRename",
    keys = { "n", "<leader>tr", noremap }
  },
  {
    desc = "Toggles whether to show all buffers that are open versus only buffers that are currently visible or bound.",
    cmd = "<CMD>TablineToggleShowAllBuffers<CR>",
    keys = { "n", "<leader>ta", noremap }
  },
})
