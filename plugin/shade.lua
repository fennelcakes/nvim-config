-- NOTE: This repo is a fork with some updates, but neither this not the
-- original are being maintained, in favor of tint.nvim
-- We use this because tint doesn't play well with modes.nvim

require 'shade'.setup({
  zindex = 50,
  overlay_opacity = 50,
  opacity_step = 1,
  keys = {
    -- brightness_up    = '<C-Up>',
    -- brightness_down  = '<C-Down>',
    toggle           = '<Leader>S',
  },
  exclude_filetypes = { "NvimTree", "DiffviewFiles"}
})
