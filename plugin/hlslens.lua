local kopts = { noremap = true, silent = true }

vim.api.nvim_set_keymap('n', 'n',
  [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]],
  kopts)
vim.api.nvim_set_keymap('n', 'N',
  [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]],
  kopts)
vim.api.nvim_set_keymap('n', '*', [[*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', '#', [[#<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g*', [[g*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g#', [[g#<Cmd>lua require('hlslens').start()<CR>]], kopts)

require('hlslens').setup({
  {
    auto_enable        = {
      description = [[Enable nvim-hlslens automatically]],
      default = true
    },
    enable_incsearch   = {
      description = [[When `incsearch` option is on and enable_incsearch is true, add lens
            for the current matched instance]],
      default = true
    },
    calm_down          = {
      description = [[When the cursor is out of the position range of the matched instance
            and calm_down is true, clear all lens]],
      default = true,
    },
    nearest_only       = {
      description = [[Only add lens for the nearest matched instance and ignore others]],
      default = false
    },
    nearest_float_when = {
      description = [[When to open the floating window for the nearest lens.
            'auto': floating window will be opened if room isn't enough for virtual text;
            'always': always use floating window instead of virtual text;
            'never': never use floating window for the nearest lens]],
      default = 'auto',
    },
    float_shadow_blend = {
      description = [[Winblend of the nearest floating window. `:h winbl` for more details]],
      default = 50,
    },
    virt_priority      = {
      description = [[Priority of virtual text, set it lower to overlay others.
        `:h nvim_buf_set_extmark` for more details]],
      default = 100,
    },
    override_lens      = {
      description = [[Hackable function for customizing the lens. If you like hacking, you
            should search `override_lens` and inspect the corresponding source code.
            There's no guarantee that this function will not be changed in the future. If it is
            changed, it will be listed in the CHANGES file.
            @param render table an inner module for hlslens, use `setVirt` to set virtual text
            @param splist table (1,1)-indexed position
            @param nearest boolean whether nearest lens
            @param idx number nearest index in the plist
            @param relIdx number relative index, negative means before current position,
                                  positive means after
        ]],
      default = nil
    },
  }
})

--
-- -- Integrate with visual-multi
-- local api = vim.api
-- local augroup = api.nvim_create_augroup
-- local autocmd = api.nvim_create_autocmd
-- local cmd = api.nvim_command
-- local hl = api.nvim_set_hl
-- --
-- -- vim.cmd([[
-- --     aug VMlens
-- --         au!
-- --         au User visual_multi_start lua require('vmlens').start()
-- --         au User visual_multi_exit lua require('vmlens').exit()
-- --     aug END
-- -- ]])
--
--
-- local hlslens_hi = {
--   -- HlSearchNear = {
--   --   fg = "#008B8B",
--   -- },
--   HlSearchLens = {
--     fg = "#5AB0F6",
--   },
--   HlSearchLensNear =
--   {
--     fg = "#008B8B",
--   },
--   HlSearchFloat =
--   {
--     fg = "#008B8B",
--   },
-- }
--
-- for k, v in pairs(hlslens_hi) do
--   autocmd({ "Filetype" }, {
--     pattern = "*",
--     callback = function()
--       hl(0, k, v)
--     end,
--   })
-- end
--
-- augroup("VMlens", {
--   clear = true,
-- })
-- autocmd({ "User" }, {
--   pattern = "visual_multi_start",
--   callback = function()
--     cmd("lua require('vmlens').start()")
--   end,
--   group = "VMlens"
-- })
-- autocmd({ "User" }, {
--   pattern = "visual_multi_exit",
--   callback = function()
--     cmd("lua require('vmlens').exit()")
--   end,
--   group = "VMlens"
-- })
