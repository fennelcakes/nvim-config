require'sad'.setup({
  diff = 'diff-so-fancy', -- you can use `diff`, `diff-so-fancy`, 'delta'
  ls_file = 'fd', -- also git ls_file
  exact = false, -- exact match
  vsplit = true, -- split sad window the screen vertically, when set to number
  -- it is a threadhold when window is larger than the threshold sad will split vertically,
  height_ratio = 0.6, -- height ratio of sad window when split horizontally
  width_ratio = 0.6, -- height ratio of sad window when split vertically

})


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Sad: PROJECT replace selection",
    cmd = "<CMD>Sad<CR>",
    -- keys = { "n", ",s", noremap }
  },
  {
    desc = "Sad: <oldtext> <newtext>",
    cmd = ":Sad ",
    -- keys = { {"n", "x"}, ",sg", noremap }
  },
})

