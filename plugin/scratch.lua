require("scratch").setup {
  scratch_file_dir = vim.fn.stdpath("cache") .. "/scratch.nvim", -- Where the scratch files will be saved
  filetypes = { "json", "xml", "go", "lua", "js", "py", "sh" }, -- filetypes to select from
}


local noremap = { noremap = true }
local command_center = require("command_center")
command_center.add({
  -- Write/Quit/Source
  {
    desc = "Scratch: New",
    cmd = function() require("scratch").scratch() end,
    keys = { "n", "<M-S-s>", noremap }
  },
  {
    desc = "Scratch: Previous (Old)",
    cmd = function() require("scratch").openScratch() end,
    keys = { "n", "<M-S-p>", noremap }
  },
})
