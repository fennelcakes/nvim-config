-- :FSRead " Flow state visual range
-- :FSClear " Clear all flow states
-- :FSToggle " Toggle flow state

-- Config

vim.g.flow_strength = 0.7 -- low: 0.3, middle: 0.5, high: 0.7 (default)
vim.g.skip_flow_default_hl = true -- If you want to override default highlights
vim.api.nvim_set_hl(0, "FSPrefix", { fg = "#cdd6f4" })
vim.api.nvim_set_hl(0, "FSSuffix", { fg = "#6C7086" })


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  {
    desc = "Flow State: Clear all",
    cmd = "<CMD>FSClear<CR>",
    -- keys = { "n", "<leader>;q", noremap }
  },
  {
    desc = "Flow State: visual Range",
    cmd = "<CMD>FSRead<CR>",
    -- keys = { "n", "<leader>;q", noremap }
  },
  {
    desc = "Flow State: Toggle",
    cmd = "<CMD>FSRead<CR>",
  },
})
