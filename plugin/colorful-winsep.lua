require("colorful-winsep").setup({
  -- highlight for Window separator
  highlight = {
    guibg = "#16161E",
    guifg = "#1F3442",
  },
  -- timer refresh rate
  interval = 30,
  -- This plugin will not be activated for filetype in the following table.
  no_exec_files = { "packer", "TelescopePrompt", "mason", "CompetiTest", "NvimTree" },
  -- Symbols for separator lines, the order: horizontal, vertical, top left, top right, bottom left, bottom right.
  symbols = { "━", "┃", "┏", "┓", "┗", "┛" },
  close_event = function()
    -- Executed after closing the window separator
  end,
  create_event = function()
    -- Executed after creating the window separator
  end,
})


-- -- Disable for nvim-tree
-- create_event = function()
--   if fn.winnr('$') == 3 then
--     local win_id = fn.win_getid(vim.fn.winnr('h'))
--     local filetype = api.nvim_buf_get_option(api.nvim_win_get_buf(win_id), 'filetype')
--     if filetype == "NvimTree" then
--       colorful_winsep.NvimSeparatorDel()
--     end
--   end
-- end
