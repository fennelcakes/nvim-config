require 'telescope-tabs'.setup {
  show_preview = true,
  close_tab_shortcut_i = '<C-d>', -- if you're in insert mode
  close_tab_shortcut_n = 'D', -- if you're in normal mode
  entry_formatter = function(tab_id, buffer_ids, file_names, file_paths)
    local entry_string = table.concat(file_names, ', ')
    return string.format('%d: %s', tab_id, entry_string)
  end,
  entry_ordinal = function(tab_id, buffer_ids, file_names, file_paths)
    return table.concat(file_names, ' ')
  end,
}


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Telescope tabs",
    cmd = "<cmd>:Telescope telescope-tabs list_tabs<CR>",
    keys = { "n", "<leader>f,t", noremap }
  }
})
