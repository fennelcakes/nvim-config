local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
  return
end

local themes = require("telescope.themes")
local actions = require "telescope.actions"

telescope.setup {
  defaults = themes.get_ivy({
    wrap_results = false,
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
    },
    prompt_prefix = " ",
    selection_caret = " ",
    path_display = { "truncate" },
    winblend = 0,
    color_devicons = true,
    layout_strategy = "horizontal",
    layout_config = {
      -- horizontal = {
      --   -- prompt_position = "bottom",
      --   preview_width = 0.55,
      --   results_width = 0.8,
      -- },
      -- vertical = {
      --   mirror = false,
      -- },

      horizontal = {
        preview_width = 90,
        -- preview_width = function(_, cols, _)
        --   if cols > 200 then
        --     return math.floor(cols * 0.5)
        --   else
        --     return math.floor(cols * 0.6)
        --   end
        -- end,
      },
      vertical = {
        preview_width = 0.65,
        width = 0.9,
        height = 0.95,
        preview_height = 0.5,
      },

      flex = {
        preview_width = 0.65,
        horizontal = {
          -- preview_width = 0.9,
        },
      },

      width = 0.99,
      height = 0.99,
      anchor = "S",
      prompt_position = "top",
      -- prompt_position = "bottom",
      preview_cutoff = 20,
      -- preview_cutoff = 120,
    },
    border = {},
    borderchars = {
      { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
      prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
      results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
      preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
    },
    set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
    file_previewer = require("telescope.previewers").vim_buffer_cat.new,
    grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
    qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
    mappings = {
      i = {
        ["<C-n>"] = actions.cycle_history_next,
        ["<C-p>"] = actions.cycle_history_prev,

        -- ["<C-j>"] = actions.move_selection_next,
        -- ["<C-k>"] = actions.move_selection_previous,

        ["<C-c>"] = actions.close,

        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,

        ["<CR>"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,

        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,

        ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
      },

      n = {
        ["<esc>"] = actions.close,
        ["q"] = actions.close,
        ["<CR>"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,

        ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,

        ["j"] = actions.move_selection_next,
        ["k"] = actions.move_selection_previous,
        ["H"] = actions.move_to_top,
        ["M"] = actions.move_to_middle,
        ["L"] = actions.move_to_bottom,

        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,
        ["gg"] = actions.move_to_top,
        ["G"] = actions.move_to_bottom,

        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down
      },
    },
  }),
  pickers = {
    -- Default configuration for builtin pickers goes here:
    -- picker_name = {
    --   picker_config_key = value,
    --   ...
    -- }
    -- Now the picker_config_key will be applied every time you call this
    -- builtin picker
  },
  extensions = {
    cder = {
      previewer_command =
        'exa '..
        '-a '..
        '--color=always '..
        '-T '..
        '--level=3 '..
        '--icons '..
        '--git-ignore '..
        '--long '..
        '--no-permissions '..
        '--no-user '..
        '--no-filesize '..
        '--git '..
        '--ignore-glob=.git',
    },
    ["ui-select"] = {
      themes.get_dropdown {
        -- even more opts
        width = 0.8,
        previewer = false,
        prompt_title = false,
        borderchars = {
          { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
          prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
          results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
          preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
        },
      }
    },

    -- fzf = {
    --   --[[
    --   | Token     | Match type                 | Description                          |
    --   | --------- | -------------------------- | ------------------------------------ |
    --   | `sbtrkt`  | fuzzy-match                | Items that match `sbtrkt`            |
    --   | `'wild`   | exact-match (quoted)       | Items that include `wild`            |
    --   | `^music`  | prefix-exact-match         | Items that start with `music`        |
    --   | `.mp3$`   | suffix-exact-match         | Items that end with `.mp3`           |
    --   | `!fire`   | inverse-exact-match        | Items that do not include `fire`     |
    --   | `!^music` | inverse-prefix-exact-match | Items that do not start with `music` |
    --   | `!.mp3$`  | inverse-suffix-exact-match | Items that do not end with `.mp3`    |
    --   ]]
    --   fuzzy = true, -- false will only do exact matching
    --   override_generic_sorter = true, -- override the generic sorter
    --   override_file_sorter = true, -- override the file sorter
    --   case_mode = "smart_case", -- or "ignore_case" or "respect_case"
    --   -- the default case_mode is "smart_case"
    -- },
    packer = {
      theme = "ivy",
      layout_config = {
        height = .5
      }
    },

    themes = {},
    terms = {},
  },
  preview = {
  }
}

require("telescope").load_extension("ui-select")
require('telescope').load_extension('fzy_native')
require('telescope').load_extension('env')
require('telescope').load_extension('cder')
require('telescope').load_extension('changes')
require('telescope').load_extension('find_pickers')
-- require("telescope").load_extension("fzf")


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- List Pickers
  {
    desc = "Telescope: find all pickers",
    cmd = "<CMD>lua require 'telescope'.extensions.find_pickers.find_pickers()<CR>",
    keys = { "n", "<leader>f;P", noremap }
  },
  {
    desc = "Telescope: changes",
    cmd = "<cmd>Telescope changes<CR>",
    keys = { "n", "<leader>fC", noremap }
  },
  {
    desc = "Telescope: builtin pickers",
    cmd = require('telescope.builtin').builtin,
    keys = { "n", "<leader>f;", noremap }
  },

  -- File Pickers
  {
    desc = "Telescope: Change Directory (cder)",
    cmd = "<CMD>Telescope cder<CR>",
    keys = { "n", "<leader>cd", noremap }
  },
  {
    desc = "Telescope: find files in working dir",
    cmd = require('telescope.builtin').find_files,
    keys = { "n", "<leader>ff", noremap }
  },
  {
    desc = "Telescope: search git files",
    cmd = require('telescope.builtin').git_files,
    keys = { "n", "<leader>fGf", noremap }
  },
  {
    desc = "Telescope: grep string under cursor",
    cmd = require('telescope.builtin').grep_string,
    keys = { "n", "<leader>f?", noremap }
  },
  {
    desc = "Telescope: grep a string in working dir",
    cmd = require('telescope.builtin').live_grep,
    keys = { "n", "<leader>fl", noremap }
  },
  -- Vim Pickers
  {
    desc = "Telescope: open buffers",
    cmd = require('telescope.builtin').buffers,
    keys = { "n", "<leader>fb", noremap }
  },
  {
    desc = "Telescope: recent files",
    cmd = require('telescope.builtin').oldfiles,
    keys = { "n", "<leader>fo", noremap }
  },
  {
    desc = "Telescope: tags in current dir",
    cmd = require('telescope.builtin').tags,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: available commands",
    cmd = require('telescope.builtin').commands,
    keys = { "n", "<leader>fc", noremap }
  },
  {
    desc = "Telescope recent commands: ",
    cmd = require('telescope.builtin').command_history,
    keys = { "n", "<leader>f;h", noremap }
  },
  {
    desc = "Telescope: search history",
    cmd = require('telescope.builtin').search_history,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: search history",
    cmd = require('telescope.builtin').search_history,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: help tags",
    cmd = require('telescope.builtin').help_tags,
    keys = { "n", "<leader>fh", noremap }
  },
  {
    desc = "Telescope: marks",
    cmd = require('telescope.builtin').marks,
    keys = { "n", "<leader>f\'", noremap }
  },
  {
    desc = "Telescope: color schemes",
    cmd = require('telescope.builtin').colorscheme,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: quickfix",
    cmd = require('telescope.builtin').quickfix,
    keys = { "n", "<leader>fq", noremap }
  },
  {
    desc = "Telescope: quickfix history",
    cmd = "<CMD>Telescope quickfixhistory<CR>",
    keys = { "n", "<leader>fQ", noremap }
  },
  {
    desc = "Telescope: loclist",
    cmd = require('telescope.builtin').loclist,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: jumplist",
    cmd = require('telescope.builtin').jumplist,
    keys = { "n", "<leader>fj", noremap }
  },
  {
    desc = "Telescope: vim options",
    cmd = require('telescope.builtin').vim_options,
    keys = { "n", "<leader>fvo", noremap }
  },
  {
    desc = "Telescope: registers",
    cmd = require('telescope.builtin').registers,
    keys = { "n", "<leader>f\"", noremap }
  },
  {
    desc = "Telescope: autocommands",
    cmd = require('telescope.builtin').autocommands,
    keys = { "n", "<leader>fau", noremap }
  },
  {
    desc = "Telescope: spelling suggest",
    cmd = require('telescope.builtin').spell_suggest,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: keymaps",
    cmd = require('telescope.builtin').keymaps,
    keys = { "n", "<leader>fk", noremap }
  },
  {
    desc = "Telescope: filetypes",
    cmd = require('telescope.builtin').filetypes,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: highlights",
    cmd = require('telescope.builtin').highlights,
    keys = { "n", "<leader>fH", noremap }
  },
  {
    desc = "Telescope: fuzzy find (current buf)",
    cmd = require('telescope.builtin').current_buffer_fuzzy_find,
    keys = { "n", "<leader>f/", noremap }
  },
  {
    desc = "Telescope: tags (current buf)",
    cmd = require('telescope.builtin').current_buffer_tags,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: previous search results",
    cmd = require('telescope.builtin').resume,
    keys = { "n", "<leader>fR", noremap }
  },
  {
    desc = "Telescope: previous pickers",
    cmd = require('telescope.builtin').pickers,
    -- keys = { "n", "", noremap }
  },

  -- LSP Pickers
  {
    desc = "Telescope: LSP references",
    cmd = require('telescope.builtin').lsp_references,
    keys = { "n", "<leader>fr", noremap }
  },
  {
    desc = "Telescope: LSP incoming calls",
    cmd = require('telescope.builtin').lsp_incoming_calls,
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: LSP outgoing calls",
    cmd = require('telescope.builtin').lsp_outgoing_calls,
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: LSP doc symbols",
    cmd = require('telescope.builtin').lsp_document_symbols,
    keys = { "n", "<leader>fs", noremap }
  },
  { desc = "Telescope: LSP workspace symbols",
    cmd = require('telescope.builtin').lsp_workspace_symbols,
    keys = { "n", "<leader>fw", noremap }
  },
  { desc = "Telescope: LSP Dyn workspace symbols",
    cmd = require('telescope.builtin').lsp_dynamic_workspace_symbols,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Telescope: LSP Diagnostics",
    cmd = require('telescope.builtin').diagnostics,
    keys = { "n", "<leader>fd", noremap }
  },
  { desc = "Telescope: LSP Implementations",
    cmd = require('telescope.builtin').lsp_implementations,
    keys = { "n", "<leader>fi", noremap }
  },
  { desc = "Telescope: LSP Definitions",
    cmd = require('telescope.builtin').lsp_definitions,
    keys = { "n", "<leader>fD", noremap }
  },
  { desc = "Telescope: LSP Type Definitions",
    cmd = require('telescope.builtin').lsp_type_definitions,
    keys = { "n", "<leader>ft", noremap }
  },


  -- Git Pickers
  { desc = "Telescope: Git Commits",
    cmd = require('telescope.builtin').git_commits,
    keys = { "n", "<leader>fGc", noremap }
  },
  { desc = "Telescope: Git Buff Commits",
    cmd = require('telescope.builtin').git_bcommits,
    keys = { "n", "<leader>fGC", noremap }
  },
  { desc = "Telescope: Git Branches",
    cmd = require('telescope.builtin').git_branches,
    keys = { "n", "<leader>fGb", noremap }
  },
  { desc = "Telescope: Git Status",
    cmd = require('telescope.builtin').git_status,
    keys = { "n", "<leader>fGs", noremap }
  },
  { desc = "Telescope: Git Stash Items",
    cmd = require('telescope.builtin').git_stash,
    keys = { "n", "<leader>fGS", noremap }
  },

  -- Treesitter Picker
  { desc = "Telescope: Treesitter",
    cmd = require('telescope.builtin').treesitter,
    keys = { "n", "T", noremap }
  },

  -- Lists Picker
  { desc = "Telescope: Environment Variables",
    cmd = "<CMD>Telescope env<CR>",
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: Planets",
    cmd = require('telescope.builtin').planets,
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: reload Lua modules",
    cmd = require('telescope.builtin').reloader,
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: json source symbols",
    cmd = require('telescope.builtin').symbols,
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: Packer",
    cmd = "<cmd>lua require('telescope').extensions.packer.packer(opts)<cr>",
    -- keys = { "n", "", noremap }
  },
  { desc = "Telescope: Zoxide",
    cmd = "<CMD>lua require'telescope'.extensions.zoxide.list{}<CR>",
    keys = { "n", "<Leader>fz", noremap }
  },
  { desc = "Telescope: Projects",
    cmd = "<cmd>lua require('telescope').extensions.projects.projects(require('telescope.themes').get_dropdown({hidden=true}))<cr>",
    keys = { "n", "<leader>fp", noremap }
  },
})
