local status_ok, bufferline = pcall(require, "bufferline")
if not status_ok then
  return
end

bufferline.setup {
  options = {
    mode = "buffers", -- set to "tabs" to only show tabpages instead
    numbers = "ordinal", -- "none" | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
    close_command = "bdelete! %d", -- can be a string | function, see "Mouse actions"
    right_mouse_command = "bdelete! %d", -- can be a string | function, see "Mouse actions"
    left_mouse_command = "buffer %d", -- can be a string | function, see "Mouse actions"
    middle_mouse_command = nil, -- can be a string | function, see "Mouse actions"
    -- NOTE: this plugin is designed with this icon in mind,
    -- and so changing this is NOT recommended, this is intended
    -- as an escape hatch for people who cannot bear it for whatever reason
    indicator = {
      icon = '▎', -- this should be omitted if indicator style is not 'icon'
      -- style = 'icon' | 'underline' | 'none',
      style = 'icon'
    },
    buffer_close_icon = '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    --- name_formatter can be used to change the buffer's label in the bufferline.
    --- Please note some names can/will break the
    --- bufferline so use this at your discretion knowing that it has
    --- some limitations that will *NOT* be fixed.
    -- name_formatter = function(buf) -- buf contains a "name", "path" and "bufnr"
    -- remove extension from markdown files for example
    --  if buf.name:match('%.md') then
    --    return vim.fn.fnamemodify(buf.name, ':t:r')
    --  end
    -- end,
    max_name_length = 18,
    max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
    tab_size = 18,
    diagnostics = "nvim_lsp", -- false | "nvim_lsp" | "coc",
    diagnostics_update_in_insert = false,
    diagnostics_indicator = function(count, level, diagnostics_dict, context)
      local icon = level:match("error") and " " or " "
      return " " .. icon .. count
    end,
    -- NOTE: this will be called a lot so don't do any heavy processing here
    -- custom_filter = function(buf_number, buf_numbers)
    --   -- filter out filetypes you don't want to see
    --   if vim.bo[buf_number].filetype ~= "<i-dont-want-to-see-this>" then
    --     return true
    --   end
    -- end,
    -- offsets = {{filetype = "NvimTree", text = "File Explorer" | function , text_align = "left"}},
    color_icons = true, -- whether or not to add the filetype icon highlights
    show_buffer_icons = true, -- disable filetype icons for buffers
    show_buffer_close_icons = false,
    show_buffer_default_icon = true, -- whether or not an unrecognised filetype should show a default icon
    show_close_icon = false,
    show_tab_indicators = true,
    persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
    -- can also be a table containing 2 custom separators
    -- [focused and unfocused]. eg: { '|', '|' }
    separator_style = "thick", -- "slant" | "thick" | "thin" | { 'any', 'any' },
    enforce_regular_tabs = false,
    always_show_bufferline = true,
    sort_by = 'insert_at_end', -- 'insert_after_current' |'insert_at_end' | 'id' | 'extension' | 'relative_directory' | 'directory' | 'tabs' | function(buffer_a, buffer_b)
    -- add custom logic
    --  return buffer_a.modified > buffer_b.modified
    -- end
  }
}


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- Bufferline
  -- Handled mostly by Hydra (<leader>b)
  {
    desc = "Move to previous Next Buffer",
    cmd = "<CMD>BufferLineCyclePrev<CR>",
    -- keys = { "n", "<S-h>", noremap }
  },
  {
    desc = "Move to next Next Buffer",
    cmd = "<CMD>BufferLineCycleNext<CR>",
    -- keys = { "n", "<S-l>", noremap }
  },
  {
    desc = "Order buffer by directory",
    cmd = "<CMD>BufferOrderByBufferDirectory<CR>",
  }, {
    desc = "Order buffer by language",
    cmd = "<CMD>BufferOrderByBufferLanguage<CR>",
  }, {
    desc = "Order buffer by window number",
    cmd = "<CMD>BufferOrderByBufferWindowNumber<CR>",
  },
  {
    desc = "Move buffer to previous position",
    cmd = "<CMD>BufferLineMovePrev<CR>",
    keys = { "n", "<Leader>bmn", noremap }
  },
  {
    desc = "Move buffer to next position",
    cmd = "<CMD>BufferLineMoveNext<CR>",
    keys = { "n", "<Leader>bmn", noremap }
  },
  {
    desc = "Pick buffer from bufferline",
    cmd = "<Cmd>BufferLinePick<CR>",
    keys = { "n", "<leader>bp", noremap }
  },
  {
    desc = "Go to buffer 1",
    cmd = "<Cmd>BufferLineGoToBuffer 1<CR>",
    keys = { "n", "<Leader>b1", noremap }
  },
})
