require("inc_rename").setup {
  cmd_name = "IncRename", -- the name of the command
  hl_group = "Substitute", -- the highlight group used for highlighting the identifier's new name
  multifile_preview = true, -- whether to enable the command preview across multiple buffers
  show_message = true, -- whether to display a `Renamed m instances in n files` message after a rename operation
}



local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- Inc-Rename
  {
    desc = "Incremental Rename",
    cmd = ":IncRename ",
    keys = { "n", "gr", noremap }
  },
})
