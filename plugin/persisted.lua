require("persisted").setup({
  save_dir = vim.fn.expand(vim.fn.stdpath("data") .. "/sessions/"), -- directory where session files are saved
  command = "VimLeavePre", -- the autocommand for which the session is saved
  branch_separator = "@@", -- string used to separate session directory name from branch name
  use_git_branch = true, -- create session files based on the branch of the git enabled repository
  autosave = true, -- automatically save session files when exiting Neovim
  autoload = true, -- automatically load the session for the cwd on Neovim startup
  allowed_dirs = nil, -- table of dirs that the plugin will auto-save and auto-load from
  ignored_dirs = nil, -- table of dirs that are ignored when auto-saving and auto-loading
  before_save = nil, -- function to run before the session is saved to disk
  after_save = nil, -- function to run after the session is saved to disk
  after_source = nil, -- function to run after the session is sourced

  telescope = {
    before_source = function()
      -- Close all open buffers
      -- Thanks to https://github.com/avently
      vim.api.nvim_input("<ESC>:%bd<CR>")
    end,
    after_source = function(session)
      print("Loaded session " .. session.name)
    end,
  },
})


require("telescope").load_extension("persisted")


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  { desc = "Telescope: Session Search (Persisted)",
    cmd = "<cmd>Telescope persisted<cr>",
    keys = { "n", "<leader>fS", noremap }
  },
  { desc = "Session Toggle (Persisted)",
    cmd = "<cmd>SessionToggle<CR>",
    -- keys = { "n", "<leader>fp", noremap }
  },

})
