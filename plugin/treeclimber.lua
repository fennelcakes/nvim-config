-- vim.cmd.packadd('nvim-treeclimber')

local tc = require('nvim-treeclimber')
-- tc.setup()

-- Highlight groups
-- Change if you don't have Lush installed
local color = require("nvim-treeclimber.hi")
local bg = color.bg_hsluv("Normal")
local fg = color.fg_hsluv("Normal")
local dim = bg.mix(fg, 20)

local map = vim.keymap.set
local set_hl = vim.api.nvim_set_hl

set_hl(0, "TreeClimberHighlight", { background = dim.hex })

set_hl(0, "TreeClimberSiblingBoundary", { background = color.terminal_color_5.hex })

set_hl(0, "TreeClimberSibling", { background = color.terminal_color_5.mix(bg, 40).hex, bold = true })

set_hl(0, "TreeClimberParent", { background = bg.mix(fg, 2).hex })

set_hl(0, "TreeClimberParentStart", { background = color.terminal_color_4.mix(bg, 10).hex, bold = true })

-- Keymaps
map("n", "<leader>k", tc.show_control_flow, {})

-- TODO: ts-textsubjects and ts-unit does some variation of this, which to keep?
-- map({ "x", "o" }, "i.", tc.select_current_node, { desc = "select current node" })
-- map({ "x", "o" }, "a.", tc.select_expand, { desc = "select parent node" })

map(
  { "n", "x", "o" },
  "<M-e>",
  tc.select_forward_end,
  { desc = "select and move to the end of the node, or the end of the next node" }
)

map(
  { "n", "x", "o" },
  "<M-b>",
  tc.select_backward,
  { desc = "select and move to the begining of the node, or the beginning of the next node" }
)

-- map({ "n", "x", "o" }, "<M-[>", tc.select_siblings_backward, {})
--
-- map({ "n", "x", "o" }, "<M-]>", tc.select_siblings_forward, {})

map(
  { "n", "x", "o" },
  "<M-g>",
  tc.select_top_level,
  { desc = "select the top level node from the current position" }
)

map({ "n", "x", "o" }, "<M-h>", tc.select_backward, { desc = "select previous node" })

map({ "n", "x", "o" }, "<M-j>", tc.select_shrink, { desc = "select child node" })

map({ "n", "x", "o" }, "<M-k>", tc.select_expand, { desc = "select parent node" })

map({ "n", "x", "o" }, "<M-l>", tc.select_forward, { desc = "select the next node" })

map({ "n", "x", "o" }, "<M-L>", tc.select_grow_forward, { desc = "Add the next node to the selection" })

map({ "n", "x", "o" }, "<M-H>", tc.select_grow_backward, { desc = "Add the next node to the selection" })
