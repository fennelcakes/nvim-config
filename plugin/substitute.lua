require("substitute").setup(
  {
    on_substitute = nil,
    yank_substituted_text = false,
    range = {
      -- prefix = "s",
      prompt_current_text = false,
      confirm = false,
      complete_word = false,
      motion1 = false,
      motion2 = false,
      suffix = "",
    },
    exchange = {
      motion = false,
      use_esc_to_cancel = true,
    },
  }
)

local sub_operator = function()
  require('substitute').operator({
    count = 1, -- number of substitutions
    register = "\"", -- register used for substitution
    motion = "iw", -- only available for `operator`, this will automatically use
    -- this operator for substitution instead of asking for.
  })
end

-- Lua
-- vim.keymap.set("n", "ss", "<cmd>lua require('substitute').line()<cr>", { noremap = true })
-- vim.keymap.set("n", ",S", "<cmd>lua require('substitute').eol()<cr>", { noremap = true })
--
-- vim.keymap.set("x", "<leader>s", "<cmd>lua require('substitute.range').visual()<cr>", { noremap = true })
-- vim.keymap.set("n", "<leader>ss", "<cmd>lua require('substitute.range').word()<cr>", { noremap = true })


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Substitute: exchange word under cursor w/ \" register",
    cmd = function ()
      sub_operator()
    end,
    keys = { "n", ",c", noremap }
  },
  {
    desc = "Substitute: exchange visual selection w/ \" register",
    cmd = "<cmd>lua require('substitute').visual()<cr>",
    keys = { "x", ",s", noremap }
  },
  {
    desc = "Substitute: exchange items w/ range selection",
    cmd = "<cmd>lua require('substitute').visual()<cr>",
    keys = { "n", ",r", noremap }
  },
  {
    desc = "Substitute: exchange w/ operator",
    cmd = "<cmd>lua require('substitute.exchange').operator()<cr>",
    keys = { "n", ",c", noremap }
  },
  {
    desc = "Substitute: exchange line",
    cmd = "<cmd>lua require('substitute.exchange').line()<cr>",
    keys = { "n", ",cx", noremap }
  },
  {
    desc = "Substitue: visual exchange",
    cmd = "<cmd>lua require('substitute.exchange').visual()<cr>",
    keys = { "x", ",cx", noremap }
  },
  {
    desc = "Substitue: Cancel exchange",
    cmd = "<cmd>lua require('substitute.exchange').cancel()<cr>",
    keys = { "n", ",cc", noremap }
  },
})

-- hay stuffrstrstrstrrtyulyl827349234
-- test of some other things
