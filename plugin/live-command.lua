-- Use :Norm or whatever command you set up to live-preview changes to buffer
-- https://github.com/smjonas/live-command.nvim

require("live-command").setup {
  commands = {
    Norm = { cmd = "norm" },
  },
  defaults = {
    enable_highlighting = true,
    hl_groups = {
      insertion = "DiffAdd",
      replacement = "DiffChanged",
      deletion = "DiffDelete",
    },
    hl_range = { 0, 0, kind = "relative" },
  },
}
