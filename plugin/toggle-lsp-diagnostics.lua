local status_ok, toggle_lsp_diagnostics = pcall(require, "toggle_lsp_diagnostics")
if not status_ok then
  return
end

toggle_lsp_diagnostics.init({
  start_on = false
})


-- local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Toggle ALL diagnostics on/off",
    cmd = "<CMD>ToggleDiag<CR>",
    -- keys = { "n", "", noremap }
  },
  --
  {
    desc = "Toggle ALL diagnostics to their default",
    cmd = "<CMD>ToggleDiagDefault<CR>",
    -- keys = { "n", "", noremap }
  },
  --
  {
    desc = "Turn ALL diagnostics on",
    cmd = "<CMD>ToggleDiagOn<CR>",
    -- keys = { "n", "", noremap }
  },
  --
  {
    desc = "Turn ALL diagnostics off",
    cmd = "<CMD>ToggleDiagOff<CR>",
    -- keys = { "n", "", noremap }
  },
})
