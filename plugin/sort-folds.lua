local noremap = { noremap = true }
-- Shorten function name
local map = vim.api.nvim_set_keymap



--[[ NOTE: -- KEYMAPS
map('v', '<Leader>sf', '<Plug>SortFolds',  noremap)
]]--


vim.g.sort_folds_ignore_case = 1
