local Hydra = require('hydra')
local function cmd(command)
  return table.concat({ '<Cmd>', command, '<CR>' })
end


local tele_hydra = [[
^                Telescope                      ^

^ _?_ grep string         ^ _l_ live grep          ^
^ _/_ cur buf fzy find

                Files / Dirs
^ _f_ find files          ^ _o_ oldfiles           ^
^ _b_ buffers             ^ _,t_ Tabs               ^
^ _F_ load file in dir    ^ _._ grep into dir      ^
^ _`_ change dir          ^ _z_ zoxide             ^
^ _P_ Projects            ^ _S_ persisted (session)^

^ _'_ marks               ^ _"_ registers
^ _q_ quickfix            ^ _Q_ quickfix history   ^
^ _j_ jumplist            ^ _L_ loclist

^ _c_ commands            ^ _C_ changes            ^
^ _h_ help tags           ^ _H_ highlights         ^
^ _k_ keymaps             ^ _v_ vim options        ^
^ _p_ command center
^
^                    Git
^ _gc_ commits            ^ _gb_ buf commits       ^
^ _gB_ branches           ^ _gs_ status            ^
^ _gf_ files              ^ _gS_ stash items       ^

^                    LSP
^ _d_ diagnostics         ^ _D_ definitions        ^
^ _s_ document symbols    ^ _w_ workspace symbols  ^
^ _i_ incoming calls      ^ _I_ implementations    ^
^ _O_ outgoing calls      ^ _,lsp_ Toggle LSP      ^
^ _r_ references (cursor) ^ _t_ type definitions   ^
^ _Y_ dyn wrkspc symbols  ^ _T_ treesitter tree    ^
^
^                    Misc
^ _;c_ colorschemes       ^ _;p_ Packer             ^
^ _;h_ command history    ^ _;P_ pickers            ^



^                   _<Esc>_ exit                    ^
]]





Hydra({
  name = "Telescope ",
  hint = tele_hydra,
  config = {
    color = 'teal',
    invoke_on_body = true,
    hint = {
      border = 'rounded',
      position = 'top'
    }
  },
  mode = 'n',
  body = '<M-s>',
  heads = {
    -- Change Directory (cder)
    { '`', cmd "Telescope cder", },
  -- NOTE: Not planning to use
    -- -- autocommands
    -- { '', cmd "Telescope autocommands" },
    -- -- json source symbols
    -- { '', cmd "Telescope symbols" },
    -- -- search history
    -- { '', cmd "Telescope search_history" },
    -- -- spelling suggest
    -- { '', cmd "Telescope spell_suggest" },
    -- -- filetypes
    -- { '', cmd "Telescope filetypes" },
    -- -- Environment Variables
    -- { '', cmd "Telescope env" },
    -- -- reload Lua modules
    -- { '', cmd "Telescope reloader" },

    -- Tabs
    { ',t', cmd  "Telescope telescope-tabs list_tabs" },
    -- Toggle LSP
    { ',lsp', cmd  "ToggleLSP" },
    -- -- color schemes
    { ';P', cmd  "lua require 'telescope'.extensions.find_pickers.find_pickers()" },
    -- -- color schemes
    { ';c', cmd "Telescope colorscheme" },
    -- recent commands ",
    { ';h', cmd "Telescope command_history" },
    -- -- Packer
    { ';p', cmd "Telescope extensions.packer.packer(opts)<cr>" },
    -- grep into dir
    { '.', cmd "GrepInDirectory" },
    -- fuzzy find (current buf)
    { '/', cmd "Telescope current_buffer_fuzzy_find" },
    -- grep string under cursor
    { '?', cmd "Telescope grep_string" },
    -- marks
    { '\'', cmd "Telescope marks" },
    -- registers
    { '\"', "Telescope registers" },
    -- open buffers
    { 'b', cmd "Telescope buffers" },
    -- available commands
    { 'c', cmd "Telescope commands" },
    -- recent commands ",
    { 'C', cmd "Telescope changes" },
    -- LSP Diagnostics
    { 'd', cmd "Telescope diagnostics" },
    -- LSP Definitions
    { 'D', cmd "Telescope lsp_definitions" },
    -- find files in working dir
    { 'f', cmd "Telescope find_files" },
    -- load file in dir
    { 'F', cmd "FileInDirectory" },
    -- Git Commits
    { 'gc', cmd "Telescope git_commits" },
    -- Git Buff Commits
    { 'gb', cmd "Telescope git_bcommits" },
    -- Git Branches
    { 'gB', cmd "Telescope git_branches" },
    -- Git Status
    { 'gs', cmd "Telescope git_status" },
    -- Git Stash Items
    { 'gS', cmd "Telescope git_stash" },
    -- search git files
    { 'gf', cmd "Telescope git_files," },
    -- help tags
    { 'h', cmd "Telescope help_tags" },
    -- highlights
    { 'H', cmd "Telescope highlights" },
    -- -- LSP incoming calls
    { 'i', cmd "Telescope lsp_incoming_calls" },
    -- LSP Implementations
    { 'I', cmd "Telescope lsp_implementations" },
    -- jumplist
    { 'j', cmd "Telescope jumplist" },
    -- keymaps
    { 'k', cmd "Telescope keymaps" },
    -- grep a string in working dir
    { 'l', cmd "Telescope live_grep" },
    -- loclist
    { 'L', cmd "Telescope loclist" },
    -- -- Noice
    -- { 'N', cmd "Telescope noice" },
    -- recent files
    { 'o', cmd "Telescope oldfiles" },
    -- -- LSP outgoing calls
    { 'O', cmd "Telescope lsp_outgoing_calls" },
    -- Command Center
    { 'p', cmd "Telescope command_center" },
    -- Projects
    { 'P', cmd "Telescope Projects" },
    -- quickfix
    { 'q', cmd "Telescope quickfix" },
    -- quickfix history
    { 'Q', cmd "Telescope quickfix history", },
    -- LSP references
    { 'r', cmd "Telescope lsp_references" },
    { 's', cmd "Telescope lsp_document_symbols" },
    { 'S', cmd "Telescope persisted" },
    -- LSP Type Definitions
    { 't', cmd "Telescope lsp_type_definitions" },
    -- -- Treesitter
    { 'T', cmd "Telescope treesitter" },
    -- vim options
    { 'v', cmd "Telescope vim_options" },
    -- LSP workspace symbols
    { 'w', cmd "Telescope lsp_workspace_symbols" },
    -- -- LSP Dyn workspace symbols
    { 'Y', cmd "Telescope lsp_dynamic_workspace_symbols" },
    -- Zoxide
    { 'z', cmd "Telescope zoxide" },

    { '<Esc>', cmd "<Esc>", { exit = true, nowait = true } },
  }
})
