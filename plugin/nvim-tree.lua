vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1

-- setup with some options
require("nvim-tree").setup({
  -- respect_buf_cwd = true,

  hijack_cursor = true,
  sync_root_with_cwd = true,
  sort_by = "extension",
  view = {
    adaptive_size = true,
    mappings = {
      list = {
        { key = "h", action = "dir_up" },
        { key = "l", action = "cd" },
      },
    },
    -- float = {
    --   enable = false,
    --   open_win_config = {
    --     relative = "editor",
    --     border = "rounded",
    --     width = 30,
    --     height = 30,
    --     row = 1,
    --     col = 1,
    --   },
    -- },
    relativenumber = true,
  },
  update_focused_file = {
    enable = true,
    update_root = true,
    ignore_list = {},
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
  actions = {
    open_file = {
      window_picker = {
        enable = true,
        chars = 'FJDKSLA;CMRUEIWOQP',
      },
    },
  },
  trash = {
    cmd = "trash",
    require_confirm = true,
  },
  -- Uncomment this for project.nvim or nvim-rooter support
  -- update_cwd = true,
  -- update_focused_file = {
  --   enable = true,
  --   update_cwd = true
  -- },

})


-- Open nvim-tree on vimenter
local api = vim.api
local autocmd = api.nvim_create_autocmd
autocmd({ "VimEnter" }, {
  pattern = "*",
  callback = function()
    vim.cmd([[:NvimTreeFocus]])
  end,
})


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- Nvim-Tree
  {
    desc = " Nvim-Tree Toggle",
    cmd = "<CMD>NvimTreeToggle<CR>",
    keys = { "n", "<leader>e", noremap },
  },
  {
    desc = " Nvim-Tree Focus",
    cmd = "<CMD>NvimTreeFocus<CR>",
    keys = { "n", "<leader>E", noremap },
  },
  {
    desc = " Nvim-Tree FindFile ",
    cmd = "<CMD>cd %:h<CR><CMD>NvimTreeFindFile<CR>",
    keys = { "n", "<leader>;e", noremap },
    -- keys = "",
  },
  {
    desc = " Nvim-Tree Collapse",
    cmd = "<CMD>NvimTreeCollapse<CR>",
    -- keys = '',
  },
})

-- view.mappings.list = { -- BEGIN_DEFAULT_MAPPINGS
--     { key = { "<CR>", "o", "<2-LeftMouse>" }, action = "edit" }
--     { key = "<C-e>",                          action = "edit_in_place" }
--     { key = "O",                              action = "edit_no_picker" }
--     { key = { "<C-]>", "<2-RightMouse>" },    action = "cd" }
--     { key = "<C-v>",                          action = "vsplit" }
--     { key = "<C-x>",                          action = "split" }
--     { key = "<C-t>",                          action = "tabnew" }
--     { key = "<",                              action = "prev_sibling" }
--     { key = ">",                              action = "next_sibling" }
--     { key = "P",                              action = "parent_node" }
--     { key = "<BS>",                           action = "close_node" }
--     { key = "<Tab>",                          action = "preview" }
--     { key = "K",                              action = "first_sibling" }
--     { key = "J",                              action = "last_sibling" }
--     { key = "I",                              action = "toggle_git_ignored" }
--     { key = "H",                              action = "toggle_dotfiles" }
--     { key = "U",                              action = "toggle_custom" }
--     { key = "R",                              action = "refresh" }
--     { key = "a",                              action = "create" }
--     { key = "d",                              action = "remove" }
--     { key = "D",                              action = "trash" }
--     { key = "r",                              action = "rename" }
--     { key = "<C-r>",                          action = "full_rename" }
--     { key = "x",                              action = "cut" }
--     { key = "c",                              action = "copy" }
--     { key = "p",                              action = "paste" }
--     { key = "y",                              action = "copy_name" }
--     { key = "Y",                              action = "copy_path" }
--     { key = "gy",                             action = "copy_absolute_path" }
--     { key = "[e",                             action = "prev_diag_item" }
--     { key = "[c",                             action = "prev_git_item" }
--     { key = "]e",                             action = "next_diag_item" }
--     { key = "]c",                             action = "next_git_item" }
--     { key = "-",                              action = "dir_up" }
--     { key = "s",                              action = "system_open" }
--     { key = "f",                              action = "live_filter" }
--     { key = "F",                              action = "clear_live_filter" }
--     { key = "q",                              action = "close" }
--     { key = "W",                              action = "collapse_all" }
--     { key = "E",                              action = "expand_all" }
--     { key = "S",                              action = "search_node" }
--     { key = ".",                              action = "run_file_command" }
--     { key = "<C-k>",                          action = "toggle_file_info" }
--     { key = "g?",                             action = "toggle_help" }
--   } -- END_DEFAULT_MAPPINGS
