local npairs = require("nvim-autopairs")

-- change default fast_wrap
npairs.setup {
  fast_wrap = {
    map = '<M-s>',
    chars = { '{', '[', '(', '"', "'" },
    pattern = [=[[%'%"%)%>%]%)%}%,]]=],
    end_key = 'e',
    keys = 'arstnioqwfpluy;zxcdh,./',
    check_comma = true,
    highlight = 'Search',
    highlight_grey = 'Comment'
  },

  disable_filetype = { "TelescopePrompt", "guihua", "guihua_rust", "clap_input", "vim" },

}

if vim.o.ft == 'clap_input' and vim.o.ft == 'guihua' and vim.o.ft == 'guihua_rust' then
  require 'cmp'.setup.buffer { completion = { enable = false } }
end


-- If you want insert `(` after select function or method item
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require('cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)
