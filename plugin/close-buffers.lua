require('close_buffers').setup({
  preserve_window_layout = { 'this', 'nameless', 'other' },
  next_buffer_cmd = function(windows)
    require('bufferline').cycle(1)
    local bufnr = vim.api.nvim_get_current_buf()

    for _, window in ipairs(windows) do
      vim.api.nvim_win_set_buf(window, bufnr)
    end
  end,
})


-- Delete hidden buffers
local bdelete = require('bufdelete').bufdelete
local function delete_hidden_buffers()
  local hidden_bufs = vim.tbl_filter(function(bufnr)
    return vim.fn.getbufinfo(bufnr)[1].hidden == 1
  end, vim.api.nvim_list_bufs())

  for _, bufnr in ipairs(hidden_bufs) do
    bdelete(bufnr)
  end
end

vim.api.nvim_create_user_command('BDeleteHidden', delete_hidden_buffers, { bang = true })



local command_center = require("command_center")
local noremap = { noremap = true }

command_center.add({
  {
    desc = "Close Buf: delete all but focused",
    cmd = function ()
     require('close_buffers').delete({type = 'other'})
    end,
    -- keys = { "n", "", noremap }
  },
  {
    desc = "Close Buf: visible (in windows) only", -- This plugin calls these 'hidden'
    cmd = function() require('close_buffers').delete({ type = 'hidden' }) end,
    -- mnemonic: bl for window Layout
    keys = { "n", "<leader>bl", noremap }
  },
  {
    desc = "Close Buf: Delete focused buf",
    cmd = function () require('close_buffers').delete({ type = 'this' }) end,
    keys = { "n", "<leader>bd", noremap }
  },
  {
    desc = "Close Buf: Delete nameless bufs",
    cmd = function () require('close_buffers').delete({ type = 'nameless' }) end,
    keys = { "n", "<leader>bN", noremap }
  },
  {
    desc = "Close Buf: Delete ALL listed buf",
    cmd = function () require('close_buffers').delete({ type = 'all' }) end,
    keys = { "n", "<leader>bA", noremap }
  },
})
