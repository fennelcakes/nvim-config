local status_ok, attempt = pcall(require, "attempt")
if not status_ok then
  return
end

require('telescope').load_extension 'attempt'

attempt.setup({
  --   list_buffers = false,     -- This will make them show on other pickers (like :Telescope buffers)
  --   format_opts = { [''] = '[None]' },                    -- How they'll look
  --   ext_options = { 'lua', 'js', 'py', 'cpp', 'c', '' },  -- Options to choose from
  run = {
    py = { 'w !python3' }, -- Either table of strings or lua functions
    js = { 'w !node' },
    lua = { 'w', 'luafile %' },
    -- sh = { 'w !bash' },
    -- pl = { 'w !perl' },
    -- cpp = { 'w' , '!'.. cpp_compiler ..' %:p -o %:p:r.out && echo "" && %:p:r.out && rm %:p:r.out '},
    -- c = { 'w' , '!'.. c_compiler ..' %:p -o %:p:r.out && echo "" && %:p:r.out && rm %:p:r.out'},
  }
})

-- local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Attempt: new attempt, selecting extension",
    cmd = "<cmd>lua require('attempt').new_select()<CR>",
    -- keys = {"n", "<leader>an", noremap }
  },
  {
    desc = "Attempt: new attempt, inputing extension",
    cmd = "<cmd>lua require('attempt').new_input_ext()<CR>",
    -- keys = {"n", "<leader>ai", noremap }
  },
  {
    desc = "Attempt: run attempt",
    cmd = "<cmd>lua require('attempt').run()<CR>",
    -- keys = {"n", "<leader>ar", noremap }
  },
  {
    desc = "Attempt: delete attempt from current buffer",
    cmd = "<cmd>lua require('attempt').delete_buf()<CR>",
    -- keys = {"n", "<leader>ad", noremap }
  },
  {
    desc = "Attempt: rename attempt from current buffer",
    cmd = "<cmd>lua require('attempt').rename_buf()<CR>",
    -- keys = {"n", "<leader>ac", noremap }
  },
  {
    desc = "Attempt: search using Telescope",
    cmd = "<cmd>Telescope attempt<CR>",
    -- keys = {"n", "<leader>al", noremap }
  },
  {
    desc = "Attempt: search using ui.select()",
    cmd = "<cmd>lua require('attempt').open_select()<CR>",
    -- keys = {"n", "<leader>al", noremap }
  },
})
