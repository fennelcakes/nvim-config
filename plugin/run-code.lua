-- require('run-code').setup {
--   output = {
--     buffer = false,
--     split_cmd = '20split',
--   }
-- }


--[[ NOTE: -- KEYMAPS

vim.api.nvim_set_keymap('v', '<leader>r', ':RunCodeSelected<CR>', {})
vim.api.nvim_set_keymap('n', '<leader>rr', ':RunCodeFile<CR>', {})
vim.api.nvim_set_keymap('n', '<leader>rt', ':RunCodeLauncher<CR>', {})

-- set custom commands
-- vim.api.nvim_set_keymap('n', '<leader>R1', ':RunCodeSetCustomCmd 1<CR>', {})
-- vim.api.nvim_set_keymap('n', '<leader>R2', ':RunCodeSetCustomCmd 2<CR>', {})
-- vim.api.nvim_set_keymap('n', '<leader>R3', ':RunCodeSetCustomCmd 3<CR>', {})
--
-- -- run custom commands
-- vim.api.nvim_set_keymap('n', '<leader>r1', ':RunCodeCustomCmd 1<CR>', {})
-- vim.api.nvim_set_keymap('n', '<leader>r2', ':RunCodeCustomCmd 2<CR>', {})
-- vim.api.nvim_set_keymap('n', '<leader>r3', ':RunCodeCustomCmd 3<CR>', {})

]] --



-- local noremap = { noremap = true }
-- local command_center = require("command_center")
--
-- command_center.add({
--   -- Run Code
--
--   {
--     desc = "Run Code: Selected (visual)",
--     cmd = ":RunCodeSelected<CR>",
--     keys = { "v", "<leader>r", noremap },
--   },
--   {
--     desc = "Run Code: Current File",
--     cmd = ":RunCodeFile<CR>",
--     keys = { "n", "<leader>rr", noremap },
--   },
--   {
--     desc = "Run Code Launcher",
--     cmd = ":RunCodeLauncher<CR>",
--     keys = { "n", "<leader>rt", noremap },
--   },
--
-- })
