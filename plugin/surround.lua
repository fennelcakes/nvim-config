require("nvim-surround").setup({
  keymaps = {
    -- I don't understand the need of this insert mode mapping with autopairs
    -- enable
    insert = "<C-M-S-s>urround",
    insert_line = "<C-M-S-s>urround",
    normal = "ys",
    normal_cur = "yss",
    normal_line = "yS",
    normal_cur_line = "ySS",
    visual = "s",
    visual_line = "gS",
    delete = "ds",
    change = "cs",

    -- insert = "<C-g>s",
    -- insert_line = "<C-g>S",
    -- normal = "ys",
    -- normal_cur = "yss",
    -- normal_line = "yS",
    -- normal_cur_line = "ySS",
    -- visual = "s",
    -- visual_line = "s",
    -- delete = "ds",
    -- change = "cs",


  },
})

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Surround word with function",
    cmd = "ysiwf",
  },
  {
    desc = "Surround line with function",
    cmd = "yssf",
  },
  {
    desc = "Surround line with parens",
    cmd = "yssb",
  },
  {
    desc = "Surround line with brackets",
    cmd = "yssr",
  },
  {
    desc = "Surround line with parens on new lines",
    cmd = "ySSb",
  },
  {
    desc = "Surround line with brackets on new lines",
    cmd = "ySSr",
  },
  {
    desc = "Surround line with function on new lines",
    cmd = "ySSf",
  },
})


--[[ test area


line of stuff

word


]]
