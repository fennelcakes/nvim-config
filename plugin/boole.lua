require('boole').setup({
  mappings = {
    increment = '<C-a>',
    decrement = '<C-x>'
  },
  -- User defined loops
  additions = {
    {'Foo', 'Bar'},
    {'tic', 'tac', 'toe'},
--     {'True', 'False'},
--     {'true', 'false'},
--     {'0', '1'},
    {'enable', 'disable'},
--     {'Enable', 'Disable'},
--     {'yes', 'no'},
--     {'Yes', 'No'},
--     {'var1', 'var2'},
--     {'pos', 'neg'},
--     {'on', 'off'},
    {'foo', 'bar'},
    {'next', 'previous'},
    {'above', 'below'},
    {'up', 'down'},
    {'top', 'bottom'},
    {'left', 'right'},
  },
})
