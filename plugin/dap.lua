local dap = require('dap')

-- Debug calls
require('dap-python').setup('~/.pyenv/debugpy/bin/python3')

-- Extensions
require("nvim-dap-virtual-text").setup()

require("dapui").setup({
  icons = { expanded = "▾", collapsed = "▸" },
  mappings = {
    -- Use a table to apply multiple mappings
    expand = { "<CR>", "<2-LeftMouse>" },
    open = "o",
    remove = "d",
    edit = "e",
    repl = "r",
    toggle = "t",
  },
  -- Expand lines larger than the window
  -- Requires >= 0.7
  expand_lines = vim.fn.has("nvim-0.7"),
  -- Layouts define sections of the screen to place windows.
  -- The position can be "left", "right", "top" or "bottom".
  -- The size specifies the height/width depending on position. It can be an Int
  -- or a Float. Integer specifies height/width directly (i.e. 20 lines/columns) while
  -- Float value specifies percentage (i.e. 0.3 - 30% of available lines/columns)
  -- Elements are the elements shown in the layout (in order).
  -- Layouts are opened in order so that earlier layouts take priority in window sizing.
  layouts = {
    {
      elements = {
      -- Elements can be strings or table with id and size keys.
        { id = "scopes", size = 0.25 },
        "breakpoints",
        "stacks",
        "watches",
      },
      size = 40, -- 40 columns
      position = "left",
    },
    {
      elements = {
        "repl",
        "console",
      },
      size = 0.25, -- 25% of total lines
      position = "bottom",
    },
  },
  floating = {
    max_height = nil, -- These can be integers or a float between 0 and 1.
    max_width = nil, -- Floats will be treated as percentage of your screen.
    border = "rounded", -- Border style. Can be "single", "double" or "rounded"
    mappings = {
      close = { "q", "<Esc>" },
    },
  },
  windows = { indent = 1 },
  render = {
    max_type_length = nil, -- Can be integer or nil.
  }
})


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  -- Dap
  {
    desc = "Dap",
    cmd = "<cmd>lua require('dapui').toggle()<CR>",
    keys = { "n", "<leader>;d", noremap }
  },
  -- See Hydra for Dap usage
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require('dapui').open()<CR>",
  --   -- keys = {"n", ";do", noremap}
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require('dapui').close()<CR>",
  --   -- keys = {"n", ";dc", noremap}
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require('dap-python').debug_selection()<CR>",
  --   -- keys = {"v", ";dp", noremap}
  -- },
  --
  -- {
  --   desc = "Dap - Continue",
  --   cmd = "<cmd>lua require'dap'.continue()<CR>",
  --   keys = { "n", ";n", noremap }
  -- },
  -- {
  --   desc = "Dap - Step Over",
  --   cmd = "<cmd>lua require'dap'.step_over()<CR>",
  --   keys = { "n", ";j", noremap }
  -- },
  -- {
  --   desc = "Dap -",
  --   cmd = "<cmd>lua require'dap'.step_into()<CR>",
  --   keys = { "n", ";i", noremap }
  -- },
  -- {
  --   desc = "Dap -",
  --   cmd = "<cmd>lua require'dap'.step_out()<CR>",
  --   keys = { "n", ";k", noremap }
  -- },
  -- {
  --   desc = "Dap -",
  --   cmd = "<cmd>lua require'dap'.toggle_breakpoint()<CR>",
  --   keys = { "n", ";b", noremap }
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",
  --   keys = { "n", ";cb", noremap }
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",
  --   -- keys = {"n", ";mb", noremap}
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require'dap'.repl.open()<CR>",
  --   -- keys = {"n", ";ro", noremap}
  -- },
  -- {
  --   desc = "Dap",
  --   cmd = "<cmd>lua require'dap'.run_last()<CR>",
  --   -- keys = {"n", ";rl", noremap}
  -- },
})
