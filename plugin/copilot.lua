-- Copilot

require('copilot').setup({
  panel = {
    enabled = true,
    auto_refresh = false,
    keymap = {
      jump_prev = "[[",
      jump_next = "]]",
      accept = "<CR>",
      refresh = "gcr",
      open = "<M-CR>"
    },
  },
  suggestion = {
    enabled = true,
    auto_trigger = false,
    debounce = 75,
    keymap = {
     accept = "<M-c>",
     next = "<M-]>",
     prev = "<M-[>",
     dismiss = "<C-]>",
    },
  },
  filetypes = {
    yaml = false,
    markdown = true,
    help = false,
    gitcommit = false,
    gitrebase = false,
    hgcommit = false,
    svn = false,
    cvs = false,
    ["."] = false,
  },
  copilot_node_command = 'node', -- Node version must be < 18
  server_opts_overrides = {},
})


local command_center = require("command_center")
local noremap = { noremap = true }
command_center.add({
  {
    desc = "Copilot toggle",
    cmd = "<CMD>:Copilot toggle<CR>",
    keys = { "n", "<Leader>cot", noremap }
  },
  {
    desc = "Copilot suggestion",
    cmd = "<CMD>:Copilot suggestion<CR>",
    keys = { "n", "<Leader>cos", noremap }
  },
  {
    desc = "Copilot panel",
    cmd = "<CMD>Copilot panel<CR>",
    keys = { "n", "<Leader>cop", noremap }
  },
})
