local opts = {
  create_keymaps = true,
  move_to_debugline = true,

}

require("debugprint").setup(opts)

-- TODO: Unsure why but I'm unable to set keymaps, either as described in the
-- TODO: README or with command center here. Calling the lua functions directly (e.g.
-- TODO: ':lua require...') doesn't work either. Keep an eye on commits I guess.


-- local command_center = require("command_center")
-- local noremap = { noremap = true }
--
-- command_center.add({
--   { desc = "Debugline: print below",
--     cmd = function()
--       require('debugprint').debugprint()
--       end,
--     keys = { "n", "dqp", noremap }
--   },
--   { desc = "D<CRebugline: print above",
--     cmd = function()
--       require('debugprint').debugprint({above = true})
--       end,
--     keys = { "n", "dqP", noremap }
--   },
--   { desc = "Debugline: print variable above",
--     cmd = function()
--       require('debugprint').debugprint({variable = true})
--       end,
--     keys = { "n", "dQp", noremap }
--   },
--   { desc = "Debugline: print variable below",
--     cmd = function()
--       require('debugprint').debugprint({variable = true}, {above = true})
--       end,
--     keys = { "n", "dQP", noremap }
--   },
-- })
