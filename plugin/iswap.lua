require('iswap').setup{}

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "ISwap: Select two items",
    cmd = ":ISwap<CR>",
    keys = {"n", "<leader>si", noremap}
  },
  {
    desc = "ISwapWith: Swap item under cursor",
    keys = {"n", "<leader>su", noremap},
    cmd = ":ISwapWith<CR>"
  },
  {
    desc = "ISwapNode: Select items within node",
    cmd = ":ISwapNode<CR>",
    keys = {"n", "<leader>sn", noremap}
  },
})
