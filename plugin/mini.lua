-- require('mini.ai').setup({
--   -- Table with textobject id as fields, textobject specification as values.
--   -- Also use this to disable builtin textobjects. See |MiniAi.config|.
--   custom_textobjects = nil,
--
--   -- Module mappings. Use `''` (empty string) to disable one.
--   mappings = {
--     -- Main textobject prefixes
--     around = 'a',
--     inside = 'i',
--
--     -- Next/last variants
--     around_next = 'an',
--     inside_next = 'in',
--     around_last = 'al',
--     inside_last = 'il',
--
--     -- Move cursor to corresponding edge of `a` textobject
--     goto_left = 'g[',
--     goto_right = 'g]',
--   },
--
--   -- Number of lines within which textobject is searched
--   n_lines = 50,
--
--   -- How to search for object (first inside current line, then inside
--   -- neighborhood). One of 'cover', 'cover_or_next', 'cover_or_prev',
--   -- 'cover_or_nearest', 'next', 'previous', 'nearest'.
--   search_method = 'cover_or_next',
-- })

-- require('mini.surround').setup({
--   mappings = {
--     add = 'ys',
--     delete = 'ds',
--     find = '',
--     find_left = '',
--     highlight = '',
--     replace = 'cs',
--     update_n_lines = '',
--
--     -- Add this only if you don't want to use extended mappings
--     suffix_last = '',
--     suffix_next = '',
--   },
--
--   n_lines = 1000,
--   search_method = 'cover_or_next',
-- })
-- -- Remap adding surrounding to Visual mode selection
-- vim.api.nvim_del_keymap('x', 'ys')
-- vim.api.nvim_set_keymap('x', 's', [[:<C-u>lua MiniSurround.add('visual')<CR>]], { noremap = true })
-- -- Make special mapping for "add surrounding for line"
-- vim.api.nvim_set_keymap('n', 'yss', 'ys_', { noremap = false })


require('mini.indentscope').setup({
  draw = {
    -- Delay (in ms) between event and start of drawing scope indicator
    delay = 15,

    -- Animation rule for scope's first drawing. A function which, given
    -- next and total step numbers, returns wait time (in ms). See
    -- |MiniIndentscope.gen_animation()| for builtin options. To disable
    -- animation, use `require('mini.indentscope').gen_animation('none')`.
    animation = require('mini.indentscope').gen_animation('linear', { duration = 5 })
  },

  -- Module mappings. Use `''` (empty string) to disable one.
  mappings = {
    -- Textobjects
    object_scope = 'ii',
    object_scope_with_border = 'ai',

    -- Motions (jump to respective border line; if not present - body line)
    goto_top = '[i',
    goto_bottom = ']i',
  },

  -- Options which control scope computation
  options = {
    -- Type of scope's border: which line(s) with smaller indent to
    -- categorize as border. Can be one of: 'both', 'top', 'bottom', 'none'.
    border = 'both',

    -- Whether to use cursor column when computing reference indent.
    -- Useful to see incremental scopes with horizontal cursor movements.
    indent_at_cursor = true,

    -- Whether to first check input line to be a border of adjacent scope.
    -- Use it if you want to place cursor on function header to get scope of
    -- its body.
    try_as_border = false,
  },

  -- Which character to use for drawing scope indicator
  symbol = '╎',
})
local hl = vim.api.nvim_set_hl
local autocmd = vim.api.nvim_create_autocmd
autocmd({ "Filetype" }, {
  pattern = "*",
  callback = function()
    hl(0, "MiniIndentscopeSymbol", { fg = "#25747D" })
  end,
})
