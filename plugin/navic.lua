-- NOTE: onedark now supports highlights so this is just a customization now

local navic = require("nvim-navic")

navic.setup {
  icons = {
    File          = " ",
    Module        = " ",
    Namespace     = " ",
    Package       = " ",
    Class         = " ",
    Method        = " ",
    Property      = " ",
    Field         = " ",
    Constructor   = " ",
    Enum          = "練",
    Interface     = "練",
    Function      = " ",
    Variable      = " ",
    Constant      = " ",
    String        = " ",
    Number        = " ",
    Boolean       = "◩ ",
    Array         = " ",
    Object        = " ",
    Key           = " ",
    Null          = "ﳠ ",
    EnumMember    = " ",
    Struct        = " ",
    Event         = " ",
    Operator      = " ",
    TypeParameter = " ",
  },
  highlight = true,
  separator = "  ",
  depth_limit = 0,
  depth_limit_indicator = "..",
}
--
-- navic.setup {
--   icons = {
--     File = ' ',
--     Module = ' ',
--     Namespace = ' ',
--     Package = ' ',
--     Class = ' ',
--     Method = ' ',
--     Property = ' ',
--     Field = ' ',
--     Constructor = ' ',
--     Enum = ' ',
--     Interface = ' ',
--     Function = ' ',
--     Variable = ' ',
--     Constant = ' ',
--     String = ' ',
--     Number = ' ',
--     Boolean = ' ',
--     Array = ' ',
--     Object = ' ',
--     Key = ' ',
--     Null = ' ',
--     EnumMember = ' ',
--     Struct = ' ',
--     Event = ' ',
--     Operator = ' ',
--     TypeParameter = ' '
--   }
-- }



local colors = {
  fg = '#bbc2cf',
  bg1 = '#232B38',
  highlight = '#556178',
  bg2 = '#1E242E',
  separator = '#d16d9e',
  red = '#e95678',
  orange = '#FF8700',
  yellow = '#f7bb3b',
  green = '#afd700',
  cyan = '#36d0e0',
  blue = '#61afef',
  violet = '#C972E4',
  teal = '#1abc9c',
}
local kind = {
  [1] = { 'File', ' ', colors.fg },
  [2] = { 'Module', ' ', colors.blue },
  [3] = { 'Namespace', ' ', colors.orange },
  [4] = { 'Package', ' ', colors.violet },
  [5] = { 'Class', ' ', colors.violet },
  [6] = { 'Method', ' ', colors.violet },
  [7] = { 'Property', ' ', colors.cyan },
  [8] = { 'Field', ' ', colors.teal },
  [9] = { 'Constructor', ' ', colors.blue },
  [10] = { 'Enum', '了', colors.green },
  [11] = { 'Interface', '練', colors.orange },
  [12] = { 'Function', ' ', colors.violet },
  [13] = { 'Variable', ' ', colors.blue },
  [14] = { 'Constant', ' ', colors.cyan },
  [15] = { 'String', ' ', colors.green },
  [16] = { 'Number', ' ', colors.green },
  [17] = { 'Boolean', '◩ ', colors.orange },
  [18] = { 'Array', ' ', colors.blue },
  [19] = { 'Object', ' ', colors.orange },
  [20] = { 'Key', ' ', colors.red },
  [21] = { 'Null', 'ﳠ ', colors.red },
  [22] = { 'EnumMember', ' ', colors.green },
  [23] = { 'Struct', ' ', colors.violet },
  [24] = { 'Event', ' ', colors.violet },
  [25] = { 'Operator', ' ', colors.green },
  [26] = { 'TypeParameter', ' ', colors.green },
}


local api = vim.api
-- local augroup = api.nvim_create_augroup
local autocmd = api.nvim_create_autocmd
-- local cmd = api.nvim_command
local hl = api.nvim_set_hl

autocmd({ "Filetype" }, {
  pattern = "*",
  callback = function()

    for _, v in pairs(kind) do
      hl(0, 'NavicIcons' .. v[1], { default = true, fg = v[3], bg = "#1E242E" })
    end

    hl(0, "NavicText", {
      fg = colors.fg,
      bg = colors.bg2,
    })
    hl(0, "NavicSeparator", {
      fg = colors.separator,
      bg = colors.bg2,
    })
    hl(0, "File", {
      fg = colors.fg,
      bg = colors.bg2,
    })

    hl(0, "Winbar", { -- winbar of current window
      bg = colors.bg2,
    })

    hl(0, "WinbarNC", { -- winbar of not-current window
      bg = colors.bg1,
    })

  end,
})

-- vim.api.nvim_set_hl(0, "NavicIconsFile",          {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsModule",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsNamespace",     {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsPackage",       {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsClass",         {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsMethod",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsProperty",      {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsField",         {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsConstructor",   {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsEnum",          {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsInterface",     {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsFunction",      {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsVariable",      {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsConstant",      {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsString",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsNumber",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsBoolean",       {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsArray",         {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsObject",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsKey",           {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsNull",          {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsEnumMember",    {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsStruct",        {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsEvent",         {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsOperator",      {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicIconsTypeParameter", {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicText",               {default = true, bg = "#000000", fg = "#ffffff"})
-- vim.api.nvim_set_hl(0, "NavicSeparator",          {default = true, bg = "#000000", fg = "#ffffff"})
