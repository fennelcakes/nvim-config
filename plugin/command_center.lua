require("telescope").load_extension('command_center')


local command_center = require("command_center")
require('telescope').setup {
  extensions = {
    command_center = {
      components = {
        command_center.component.KEYS,
        command_center.component.DESC,
        command_center.component.CMD,
        command_center.component.CATEGORY,
      },
      sort_by = {
        command_center.component.DESC,
        command_center.component.KEYS,
      },
      auto_replace_desc_with_cmd = false,
    }
  }
}


