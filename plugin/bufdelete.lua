-- user_cmd('BufCurOnly', 'execute "%bdelete|edit#|bdelete#"', { nargs = 0 }) -- Deletes all buffers but current

local bdelete = require('bufdelete').bufdelete
-- Delete hidden buffers
local function delete_hidden_buffers()
  local hidden_bufs = vim.tbl_filter(function(bufnr)
    return vim.fn.getbufinfo(bufnr)[1].hidden == 1
  end, vim.api.nvim_list_bufs())

  for _, bufnr in ipairs(hidden_bufs) do
    bdelete(bufnr)
  end
end

vim.api.nvim_create_user_command('BdeleteHidden', delete_hidden_buffers, { bang = true })

--
local command_center = require("command_center")
local noremap = { noremap = true }

command_center.add({
  {
    desc = "Delete hidden buffers",
    cmd = "<CMD>BdeleteHidden<CR>",
    keys = { "n", "<leader>bH", noremap }
  },
})
