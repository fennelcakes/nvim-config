require("dir-telescope").setup({
  hidden = true,
  respect_gitignore = true,
})


local noremap = { noremap = true }
local command_center = require("command_center")
command_center.add({
  {
    desc = "Telescope Dir: Grep in Dir",
    cmd = "<cmd>GrepInDirectory<CR>",
    keys = { "n", "<Leader>f.", noremap }
  },
  {
    desc = "Telescope Dir: File in Dir",
    cmd = "<cmd>FileInDirectory<CR>",
    keys = { "n", "<Leader>fF", noremap }
  },
})
