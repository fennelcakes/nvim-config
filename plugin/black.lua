-- black (python formatter)

--[[ NOTE: -- KEYMAPS
vim.api.nvim_set_keymap('n', ',fb', '<cmd>call Black()<cr>', {noremap = true})
]] --


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- Black
  {
    desc = "Format Python file with Black",
    cmd = "<cmd>call Black()<cr>",
    keys = { "n", ",fb", noremap }
  },
})
