-- NOTE:
-- <M-a> and <M-p> to jump around between references
-- <M-i> to jump to def

-- default configuration
require('illuminate').configure({
  -- providers: provider used to get references in the buffer, ordered by priority
  providers = {
    'lsp',
    'treesitter',
    'regex',
  },
  -- delay: delay in milliseconds
  delay = 100,
  -- filetypes_denylist: filetypes to not illuminate, this overrides filetypes_allowlist
  filetypes_denylist = {
    'dirvish',
    'fugitive',
    'txt',
  },
  -- filetypes_allowlist: filetypes to illuminate, this is overriden by filetypes_denylist
  filetypes_allowlist = {},
  -- modes_denylist: modes to not illuminate, this overrides modes_allowlist
  modes_denylist = {},
  -- modes_allowlist: modes to illuminate, this is overriden by modes_denylist
  modes_allowlist = {},
  -- providers_regex_syntax_denylist: syntax to not illuminate, this overrides providers_regex_syntax_allowlist
  -- Only applies to the 'regex' provider
  -- Use :echom synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name')
  providers_regex_syntax_denylist = {},
  -- providers_regex_syntax_allowlist: syntax to illuminate, this is overriden by providers_regex_syntax_denylist
  -- Only applies to the 'regex' provider
  -- Use :echom synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name')
  providers_regex_syntax_allowlist = {},
  -- under_cursor: whether or not to illuminate under the cursor
  under_cursor = false,
})

local ill_highlights = {
  "IlluminatedWordText",
  "IlluminatedWordRead",
  "IlluminatedWordWrite",
}

local autocmd = vim.api.nvim_create_autocmd
local hl = vim.api.nvim_set_hl
for _, v in pairs(ill_highlights) do
  autocmd({ "Filetype" }, {
    pattern = "*",
    callback = function()
      hl(0, v, {
        fg = "#AB9766",
        bg = "#4B4D53",
      })
    end,
  })
end

local command_center = require("command_center")
command_center.add({
  {
    desc = "Globally pause vim-illuminate.",
    cmd = function()
      require('illuminate').pause()
    end,
  },
  {
    desc = "Globally resume vim-illuminate.",
    cmd = function()
      require('illuminate').resume()
    end,
  },
  {
    desc = "Globally toggle pause/resume for vim-illuminate.",
    cmd = function()
      require('illuminate').toggle()
    end,
  },
  {
    desc = "Buffer-local toggle pause/resume for vim-illuminate.",
    cmd = function()
      require('illuminate').toggle_buf()
    end,
  },
  {
    desc = "Buffer-local pause of vim-illuminate.",
    cmd = function()
      require('illuminate').pause_buf()
    end,
  },
  {
    desc = "Buffer-local resume of vim-illuminate.",
    cmd = function()
      require('illuminate').resume_buf()
    end,
  },
  {
    desc = "Freeze the illumination on the buffer",
    cmd = function()
      require('illuminate').freeze_buf()
    end,
  },
  {
    desc = "Unfreeze the illumination on the buffer.",
    cmd = function()
      require('illuminate').unfreeze_buf()
    end,
  },
  {
    desc = "Move cursor to the closest refs after cursor",
    cmd = function()
      require('illuminate').goto_next_reference()
    end,
  },
  {
    desc = "Move cursor to the closest refs before cursor",
    cmd = function()
      require('illuminate').goto_prev_reference()
    end,
  },
  {
    desc = "Selects current reference for use as a text-object.",
    cmd = function()
      require('illuminate').textobj_select()
    end,
  },
})
