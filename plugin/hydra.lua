local Hydra = require('hydra')

local function cmd(command)
  return table.concat({ '<Cmd>', command, '<CR>' })
end

-- "<C-a> and <C-x> increment/decrement the next number on the line, or, w/ :norm, in a visual selection",
-- Hydra({
--   name = "Numeric Increment/Decrement",
--   config = {
--     color = 'pink',
--     invoke_on_body = true,
--     hint = {
--       position = 'bottom',
--       border = 'rounded'
--     },
--   },
--   mode = { 'n' },
--   body = '<C-x>',
--   heads = {
--     { 'f', "<C-a>" }, -- On a line
--     { 'd', "<C-x>" },
--     { '<esc>', "<esc>", { exit = true, nowait = true } },
--   }
-- })

-- Line Management
local vline_move_hint = [[
_j_: move ln up   _J_: dupl ln up
_k_: move ln up   _K_: dupl ln up
_h_: move ln left _H_: dupl ln right
_l_: move ln left _L_: dupl ln right
_<esc>_: exit
]]

Hydra({
  hint = vline_move_hint,
  name = 'V-Mode Selection Move',
  config = {
    color = 'amaranth',
    invoke_on_body = true,
    hint = {
      position = 'top',
      border = 'rounded'
    },
  },
  mode = { 'v', 'x', 's' },
  body = '<S-m>',
  heads = {
    -- Move line up/down/left/right
    { 'j', "<Plug>GoVSMDown" },
    { 'k', "<Plug>GoVSMUp" },
    { 'h', "<Plug>GoVSMLeft" },
    { 'l', "<Plug>GoVSMRight" },
    { 'J', "<Plug>GoVSDDown" },
    { 'K', "<Plug>GoVSDUp" },
    { 'H', "<Plug>GoVSDLeft" },
    { 'L', "<Plug>GoVSDRight" },
    { '<esc>', "<esc>", { exit = true, nowait = true } },
  }

})

Hydra({
  hint = vline_move_hint,
  name = 'N-Mode Line Move',
  config = {
    color = 'amaranth',
    invoke_on_body = true,
  },
  mode = 'n',
  body = '<S-m>',
  heads = {
    -- Move line up and down
    { 'j', "<Plug>GoNSMDown" },
    { 'k', "<Plug>GoNSMUp" },
    { 'h', "<Plug>GoNSMLeft" },
    { 'l', "<Plug>GoNSMRight" },
    { 'J', "<Plug>GoNSDDown" },
    { 'K', "<Plug>GoNSDUp" },
    { 'H', "<Plug>GoNSDLeft" },
    { 'L', "<Plug>GoNSDRight" },
    { '<esc>', "<esc>", { exit = true, nowait = true } },
  }
})



-- Dap Hydra
local dap = require 'dap'
local dap_hint = [[
 _n_: step over   _s_: Continue/Start   _b_: Breakpoint     _K_: Eval
 _i_: step into   _x_: Quit             ^ ^                 ^ ^
 _o_: step out    _X_: Stop             ^ ^
 _c_: to cursor   _C_: Close UI
 ^
 ^ ^              _q_: exit
]]

Hydra({
  hint = dap_hint,
  config = {
    color = 'pink',
    invoke_on_body = true,
    hint = {
      position = 'bottom',
      border = 'rounded'
    },
  },
  name = 'dap',
  mode = { 'n', 'x' },
  body = '<leader>;hd',
  heads = {
    { 'n', dap.step_over, { silent = true } },
    { 'i', dap.step_into, { silent = true } },
    { 'o', dap.step_out, { silent = true } },
    { 'c', dap.run_to_cursor, { silent = true } },
    { 's', dap.continue, { silent = true } },
    { 'x', ":lua require'dap'.disconnect({ terminateDebuggee = false })<CR>", { exit = true, silent = true } },
    { 'X', dap.close, { silent = true } },
    { 'C', ":lua require('dapui').close()<cr>:DapVirtualTextForceRefresh<CR>", { silent = true } },
    { 'b', dap.toggle_breakpoint, { silent = true } },
    { 'K', ":lua require('dap.ui.widgets').hover()<CR>", { silent = true } },
    { 'q', nil, { exit = true, nowait = true } },
  }
})

-- Window Management
local window_menu = [[

 ^^^^^^     Move     ^^^^^^   ^^    Size   ^^   ^^     Split
 ^^^^^^--------------^^^^^^   ^^-----------^^   ^^---------------
 ^ ^ _e_ ^ ^   ^ ^ _E_ ^ ^     ^    _+_   ^     _s_: horizontally
 _m_ ^ ^ _i_   _M_ ^ ^ _I_        _<_ _>_       _v_: vertically
 ^ ^ _n_ ^ ^   ^ ^ _N_ ^ ^     ^    _-_   ^     _c_: close
 focus^^^^^^   window^^^^^^   ^ _=_ equalize^   _o_: only

        ^^^^focus _f_^^^^  ^^^^shift _<C-e>_^^^^  ^^^^swap _<C-w>_^^^^
        ^^^^search _*_^^^^ ^^^^prev _p_^^^^
]]

local function focus_window()
  local window = require('window-picker').pick_window()
  vim.api.nvim_set_current_win(window)
end

Hydra({

  name = 'Windows',
  hint = window_menu,
  config = {
    color = 'pink',
    invoke_on_body = true,
    hint = {
      position = 'bottom-left',
      border = 'rounded',
    },
  },
  mode = 'n',
  body = '<C-w>',
  heads = {
    -- split windows
    { 's', cmd 'split' },
    { 'v', cmd 'vsplit' },

    { 'c', cmd 'close' }, -- close current window
    { 'o', cmd 'only' }, -- close all windows but current

    -- window resizing
    { '=', cmd 'wincmd =' },
    { '+', cmd 'wincmd +' },
    { '-', cmd 'wincmd -' },
    { '>', cmd 'wincmd <' }, -- The symbols are reversed
    { '<', cmd 'wincmd >' },

    -- focus window
    { 'm', cmd 'wincmd h' },
    { 'n', cmd 'wincmd j' },
    { 'e', cmd 'wincmd k' },
    { 'i', cmd 'wincmd l' },
    { 'p', cmd 'wincmd p' },
    { 'f', focus_window },

    -- move window around
    { 'M', cmd 'WinShift left' },
    { 'N', cmd 'WinShift down' },
    { 'E', cmd 'WinShift up' },
    { 'I', cmd 'WinShift right' },

    -- WinShift modes
    { '<C-w>', cmd 'WinShift swap' },
    { '<C-e>', cmd 'WinShift' },

    -- Search text under cursor in new v-split
    { '*', '<cmd>vsplit<CR>/<C-R><C-W><CR><Esc>', { exit = true, desc = false } },

    -- Exit Hydra
    { 'q', nil, { exit = true, desc = false } },
    { '<CR>', nil, { exit = true, desc = false } },
    { '<Esc>', nil, { exit = true, desc = false } },
  }

})




-- Side Scroll
Hydra({
  name = 'Side scroll',
  mode = 'n',
  body = 'z',
  heads = {
    { 'h', '10zh' },
    { 'l', '10zl', { desc = '←/→' } },
    { 'H', 'zH' },
    { 'L', 'zL', { desc = 'half screen ←/→' } },
  }
})



-- Frequently used options
local opt_hint = [[
  ^ ^        Options
  ^
  _v_ %{ve} virtual edit          ^  _a_ autosave        ^
  _i_ %{list} invisible characters  ^  _C_ Cursor Column   ^
  _s_ %{spell} spell                 ^  _l_ color column    ^
  _w_ %{wrap} wrap                  ^  _d_ LSP Diagnostics ^
  _c_ %{cul} cursor line           ^  _x_ Vtext Context   ^
  _n_ %{nu} number                  ^                      ^
  _r_ %{rnu} relative number        ^                      ^
  ^
       ^^^^                _<Esc>_
]]

Hydra({
  name = 'Options',
  hint = opt_hint,
  config = {
    color = 'amaranth',
    invoke_on_body = true,
    hint = {
      border = 'rounded',
      position = 'middle'
    }
  },
  mode = { 'n', 'x' },
  body = '<leader>o',
  heads = {
    { 'n', function()
      if vim.o.number == true then
        vim.o.number = false
      else
        vim.o.number = true
      end
    end, { desc = 'number' } },
    { 'r', function()
      if vim.o.relativenumber == true then
        vim.o.relativenumber = false
      else
        vim.o.number = true
        vim.o.relativenumber = true
      end
    end, { desc = 'relativenumber' } },
    { 'v', function()
      if vim.o.virtualedit == 'all' then
        vim.o.virtualedit = 'block'
      else
        vim.o.virtualedit = 'all'
      end
    end, { desc = 'virtualedit' } },
    { 'i', function()
      if vim.o.list == true then
        vim.o.list = false
      else
        vim.o.list = true
      end
    end, { desc = 'show invisible' } },
    { 's', function()
      if vim.o.spell == true then
        vim.o.spell = false
      else
        vim.o.spell = true
      end
    end, { exit = true, desc = 'spell' } },
    { 'w', function()
      if vim.o.wrap == true then
        vim.o.wrap = false
      else
        vim.o.wrap = true
      end
    end, { desc = 'wrap' } },
    { 'c', function()
      if vim.o.cursorline == true then
        vim.o.cursorline = false
      else
        vim.o.cursorline = true
      end
    end, { desc = 'cursor line' } },
    { 'a', ":ASToggle<CR>", { desc = "Autosave Toggle", exit = true } },
    { 'C', function()
      if vim.o.cursorcolumn == true then
        vim.o.cursorcolumn = false
      else
        vim.o.cursorcolumn = true
      end
    end, { desc = 'cursor column' } },
    { 'l', function()
      if vim.o.colorcolumn == "0" then
        vim.o.colorcolumn = "80"
      else
        vim.o.colorcolumn = "0"
      end
    end, { desc = 'color column' } },
    { 'd', ":ToggleDiag<CR>",
      { desc = "LSP Diagnostics", exit = true } },
    { 'x', ":NvimContextVtToggle<CR>",
      { desc = "Vtext Context", exit = true } },
    { '<Esc>', nil, { exit = true } },
  }
})



local Hydra = require("hydra")
local gitsigns = require('gitsigns')

local hint = [[
 _J_: next hunk   _s_: stage hunk        _d_: show deleted   _b_: blame line
 _K_: prev hunk   _u_: undo last stage   _p_: preview hunk   _B_: blame show full
 ^ ^              _S_: stage buffer      ^ ^                 _/_: show base file
 ^
 ^ ^              _<Enter>_: Diffview              _q_: exit
]]

Hydra({
   name = 'Git',
   hint = hint,
   config = {
      buffer = bufnr,
      color = 'pink',
      invoke_on_body = true,
      hint = {
         border = 'rounded'
      },
      on_enter = function()
         vim.cmd 'mkview'
         vim.cmd 'silent! %foldopen!'
         vim.bo.modifiable = false
         -- gitsigns.toggle_signs(true)
         gitsigns.toggle_linehl(true)
      end,
      on_exit = function()
         local cursor_pos = vim.api.nvim_win_get_cursor(0)
         vim.cmd 'loadview'
         vim.api.nvim_win_set_cursor(0, cursor_pos)
         vim.cmd 'normal zv'
         -- gitsigns.toggle_signs(false)
         gitsigns.toggle_linehl(false)
         gitsigns.toggle_deleted(false)
      end,
   },
   mode = {'n','x'},
   body = '<leader>g',
   heads = {
      { 'J',
         function()
            if vim.wo.diff then return ']c' end
            vim.schedule(function() gitsigns.next_hunk() end)
            return '<Ignore>'
         end,
         { expr = true, desc = 'next hunk' } },
      { 'K',
         function()
            if vim.wo.diff then return '[c' end
            vim.schedule(function() gitsigns.prev_hunk() end)
            return '<Ignore>'
         end,
         { expr = true, desc = 'prev hunk' } },
      { 's', ':Gitsigns stage_hunk<CR>', { silent = true, desc = 'stage hunk' } },
      { 'u', gitsigns.undo_stage_hunk, { desc = 'undo last stage' } },
      { 'S', gitsigns.stage_buffer, { desc = 'stage buffer' } },
      { 'p', gitsigns.preview_hunk, { desc = 'preview hunk' } },
      { 'd', gitsigns.toggle_deleted, { nowait = true, desc = 'toggle deleted' } },
      { 'b', gitsigns.blame_line, { desc = 'blame' } },
      { 'B', function() gitsigns.blame_line{ full = true } end, { desc = 'blame show full' } },
      { '/', gitsigns.show, { exit = true, desc = 'show base file' } }, -- show the base of the file
      { '<Enter>', '<Cmd>DiffviewOpen<CR>', { exit = true, desc = 'Diffview' } },
      { 'q', nil, { exit = true, nowait = true, desc = 'exit' } },
   }
})
