-- BUG: Any commit past 84260 doesn't respect bg_d highlight 30Aug22

require('modes').setup({
  colors = {
    copy = "#f5c359",
    delete = "#c75c6a",
    insert = "#5AB0F6",
    -- insert = "#78ccc5",
    visual = "#9745be",
  },

  -- Set opacity for cursorline and number background
  line_opacity = 0.15,

  -- Enable cursor highlights
  set_cursor = true,

  -- Enable cursorline initially, and disable cursorline for inactive windows
  -- or ignored filetypes
  set_cursorline = false,

  -- Enable line number highlights to match cursorline
  set_number = false,

  -- Disable modes highlights in specified filetypes
  -- Please PR commonly ignored filetypes
  ignore_filetypes = { 'NvimTree', 'TelescopePrompt' }
})


-- steal from modicator.nvim to try to get numberline highlights working

local api = vim.api
local autocmd = api.nvim_create_autocmd
local get_highlight_fg = function(group)
  return api.nvim_get_hl_by_name(group, true).foreground
end

local options = {
  line_numbers = true,
  cursorline = true,
  highlights = {
    modes = {
      ['n']  = '#97CA72',
      ['i']  = '#5AB0F6',
      ['v']  = '#C972E4',
      ['V']  = '#C972E4',
      [''] = '#C972E4',
      ['s']  = get_highlight_fg('Keyword'),
      ['S']  = get_highlight_fg('Keyword'),
      ['R']  = '#EF5F6B',
      ['c']  = get_highlight_fg('Constant'),
    },
  },
}

local set_merged_hl = function(hlgroup, color)
  local base_highlight = api.nvim_get_hl_by_name(hlgroup, true)
  local opts = vim.tbl_extend('keep', { foreground = color }, base_highlight)
  api.nvim_set_hl(0, hlgroup, opts)
end

autocmd({'ModeChanged'}, {
    callback = function()
      local mode = api.nvim_get_mode().mode
      local color = options.highlights.modes[mode] or options.highlights.modes.n
      set_merged_hl('CursorLineNr', color)
    end,
  })
