local easypick = require("easypick")


local colorscheme_list = [[
<< EOF
:Catppuccin frappe
:colorscheme onedark | set background=dark
:colorscheme onenord | set background=dark
:colorscheme kanagawa
:colorscheme nightfox
:colorscheme nordfox
:colorscheme spring-night
:colorscheme tokyonight
:colorscheme gruvbox | set background=light
:colorscheme gruvbox | set background=dark
EOF
]]

-- only required for the example to work
local base_branch = "develop"

local easypick_list = [[
<< EOF
:Easypick ls
:Easypick changed_files
:Easypick conflicts
:Easypick color_schemes
EOF
]]

easypick.setup({
  pickers = {
    -- EXAMPLES

    -- list files inside current folder with default previewer
    -- name for your custom picker, that can be invoked using :Easypick <name> (supports tab completion)
    { name = "ls",
      -- the command to execute, output has to be a list of plain text entries
      command = "ls",
      -- specify your custom previwer, or use one of the easypick.previewers
      previewer = easypick.previewers.default() },

    -- diff current branch with base_branch and show files that changed with respective diffs in preview
    { name = "changed_files",
      command = "git diff --name-only $(git merge-base HEAD " .. base_branch .. " )",
      previewer = easypick.previewers.branch_diff({ base_branch = base_branch }) },

    -- list files that have conflicts with diffs in preview
    { name = "conflicts",
      command = "git diff --name-only --diff-filter=U --relative",
      previewer = easypick.previewers.file_diff() },

    -- CUSTOM

    { name = "easypick_list",
      command = "cat " .. easypick_list,
      -- pass a pre-configured action that runs the command
      action = easypick.actions.run_nvim_command,
      -- you can specify any theme you want, but the dropdown looks good for this example =)
      opts = require('telescope.themes').get_dropdown({})
    },
    { name = "color_schemes",
      command = "cat " .. colorscheme_list,
      -- pass a pre-configured action that runs the command
      action = easypick.actions.run_nvim_command,
      -- you can specify any theme you want, but the dropdown looks good for this example =)
      opts = require('telescope.themes').get_dropdown({})
    }

  }
})



local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Easypick: Pickers",
    cmd = ":Easypick easypick_list<CR>",
    keys = { "n", "<C-e>", noremap }
  },
  {
    desc = "Easypick: colorcsheme",
    cmd = ":Easypick color_schemes<CR>",
    -- keys = { "n", "<leader>cs", noremap }
  },
})
