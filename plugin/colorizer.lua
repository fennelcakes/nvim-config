-- vim.opt.termguicolors = true -- Set in options.lua, uncomment if needed for load order or something

-- DarkCyan
-- #FF4499

require 'colorizer'.setup {

  DEFAULT_OPTIONS = {
    RGB      = true; -- #RGB hex codes
    RRGGBB   = true; -- #RRGGBB hex codes
    names    = true; -- "Name" codes like Blue
    RRGGBBAA = false; -- #RRGGBBAA hex codes
    rgb_fn   = false; -- CSS rgb() and rgba() functions
    hsl_fn   = false; -- CSS hsl() and hsla() functions
    css      = true; -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
    css_fn   = false; -- Enable all CSS *functions*: rgb_fn, hsl_fn
    -- Available modes: foreground, background
    mode     = 'background'; -- Set the display mode.
  }

}

package.loaded['colorizer'] = nil
require('colorizer').setup(...)
require('colorizer').attach_to_buffer(0)
