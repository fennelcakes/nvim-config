require('code_runner').setup {
  mode = "term",
  startinsert = true,
  term = {
    position = "bot",
    size = 15,
  },
  filetype = {
    java = "cd $dir && javac $fileName && java $fileNameWithoutExt",
    python = "python3 -u",
    typescript = "deno run",
    rust = "cd $dir && rustc $fileName && $dir/$fileNameWithoutExt"
  },
  project = {
    -- ["~/deno/example"] = {
    --   name = "ExapleDeno",
    --   description = "Project with deno using other command",
    --   file_name = "http/main.ts",
    --   command = "deno run --allow-net"
    -- },
    -- ["~/cpp/example"] = {
    --   name = "ExapleCpp",
    --   description = "Project with make file",
    --   command = "make buid & cd buid/ & ./compiled_file"
    -- }
  },
}


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  { desc = "Code Runner: Run Code",
    cmd = "<cmd>RunCode<CR>",
    keys = { "n", "<leader>r", noremap }
  },
  { desc = "Code Runner: RunFile",
    cmd = "<cmd>RunFile<CR>",
    keys = { "n", "<leader>rf", noremap }
  },
  { desc = "Code Runner: RunFile tab",
    cmd = "<cmd>RunFile tab<CR>",
    keys = { "n", "<leader>rft", noremap }
  },
  { desc = "Code Runner: RunProject",
    cmd = "<cmd>RunProject<CR>",
    keys = { "n", "<leader>rp", noremap }
  },
  { desc = "Code Runner: Close",
    cmd = "<cmd>RunClose<CR>",
    keys = { "n", "<leader>rc", noremap }
  },
  { desc = "Code Runner: CRFiletype",
    cmd = "<cmd>CRFiletype<CR>",
    keys = { "n", "<leader>crf", noremap }
  },
  { desc = "Code Runner: CRPRoject",
    cmd = "<cmd>CRProjects<CR>",
    keys = { "n", "<leader>crp", noremap }
  },

})
