require 'marks'.setup {
  signs = true,
  -- signcolumn = "auto",
  -- whether to map keybinds or not. default true
  default_mappings = true,
  -- which builtin marks to show. default {}
  builtin_marks = { "<", ">", "^" },
  -- whether movements cycle back to the beginning/end of buffer. default true
  cyclic = true,
  -- whether the shada file is updated after modifying uppercase marks. default false
  force_write_shada = false,
  -- how often (in ms) to redraw signs/recompute mark positions.
  -- higher values will have better performance but may cause visual lag,
  -- while lower values may cause performance penalties. default 150.
  refresh_interval = 250,
  -- sign priorities for each type of mark - builtin marks, uppercase marks, lowercase
  -- marks, and bookmarks.
  -- can be either a table with all/none of the keys, or a single number, in which case
  -- the priority applies to all marks.
  -- default 10.
  sign_priority = { lower = 10, upper = 15, builtin = 8, bookmark = 20 },
  -- disables mark tracking for specific filetypes. default {}
  excluded_filetypes = {},
  -- marks.nvim allows you to configure up to 10 bookmark groups, each with its own
  -- sign/virttext. Bookmarks can be used to group together positions and quickly move
  -- across multiple buffers. default sign is '!@#$%^&*()' (from 0 to 9), and
  -- default virt_text is "".
  bookmark_0 = {
    sign = "⚑",
    virt_text = "hello world"
  },
  mappings = {}
}


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  -- Marks.nvim
  {
    desc = "Marks: Set mark x", -- puts you in a 'pending mode' awaiting a char
    cmd = function()
      require 'marks'.set()
    end,
    keys = { "n", "m", noremap },
  },
  {
    desc = "Marks: Set the next available alphabetical (lowercase) mark",
    cmd = function()
      require 'marks'.set_next()
    end,
    keys = { "n", "m,", noremap },
  },
  {
    desc = "Marks: Toggle the next available mark at the current line",
    cmd = function()
      require 'marks'.toggle()
    end,
    keys = { "n", "m;", noremap },
  },
  {
    desc = "Marks: Delete mark x",
    cmd = function()
      require 'marks'.delete()
    end,
    keys = { "n", "dm", noremap },
  },
  {
    desc = "Marks: Delete all marks on the current line",
    cmd = function()
      require 'marks'.delete_line()
    end,
    keys = { "n", "dm-", noremap },
  },
  {
    desc = "Marks: Delete all marks in the current buffer",
    cmd = function()
      require 'marks'.delete_buf()
    end,
    keys = { "n", "dm<space>", noremap },
  },
  {
    desc = "Marks: Move to next mark",
    cmd = function()
      require 'marks'.next()
    end,
    keys = { "n", "m]", noremap },
  },
  {
    desc = "Marks: Move to previous mark",
    cmd = function()
      require 'marks'.prev()
    end,
    keys = { "n", "m[", noremap },
  },
  {
    desc = "Marks: Preview mark.",
    cmd = function()

      require 'marks'.preview()
    end,
    keys = { "n", "m:", noremap },
  },
  {
    desc= "Marks: Fill loclist with all marks",
    cmd = "<CMD>MarksListAll<CR>",
    keys = { "n", "m/", noremap}
  }

  -- Unsure how these work
  -- {
  --   desc = "Marks: Add a bookmark from bookmark group[0-9].",
  --   cmd = function()
  --
  --   end,
  --   keys = { "n", "m[0-9]", noremap },
  -- },
  -- {
  --   desc = "Marks: Delete all bookmarks from bookmark group[0-9].",
  --   cmd = function()
  --
  --   end,
  --   keys = { "n", "dm[0-9]", noremap },
  -- },
  -- {
  --   desc = "Marks: Move to the next bookmark",
  --   cmd = function()
  --     require 'marks'.next_bookmark()
  --   end,
  --   keys = { "n", "m}", noremap },
  -- },
  -- {
  --   desc = "Marks: Move to the previous bookmark",
  --   cmd = function()
  --     require 'marks'.prev_bookmark()
  --   end,
  --   keys = { "n", "m{", noremap },
  -- },
  -- {
  --   desc = "Marks: Delete the bookmark under the cursor.",
  --   cmd = function()
  --     require 'marks'.delete_bookmark()
  --   end,
  --   keys = { "n", "dm=", noremap },
  -- },
})
