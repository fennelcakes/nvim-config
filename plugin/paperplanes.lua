-- options shown with default values
require("paperplanes").setup({
  register = "+",
  provider = "0x0.st",
  provider_options = {},
  notifier = vim.notify or print,
})


-- local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Paperlanes: post buffer to a paste bin",
    cmd =":PP<CR>",
  },
  {
    desc = "Paperlanes: post visual selection to a paste bin",
    cmd =":'<,'>PP<CR>",
  },
})
