local setup = {
  allowed_separators = {
    ',',
    ';', 'and', 'or', '&', '&&', '||',
    '|',
    '==',
    '===',
    '!=',
    '!==',
    '-',
    '+',
    ['<'] = '>',
    ['<='] = '>=',
    ['>'] = '<',
    ['>='] = '<=',
  },
  use_default_keymaps = true,
  keymaps = {
    ['<C-.>'] = 'swap_with_right',
    ['<C-,>'] = 'swap_with_left',
    ['<space>.'] = 'swap_with_right_with_opp',
    ['<space>,'] = 'swap_with_left_with_opp',
  },
  ignore_injected_langs = false,
}

require('sibling-swap').setup()
