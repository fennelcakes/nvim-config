require("noice").setup({
  lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
  },
  -- you can enable a preset for easier configuration
  presets = {
    bottom_search = true, -- use a classic bottom cmdline for search
    command_palette = true, -- position the cmdline and popupmenu together
    long_message_to_split = true, -- long messages will be sent to a split
    inc_rename = true, -- enables an input dialog for inc-rename.nvim
    lsp_doc_border = true, -- add a border to hover docs and signature help
  },
})

-- require("noice").setup({
--   cmdline = {
--     format = {
--       search_down = {
--         view = "cmdline",
--       },
--       search_up = {
--         view = "cmdline",
--       },
--     },
--     view = "cmdline",
--   },
--   -- cmdline_popup = {
--   --   position = {
--   --     row = "97%",
--   --     col = "1",
--   --   },
--   -- }
--   views = {
--     mini = {
--       backend = "mini",
--       relative = "editor",
--       align = "message-right",
--       timeout = 10000,
--       reverse = false,
--       position = {
--         row = -1,
--         col = -1,
--       },
--       size = "auto",
--       -- size = {
--       --   width = "5%",
--       --   height = "5%",
--       -- },
--       border = {
--         style = "none",
--       },
--       zindex = 60,
--       win_options = {
--         winblend = 30,
--         winhighlight = {
--           Normal = "NoiceMini",
--           -- Normal = "NoiceMini",
--           IncSearch = "",
--           Search = "",
--         },
--       },
--     },
--   }
-- })
require("telescope").load_extension("noice")

--
local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = ":Noice - show a full message history",
    cmd = "<cmd>Noice<CR>",
    keys = { "n", "<leader>n", noremap }
    -- keys = { "n", "<leader>n", noremap }
  },
  {
    desc = "Telescope: Noice",
    cmd = "<cmd>Noice telescope<CR>",
    keys = { "n", "<leader>N", noremap }
  },
})
