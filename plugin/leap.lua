require('leap').add_default_mappings()


-- bidirectional
-- require('leap').leap { target_windows = { vim.fn.win_getid() } }

-- require('leap').leap { opts = {
require('leap').setup {
  max_phase_one_targets = nil,
  highlight_unlabeled_phase_one_targets = false,
  max_highlighted_traversal_targets = 10,
  case_sensitive = false,
  equivalence_classes = { ' \t\r\n', },
  substitute_chars = {},
  safe_labels = { 's', 'e', 't', 'n', 'f', },
  labels = { 's', 'e', 't', 'n', 'f', 'w', 'y', 'g', 'm', 'h', 'l', 'S', 'E', 'T', 'N', 'F', 'W', 'Y', 'G', 'M', 'H', 'L'},

-- qwfplu;
-- arstneio
-- zxcdh,./

  special_keys = {
    repeat_search = '<enter>',
    next_phase_one_target = '<enter>',
    next_target = { '<enter>', ';' },
    prev_target = { '<tab>', ',' },
    next_group = '<space>',
    prev_group = '<tab>',
    multi_accept = '<enter>',
    multi_revert = '<backspace>',
  },
}

-- The below settings make Leap's highlighting a bit closer to what you've been
-- used to in Lightspeed.

vim.api.nvim_set_hl(0, 'LeapBackdrop', {
  fg = '#3C4558'
})
vim.api.nvim_set_hl(0, 'LeapMatch', {
  fg = '#C972E4',  -- for light themes, set to 'black' or similar
  bold = true,
  nocombine = true,
})
vim.api.nvim_set_hl(0, 'LeapLabelPrimary', {
  fg = '#ED5E6A',  -- for light themes, set to 'black' or similar
  bold = true,
  nocombine = true,
})
vim.api.nvim_set_hl(0, 'LeapLabelSecondary', {
  fg = '#5AB0F6',  -- for light themes, set to 'black' or similar
  bold = true,
  nocombine = true,
})
vim.api.nvim_set_hl(0, 'LeapLabelSelected', {
  fg = '#D99B5E',  -- for light themes, set to 'black' or similar
  bold = true,
  nocombine = true,
})


-- replaces clever-f
require('flit').setup {
  keys = { f = 'f', F = 'F', t = 't', T = 'T' },
  -- A string like "nv", "nvo", "o", etc.
  labeled_modes = "v",
  multiline = true,
  -- Like `leap`s similar argument (call-specific overrides).
  -- E.g.: opts = { equivalence_classes = {} }
  opts = {}
}



-- *<Plug>(leap-forward-to)*
-- arguments: `{}` in Normal mode, otherwise `{ offset = +1, inclusive_op = true }`
-- default mapping: `s`
--
-- *<Plug>(leap-forward-till)*
-- arguments: `{ offset = -1, inclusive_op = true }`
-- default mapping: `x` (Visual and Operator-pending mode only)
--
-- *<Plug>(leap-backward-to)*
-- arguments: `{ backward = true }`
-- default mapping: `S`
--
-- *<Plug>(leap-backward-till)*
-- arguments: `{ backward = true, offset = 2 }`
-- default mapping: `X` (Visual and Operator-pending mode only)
--
-- *<Plug>(leap-cross-window)*
-- arguments: `{ target_windows = require('leap.util').get_enterable_windows() }`
-- default mapping: `gs`

-- Example: `vim.keymap.set({'n', 'x', 'o'}, 'f', '<Plug>(leap-forward-to)')`


-- Getting used to `d` shouldn't take long - after all, it is more comfortable
-- than `x`, and even has a better mnemonic.
-- If you still desperately want your old `x` back, then just delete these
-- mappings set by Leap:
vim.keymap.del({'x', 'o'}, 'S')
vim.keymap.del({'x', 'o'}, 'x')
vim.keymap.del({'x', 'o'}, 'X')
-- To set alternative keys for "exclusive" selection:
-- vim.keymap.set({'x', 'o'}, 'c', '<Plug>(leap-forward-to)')
-- vim.keymap.set({'x', 'o'}, 'C', '<Plug>(leap-backward-to)')
-- vim.keymap.set({'n', 'x', 'o', }, '<C-s>', '<Plug>(leap-cross-window)')



-- than `x`, and even has a better mnemonic.


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Leap: forwards (Normal)",
    cmd = function() require('leap').leap {} end,
    keys = { {"n"}, "s", noremap }
  },
  {
    desc = "Leap: backwards (Normal)",
    cmd = function() require('leap').leap {backward = true} end,
    keys = { {"n"}, "S", noremap }
  },
  {
    desc = "Leap: forwards (ox modes)",
    cmd = function() require('leap').leap {} end,
    keys = { {"x", "o"}, "c", noremap }
  },
  {
    desc = "Leap: backwards (ox modes)",
    cmd = function() require('leap').leap {backward = true} end,
    keys = { {"x", "o"}, "C", noremap }
  },
  {
    desc = "Leap: cross-window",
    cmd = function() require('leap').leap {backward = true} end,
    keys = { {"n"}, ";s", noremap }

  },
  {
    -- Can only leap backwards and within scope so not as useful atm
    desc = "Leap: Treesitter AST Nodes",
    cmd = function() require'leap-ast'.leap() end,
    keys = { {"n"}, ";S", noremap }
  },
})


-- test of some stuff
-- here we test leap magnetic and remote objects



