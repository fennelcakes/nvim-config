require('opptogg').setup({
-- Defaults
  -- opp_table = {
  --   ["True"]  = "False",
  --   ["true"]  = "false",
  --   ["0"]     = "1",
  --   ["yes"]   = "no",
  --   ["Yes"]   = "No",
  --   ["foo"]   = "bar",
  --   ["var1"]  = "var2",
  --
  --   -- TODO: the regex \k doesn't capture these
  --   -- opps["+"]    = "-"
  --   -- opps["*"]    = "/"

  --   -- opps["="]    = "!="
  -- },
  -- mapping = '',


  opp_table = {
    ["True"]  = "False",
    ["true"]  = "false",
    ["0"]     = "1",
    ["enable"]     = "disable",
    ["Enable"]     = "Disable",
    ["yes"]   = "no",
    ["Yes"]   = "No",
    ["foo"]   = "bar",
    ["var1"]  = "var2",
    ["pos"]  = "neg",
    ["on"]  = "off",
    ["next"]  = "previous",
    ["above"]  = "below",
    ["up"]  = "down",
    ["top"]  = "bottom",
    ["left"]  = "right",
  },
  mapping = '<leader>`',

})


-- Test Area

-- True False
-- true false
-- 1 0

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Toggle Boolean Value",
    cmd = "<CMD>OppTogg<CR>",
    keys = { "n", "<leader>`", noremap }
  },
})



-- NOTE: Here's a function that does the same thing without needing the plugin
-- I made the plugin as a way to practice making plugins, practice lua, etc.

-- Toggle Opposite Values
-- user_cmd(
--   'OppTogg',
--   function()
--     -- Set word under cursor to 'cursorword'
--     -- NOTE: this code came from:
--     -- https://github.com/xiyaowong/nvim-cursorword/blob/master/plugin/nvim-cursorword.lua)
--     local function matchstr(...)
--       local ok, ret = pcall(vim.fn.matchstr, ...)
--       return ok and ret or ""
--     end
--
--     local column = api.nvim_win_get_cursor(0)[2]
--     local line = api.nvim_get_current_line()
--
--     local left = matchstr(line:sub(1, column + 1), [[\k*$]])
--     local right = matchstr(line:sub(column + 1), [[^\k*]]):sub(2)
--
--     local cursorword = left .. right
--
--     -- Toggle the word under the cursor and put the cursor back in position
--     local opps = {
--       ["True"] = "False",
--       ["true"] = "false",
--       ["0"]    = "1",
--       ["yes"]    = "no",
--       ["foo"]    = "bar",
--       ["var1"]    = "var2",
--     }
--
--     -- TODO: the regex \k doesn't capture these
--     -- opps["+"]    = "-"
--     -- opps["*"]    = "/"
--     -- opps["="]    = "!="
--
--     local function toggle_word(output)
--       local cursor = api.nvim_win_get_cursor(0)
--       vim.cmd({
--         cmd = 'norm', args = { 'ciw' .. output }
--       })
--       api.nvim_win_set_cursor(0, cursor)
--     end
--
--     for k, v in pairs(opps) do
--       if cursorword == k then
--         toggle_word(v)
--         return
--       elseif cursorword == v then
--         toggle_word(k)
--         return
--       end
--
--     end
--
--     -- If nothing matched the opposites table, notify
--     vim.notify('OppTogg: no opposing value found under cursor', 3)
--
--   end,
--   { nargs = 0 }
-- )
-- -- end of function
--
--


