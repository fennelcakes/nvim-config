local status_ok, fterm = pcall(require, "FTerm")
if not status_ok then
  return
end

fterm.setup({
  -- • "none": No border (default).
  -- • "single": A single line box.
  -- • "double": A double line box.
  -- • "rounded": Like "single", but with rounded
  --   corners ("╭" etc.).
  -- • "solid": Adds padding by a single whitespace
  --   cell.
  -- • "shadow": A drop shadow effect by blending
  --   with the background.
  border = 'rounded',
  dimensions = {
    height = 0.3, -- Height of the terminal window
    width = 0.5, -- Width of the terminal window
    x = 0.2, -- X axis of the terminal window
    y = 1.0, -- Y axis of the terminal window
  },


  ---Close the terminal as soon as shell/command exits.
  ---Disabling this will mimic the native terminal behaviour.
  ---@type boolean
  auto_close = true,
})

-- Example keybindings

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Toggle Terminal (Fterm) (Normal)",
    cmd = '<CMD>lua require("FTerm").toggle()<CR>',
    keys = { "n", "<leader>;;", noremap }
  },
  {
    desc = "Toggle Terminal (Fterm) (Terminal)",
    cmd = '<C-\\><C-n><CMD>lua require("FTerm").toggle()<CR>',
    keys = { "t", "<leader>;;", noremap }
  },
})
