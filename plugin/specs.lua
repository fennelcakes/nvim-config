require('specs').setup {
  show_jumps       = true,
  min_jump         = 7,
  popup            = {
    delay_ms = 5, -- delay before popup displays
    inc_ms = 10, -- time increments used for fade/resize effects
    blend = 0, -- starting blend, between 0-100 (fully transparent), see :h winblend
    width = 30,
    -- winhl = "PMenu",
    winhl = "Question",
    fader = require('specs').pulse_fader,
    resizer = require('specs').shrink_resizer
  },
  ignore_filetypes = {},
  ignore_buftypes  = {
    nofile = true,
  },
}



local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  { desc = "Show Specs (flashes cursor position)",
    cmd = ":lua require('specs').show_specs()<CR>",
    -- keys = { "n", "<C-b>", noremap },
  },
  -- n and N are remapped by hls-lens
  -- { desc = "Jump to Next Search Term and Flash (Specs)",
  --   -- cmd = 'n:lua require("specs").show_specs()<CR>',
  --   keys = { "n", "n", noremap },
  -- },
  -- { desc = "Jump to Previous Search Term and Flash (Specs)",
  --   cmd = 'N:lua require("specs").show_specs()<CR>',
  --   keys = { "n", "N", noremap },
  -- },
})


-- Testing ground:
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
------------------- ----------------- ------------------- -----------------
