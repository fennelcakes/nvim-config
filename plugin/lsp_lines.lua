local status_ok, lsp_lines = pcall(require, "lsp_lines")
if not status_ok then
  return
end

lsp_lines.setup()

-- Disable virtual_text since it's redundant due to lsp_lines.
vim.diagnostic.config({
  virtual_text = false,
  virtual_lines = false,
})



local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Lsp Lines Toggle",
    cmd = "<cmd>lua require('lsp_lines').toggle()<CR>",
    keys = { "n", "<leader>l", noremap }
  },
})
