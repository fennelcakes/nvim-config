require("harpoon").setup({
  global_settings = {
    -- sets the marks upon calling `toggle` on the ui, instead of require `:w`.
    save_on_toggle = false,
    -- saves the harpoon file upon every change. disabling is unrecommended.
    save_on_change = true,
    -- sets harpoon to run the command immediately as it's passed to the terminal when calling `sendCommand`.
    enter_on_sendcmd = false,
    -- closes any tmux windows harpoon that harpoon creates when you close Neovim.
    tmux_autoclose_windows = false,
    -- filetypes that you want to prevent from adding to the harpoon list menu.
    excluded_filetypes = {},
    -- set marks specific to each git branch inside git repository
    mark_branch = false,
    -- use a dynamic width for the Harpoon popup menu
    menu = {
      width = vim.api.nvim_win_get_width(0) - 4,
    }
  }
})



require("telescope").load_extension('harpoon')

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  -- Harpoon
  {
    desc = "Harpoon Commands Menu",
    cmd = function()
      require('harpoon.cmd-ui').toggle_quick_menu()
    end,
    keys = { "n", "<Leader>hc", noremap }
  },

  {
    desc = "Harpoon: add to Quick Menu",
    cmd = function()
      require('harpoon.mark').add_file()
    end,
    keys = { "n", "<leader>ha", noremap }
  },
  {
    desc = "Harpoon: Quick Menu",
    cmd = "<cmd>lua require('harpoon.ui').toggle_quick_menu()<CR><cmd>set rnu<cr>",
    -- keys = { "n", "<leader>m", noremap }
  },
  {
    desc = "Telescope: Harpoon Quick Menu",
    cmd = "<cmd>Telescope harpoon marks<CR>",
    -- cmd = "<cmd>lua require('harpoon.ui').toggle_quick_menu()<CR><cmd>set rnu<cr>",
    keys = { "n", "<leader>h", noremap }
  },
})
