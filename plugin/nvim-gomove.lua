require("gomove").setup {
  -- whether or not to map default key bindings, (true/false)
  map_defaults = false,
  -- whether or not to reindent lines moved vertically (true/false)
  reindent = true,
  -- whether or not to undojoin same direction moves (true/false)
  undojoin = true,
  -- whether to not to move past end column when moving blocks horizontally, (true/false)
  move_past_end_col = false,
}


local command_center = require("command_center")
command_center.add({
  -- Lines:
  -- Name 	Function
  -- Vertical

  {
    desc = "Normal Smart Move Up",
    cmd = "<Plug>GoNSMUp",
  },
  {
    desc = "Normal Smart Move Down",
    cmd = "<Plug>GoNSMDown",
  },
  {
    desc = "Normal Smart Move Left",
    cmd = "<Plug>GoNSMLeft"
  },
  {
    desc = "Normal Smart Move Right",
    cmd = "<Plug>GoNSMRight",
  },
  {
    desc = "Visual Smart Move Up",
    cmd = "<Plug>GoVSMUp",
  },
  {
    desc = "Visual Smart Move Down",
    cmd = "<Plug>GoVSMDown",
  },
  {
    desc = "Visual Smart Move Left",
    cmd = "<Plug>GoVSMLeft"
  },
  {
    desc = "Visual Smart Move Right",
    cmd = "<Plug>GoVSMRight",
  },
  {
    desc = "Normal Smart Duplicate Up",
    cmd = "<Plug>GoNSDUp",
  },
  {
    desc = "Normal Smart Duplicate Down",
    cmd = "<Plug>GoNSDDown",
  },
  {
    desc = "Normal Smart Duplicate Left",
    cmd = "<Plug>GoNSDLeft"
  },
  {
    desc = "Normal Smart Duplicate Right",
    cmd = "<Plug>GoNSDRight",
  },
  {
    desc = "Visual Smart Duplicate Up",
    cmd = "<Plug>GoVSDUp",
  },
  {
    desc = "Visual Smart Duplicate Down",
    cmd = "<Plug>GoVSDDown",
  },
  {
    desc = "Visual Smart Duplicate Left",
    cmd = "<Plug>GoVSDLeft"
  },
  {
    desc = "Visual Smart Duplicate Right",
    cmd = "<Plug>GoVSDRight",
  },
})

