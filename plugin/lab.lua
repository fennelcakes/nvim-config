require('lab').setup {
  code_runner = {
    enabled = true,
  },
  quick_data = {
    enabled = false,
  }
}

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({

  -- Lab Code
  {
    desc = "Lab code start",
    cmd = "<CMD>Lab code start<CR>:Lab code run<CR>",
    keys = { "n", "<leader>lr", noremap }
  },
  {
    desc = "Lab code stop",
    cmd = "<CMD>Lab code stop<CR>",
    keys = { "n", "<leader>ls", noremap }
  },
})

