require('lsp-toggle').setup {
  create_cmds = true, -- Whether to create user commands
  telescope = true, -- Whether to load telescope extensions
}


local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "LSP Toggle",
    cmd = "<cmd>ToggleLSP<CR>",
    keys = { "n", ",lsp", noremap }
  },
  {
    desc = "Telescope: LSP Toggle",
    cmd = "<cmd>Telescope ToggleLSP<CR>",
    keys = { "n", "<leader>fL", noremap }
  },
})
