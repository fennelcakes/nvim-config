require("windows").setup({
  autowidth = { --		       |windows.autowidth|
    enable = false,
    winwidth = 5, --		        |windows.winwidth|
    filetype = { --	      |windows.autowidth.filetype|
      help = 2,
    },
  },
  ignore = { --			  |windows.ignore|
    buftype = { "quickfix", "nofile" },
    filetype = { "DiffviewFiles", "neo-tree", "undotree", "gundo", }
  },
  animation = {
    enable = true,
    duration = 150,
    fps = 30,
    easing = "in_out_sine"
  }
})
