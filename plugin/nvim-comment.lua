require('nvim_comment').setup {

  -- Linters prefer comment and line to have a space in between markers
  marker_padding = true,
  -- should comment out empty or whitespace only lines
  comment_empty = true,
  -- trim empty comment whitespace
  comment_empty_trim_whitespace = true,
  -- Should key mappings be created
  create_mappings = true,
  -- Normal mode mapping left hand side
  line_mapping = "<leader><leader>/",
  -- Visual/Operator mapping left hand side
  operator_mapping = "<leader>/",
  -- text object mapping, comment chunk,,
  comment_chunk_text_object = "ic",
  -- Hook function to call before commenting takes place
  hook = nil

}


-- Nvim-Comment

local noremap = { noremap = true }
local command_center = require("command_center")

command_center.add({
  {
    desc = "Comment Line Toggle (normal)",
    cmd = "<CMD>CommentToggle<CR>",
--    keys = { "n", "<leader><leader>/", noremap },
  },
  {
    desc = "Comment Line Toggle (operator pending)",
    cmd = "<CMD>CommentToggle<CR>",
    -- keys = { "o", "<leader>/", noremap },
  },
  {
    desc = "Comment Line Toggle (visual)",
    cmd = "<CMD>CommentToggle<CR>",
    -- keys = { "v", "<leader>/", noremap },
  },
  {
    desc = "Nvim Comment: Insert Comment Character",
    cmd = "ix<esc>v:CommentToggle<CR>wxi",
    keys = { "n", "gcc", noremap }
  },
})
